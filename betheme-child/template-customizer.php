<?php
/**
 * Template Name: Customizer Page Template
 *
 * @package Betheme Child Theme
 * @author Studio Akar
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js<?php echo esc_attr(mfn_user_os()); ?>"<?php mfn_tag_schema(); ?>>

	<?php get_template_part('customizer/index'); ?>	
	
</html>
