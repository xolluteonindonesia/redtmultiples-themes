<?php
add_action('init', 'create_post_type_custom_art');
function create_post_type_custom_art()
{
	register_post_type('custom-art', // Register Custom Post Type
		array(
		'labels' => array(
			'name' => __('Custom Art', 'stakcustomart'), // Rename these to suit
			'singular_name' => __('Custom Art', 'stakcustomart'),
			'add_new' => __('Add New', 'stakcustomart'),
			'add_new_item' => __('Add New Custom Art', 'stakcustomart'),
			'edit' => __('Edit', 'stakcustomart'),
			'edit_item' => __('Edit Custom Art', 'stakcustomart'),
			'new_item' => __('New Custom Art', 'stakcustomart'),
			'view' => __('View Custom Art', 'stakcustomart'),
			'view_item' => __('View Custom Art', 'stakcustomart'),
			'search_items' => __('Search Custom Art', 'stakcustomart'),
			'not_found' => __('No Custom Arts found', 'stakcustomart'),
			'not_found_in_trash' => __('No Custom Arts found in Trash', 'stakcustomart')
		),
		'public' => true,
		'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
		'has_archive' => true,
		'supports' => array(
			'title',
			'editor',
			// 'excerpt',
			// 'thumbnail'
		), // Go to Dashboard Custom HTML5 Blank post for supports
		'can_export' => true, // Allows export in Tools > Export
		'taxonomies' => array(
			// 'post_tag',
			// 'category'
		), // Add Category and Post Tags support
		'show_ui' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => true
	));
}
function ajax_stak_save_custom_art()
{
	global $woocommerce;
	$return = [];
	// var_dump($_REQUEST);
	$request = $_REQUEST;

	/*if(!is_user_logged_in()){
		$return = [
			'status' => 'failed',
			'message' => 'You are not logged in, please login first and retry!'
		];
		echo json_encode($return);
		exit;
	}*/
	$user = wp_get_current_user();

	// save the image here.
	$upload_dir = wp_upload_dir();
	$data = $request['cropped_img'];

	if (preg_match('/^data:image\/(\w+);base64,/', $data, $type)) {
		$data = substr($data, strpos($data, ',') + 1);
		$type = strtolower($type[1]); // jpg, png, gif

		if (!in_array($type, [ 'jpg', 'jpeg' ])) {
			throw new \Exception('invalid image type');
			exit;
		}

		$data = base64_decode($data);

		if (!$data) {
			throw new \Exception('base64_decode failed');
			exit;
		}
	} else {
		throw new \Exception('did not match data URI with image data');
		exit;
	}
	// var_dump($upload_dir);
	// var_dump($data);
	$sess_id = date("Ymd_His");

	$file_path = $upload_dir['basedir'].'/stak_customizer';
	if(!file_exists($file_path))
		mkdir($file_path, 0755);

	$crop_name = 'custom-art-'.$user->ID.'-'.$sess_id.'.jpg';

	// now we save the image first before add to cart.
	$crop_save = $file_path.'/'.$crop_name;
	// var_dump($crop_save);
	file_put_contents($crop_save, $data);

	if(is_user_logged_in()){
		// setup the post here first.
		$string = 'Custom Art Collection '.$user->display_name.' - '.$request['id'];
		$post_params = [
			'post_title' => $string,
			'post_content' => $string,
			'post_status' => 'publish',
			'post_type' => 'custom-art',
			'comment_status' => 'closed'
		];

		$post_id = wp_insert_post($post_params);
		if($post_id === 0){
			$return = [
				'status' => 'failed',
				'message' => 'Failed in saving your custom art, please retry or contact admin!'
			];
			echo json_encode($return);
			exit;
		}

		update_post_meta($post_id, 'user_id', $user->ID);
		update_post_meta($post_id, 'cropped_img', $upload_dir['baseurl'].'/stak_customizer/'.$crop_name);

		foreach($request as $key => $val){
			// var_dump($key);
			if($key !== 'action' && $key !== 'cropped_img'){
				if($key === 'id')
					update_post_meta($post_id, 'product_id', $val);
				update_post_meta($post_id, $key, $val);
			}
		}
		$return = [
			'status' => 'success',
			'message' => 'Custom art is saved!'
		];
	}else{
		if(!session_id())
			session_start();

		$to_save_in_session = [
			'cropped_img' => $upload_dir['baseurl'].'/stak_customizer/'.$crop_name
		];

		foreach($request as $key => $val){
			// var_dump($key);
			if($key !== 'action' && $key !== 'cropped_img'){
				if($key === 'id')
					$to_save_in_session['product_id'] =  $val;
				$to_save_in_session[$key] =  $val;
			}
		}

		$_SESSION['custom_art'] = [$sess_id => $to_save_in_session];

		$return = [
			'status' => 'not_logged_in',
			'session_name' => $session_name,
			'message' => 'You are not logged in, please login/register first and retry!'
		];
	}
	
	echo json_encode($return);
	exit;
}

add_action( 'wp_ajax_ajax_stak_save_custom_art', 'ajax_stak_save_custom_art' );
// If you wanted to also use the function for non-logged in users (in a theme for example)
add_action( 'wp_ajax_nopriv_ajax_stak_save_custom_art', 'ajax_stak_save_custom_art' );

function get_cropped_price()
{
	$request = $_REQUEST;
	// var_dump($request);
	$product = wc_get_product( $request['product_id'] );
	$price = floatval( calculate_custom_price( $request['width']*$request['height'] ) );
	if(get_woocommerce_currency_symbol() != 'HK $'){
		if(is_plugin_active('woocommerce-currency-switcher/index.php')){
			$woocs = get_option('woocs');
			$rate = floatval($woocs['USD']['rate']);
			$price = $price*$rate;
		}
	}
	// var_dump($price);
	$curr = get_woocommerce_currency_symbol();
	echo '<span class="woocommerce-currency_symbol">'.$curr.'</span> '.number_format($price, 2);
	exit;
}
add_action( 'wp_ajax_get_cropped_price', 'get_cropped_price' );
// If you wanted to also use the function for non-logged in users (in a theme for example)
add_action( 'wp_ajax_nopriv_get_cropped_price', 'get_cropped_price' );

function stak_custom_art_add_to_cart()
{
	$request = $_REQUEST;
	// var_dump($request);
	$product_id = false;

	if(isset($request['product_id'])){
		$product_id = intval( $request['product_id'] );
	}else{
		if(isset($request['id'])){
			$product_id = intval( $request['id'] );
		}
	}
	
	if(!$product_id){
		echo json_encode([
			'status' => 'error',
			'post_id' => intval($request['art_id']),
			'message' => 'The product is missing from the record!'
		]);
		exit;
	}

	
	// var_dump($product_id); exit;
	try {
		$added = WC()->cart->add_to_cart($product_id, 1);
	} catch (Exception $e) {
		echo json_encode([
			'status' => 'error',
			'post_id' => intval($request['art_id']),
			'message' => $e->getMessage()
		]);
		exit;
	}

	echo json_encode([
		'status' => 'ok',
		'post_id' => intval($request['art_id']),
		'message' => $added
	]);
	exit;
}
add_action( 'wp_ajax_stak_custom_art_add_to_cart', 'stak_custom_art_add_to_cart' );
// If you wanted to also use the function for non-logged in users (in a theme for example)
add_action( 'wp_ajax_nopriv_stak_custom_art_add_to_cart', 'stak_custom_art_add_to_cart' );

// stak_delete_custom_art
function stak_delete_custom_art()
{
	$request = $_REQUEST;

	try {
		wp_trash_post(intval($request['post_id']));
	} catch (Exception $e) {
		echo json_encode([
			'status' => 'error',
			'post_id' => intval($request['art_id']),
			'message' => $e->getMessage()
		]);
		exit;
	}
	echo json_encode([
		'status' => 'ok',
		'post_id' => intval($request['art_id']),
		'message' => $added
	]);
	exit;
}
add_action( 'wp_ajax_stak_delete_custom_art', 'stak_delete_custom_art' );
// If you wanted to also use the function for non-logged in users (in a theme for example)
add_action( 'wp_ajax_nopriv_stak_delete_custom_art', 'stak_delete_custom_art' );

function stak_add_saved_arts_endpoint()
{
	add_rewrite_endpoint( 'customized-art', EP_ROOT | EP_PAGES );
}
add_action( 'init', 'stak_add_saved_arts_endpoint' );

function stak_add_saved_arts_query_var( $vars )
{
	$vars[] = 'customized-art';
	return $vars;
}
add_filter( 'query_vars', 'stak_add_saved_arts_query_var', 0 );

function stak_saved_arts_content()
{
	print_saved_arts();
}
add_action( 'woocommerce_account_customized-art_endpoint', 'stak_saved_arts_content' );

function print_saved_arts()
{
	global $wp_query;

	$pdf_links = [
		'view' 		=> home_url().'/'.$wp_query->query_vars['pagename'].'/customized-art/pdf-print',
		'download'	=> home_url().'/'.$wp_query->query_vars['pagename'].'/customized-art/pdf-print-download'
	];
	include_once('stak-custom-art-template.php');
}

/**
 * Adds rewrite rules for printing if using pretty permalinks
 */
add_action('init', '_stak_pdf_rewrite');
function _stak_pdf_rewrite() {
	add_rewrite_endpoint('pdf-print', EP_ALL);
	add_rewrite_endpoint('pdf-print-download', EP_ALL);
}

/**
 * Registers print endpoints
 */
add_filter('query_vars', '_stak_get_pdf_query_vars');
function _stak_get_pdf_query_vars( $query_vars ) {
	$query_vars[] = 'pdf-print';
	$query_vars[] = 'pdf-print-download';
	return $query_vars;
}

// init dompdf lib.
require_once 'dompdf/autoload.inc.php';
use Dompdf\Dompdf;

/**
 * Applies print templates
 */
add_action('template_redirect', '_stak_pdf_template');
function _stak_pdf_template()
{
	global $wp_query;
	// var_dump($wp_query->query_vars); exit;

	if(isset( $wp_query->query_vars['customized-art'] ) && ($wp_query->query_vars['customized-art'] == 'pdf-print' || $wp_query->query_vars['customized-art'] == 'pdf-print-download') ){
		// var_dump($wp_query->query_vars); exit;
		$user = wp_get_current_user();
		$filename = $user->ID.'-custom-art-list-'.date("Ymd_His").'.pdf';
		// instantiate and use the dompdf class
		$dompdf = new Dompdf();
		$dompdf->set_option('isRemoteEnabled', true);

		$html = stak_print_html_pdf();
		$dompdf->loadHtml($html);

		// (Optional) Setup the paper size and orientation
		$paper_size = defined(DOMPDF_DEFAULT_PAPER_SIZE) ? DOMPDF_DEFAULT_PAPER_SIZE : 'A4';
		$dompdf->setPaper($paper_size, 'portrait');
		// Render the HTML as PDF
		$dompdf->render();

		if($wp_query->query_vars['customized-art'] == 'pdf-print-download'){
			// Output the generated PDF to Browser
			$dompdf->stream($filename);
		}elseif($wp_query->query_vars['customized-art'] == 'pdf-print'){
			// output the pdf result
			header('Content-type: application/pdf');
			header('Content-Disposition: inline; filename="'.$filename.'"');
			header('Content-Transfer-Encoding: binary');
			// header('Content-Length: ' . filesize($saved));
			header('Accept-Ranges: bytes');
			print($dompdf->output());
		}
		die();
	}
}

function stak_print_html_pdf()
{
	$upload_dir = wp_upload_dir();
	// var_dump($upload_dir);
	// print_r('<img src="'.$upload_dir['baseurl'].'/2019/10/imagelogo.png">'); exit;
	//  exit;
	$user = wp_get_current_user();
	// var_dump($user); exit;
	$return = '';
	$return .= '<head>';
	$return .= '<title>Custom Art Collection - '.$user->user_login.'</title>';
	$return .= '
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="'.get_stylesheet_directory().'/customizer/libs/css/bootstrap/bootstrap.min.css?ver='.THEME_VERSION.'">
	<link rel="stylesheet" type="text/css" href="'.get_stylesheet_directory().'/css/pdf-styles.css?ver='.THEME_VERSION.'">
	';
	$return .= '</head>';

	$return .= '<body>';
	$return .= '<header>';
	// $return .= '<div class="row"><div class="col-12">';
	$return .= '';
	$return .= '<h2 class="page-title"><img src="'.$upload_dir['basedir'].'/2019/10/imagelogo.png" class="mr-2" style="width: 35px;"></h2>';
	// $return .= '</div></div>';
	$return .= '</header>';
	$return .= '<footer>';
	$return .= '<p>COPYRIGHT &copy; '.date("Y").' RED T GROUP</p>';
	$return .= '</footer>';
	$return .= '<div class="container-fluid pdf-body">';

	$return .= '<div class="row"><div class="col-12">';
	$return .= '';
	$return .= '<h3 class="title">Custom Art Collection - '.$user->user_login.'</h3>';
	$return .= '
		<p class="sub-title">
			Your collection of saved customised art(s).<br>
			<a href="mailto:'.get_bloginfo('admin_email').'" target="_blank">
				Contact us
			</a>
			for more information.
		</p>
	';
	$return .= '</div></div>';

	$return .= '<div class="row"><div class="col-12">';
	$return .= '<div class="table-responsive">';
	$return .= '<table id="custom-art" class="table table-sm table-striped table-bordered">';
	$return .= '
		<thead class="thead-dark">
			<tr>
				<th style="width: 45px;text-align: center;">#</th>
				<th style="width: 120px;">Image</th>
				<th style="width: 320px;">Name</th>
				<th>Price</th>
			</tr>
		</thead>
	';
	$return .= '<tbody>';
	$user = wp_get_current_user();
	$args = [
		'post_type' => 'custom-art',
		'meta_query' => [
			[
				'key' => 'user_id',
				'value' => $user->ID,
			]
		],
	];
	$query = new WP_Query( $args );
	// var_dump($query);
	if($query->have_posts()){
		$index = 0;
		while($query->have_posts()){
			$index++;
			$query->the_post();

			$return .= '<tr>';
			$post_id = get_the_id();
			// var_dump($post_id);
			$metas = get_post_meta($post_id);
			// var_dump($metas);
			$return .= '<td>'.$index.'</td>';
			// var_dump($upload_dir);
			// var_dump($metas['cropped_img'][0]);
			$img_path = str_replace($upload_dir['baseurl'], $upload_dir['basedir'], $metas['cropped_img'][0]);
			$return .= '<td><img src="'.$img_path.'" width="100" style="width: 100px;"></td>';
			$product = wc_get_product( $metas['product_id'][0] );
			$title_arr = explode(' - ', $product->get_name());
			$return .= '<td>';
			$return .= '<div class="product-form">';
			$return .= '<p style="margin-bottom: 5px;">'.$title_arr[1].'</p>';
			$return .= '<h4 style="margin-top: 5px;">'.$title_arr[0].'</h4>';
			$return .= '<ul>';
			$return .= '<li><strong>Width</strong>: '.number_format(floatval( $metas['custom_width'][0] ), 1).' cm</li>';
			$return .= '<li><strong>Height</strong>: '.number_format(floatval( $metas['custom_height'][0] ), 1).' cm</li>';
			$return .= '<li><strong>Medium</strong>: '.$metas['medium'][0].'</li>';
			$return .= '</ul>';
			$return .= '<div class="product-form">';
			$return .= '</td>';

			$price = floatval( calculate_custom_price( floatval( $metas['custom_width'][0] )*floatval( $metas['custom_height'][0] ) ) );
			if(get_woocommerce_currency_symbol() != 'HK $'){
				if(is_plugin_active('woocommerce-currency-switcher/index.php')){
					$woocs = get_option('woocs');
					$rate = floatval($woocs['USD']['rate']);
					$price = $price*$rate;
				}
			}
			
			$return .= '<td style="text-align: right;">'.get_woocommerce_currency_symbol().' '.number_format($price, 2).'</td>';
			$return .= '</tr>';
		}
	}else{
		$return .= '<tr><td colspan="4">';
		$return .= '<div class="alert alert-warning text-center">You donot have any saved customised art yet.</div>';
		$return .= '</td></tr>';
	}
	// exit;
	
	$return .= '</tbody>';
	$return .= '</table>';
	$return .= '</div>';
	$return .= '</div></div>';

	$return .= '<div class="row"><div class="col-12">';
	$return .= '<hr>';
	$return .= '<h5>User information:</h5>';
	$return .= '<p>Name: <strong>'.$user->display_name.'</strong></p>';
	$return .= '<p>Email: <strong>'.$user->user_email.'</strong></p>';
	$order_args = [
		'orderby' => 'date',
		'order' => 'DESC',
	];
	$orders = wc_get_orders($order_args);
	// var_dump($orders); exit;
	$return .= '<p>Purchase record: <strong>'.count($orders).' purchase(s)</strong></p>';
	
	$return .= '</div></div>';
	$return .= '</div>';

	$return .= '</body>';

	// print_r($return); exit;

	return $return;
}