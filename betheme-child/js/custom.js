(function( $ ) {
	'use strict';
	$('input[autofill="off"]').disableAutofill();
	function reset_login_btn()
	{
		$('button.woocommerce-form-login__submit').removeAttr('disabled');
		$('button.woocommerce-form-login__submit').removeClass('disabled');
		$('button.woocommerce-form-login__submit').html('LOGIN');
	}

	function reset_reg_btn()
	{
		var reg_btn = $('button[name="register"]');
		reg_btn.removeAttr('disabled');
		reg_btn.removeClass('disabled');
		reg_btn.html('REGISTER');
	}

	function disable_reg_btn()
	{
		var reg_btn = $('button[name="register"]');
		reg_btn.attr('disabled', 'disabled');
		reg_btn.addClass('disabled');
	}

	function get_countries_options()
	{
		$.ajax({
			url: stak_js_obj.ajax_url,
			type: 'GET',
			data: {action: 'stak_fetch_phone_codes'},
			dataType: 'html',
			success: function(html) {
				console.log(html);
				$('#billing_wooccm11').html(html);
				var _country = $('#billing_country').val();
				console.log(_country);
				$('#billing_wooccm11 option[name="'+_country+'"]').prop('selected', true);
			},
			error: function(errorThrown){
				console.log(errorThrown);
			}
		});
	}


	$(function(){
		console.log(stak_js_obj.ajax_url);
		if($('#billing_wooccm11').length)
			get_countries_options();

		$(document).on('change', 'input#reg_password', function(){
			setTimeout(function(){
				if($(this).attr('autofill') == 'off'){
					$(this).val('');
					$(this).removeAttr('value');
					$(this).attr('autofill', 'stak-off');
					disable_reg_btn();
				}
			}, 500);
			
		});

		$(document).on('change', '#billing_country', function(){
			var _country = $(this).val();
			console.log(_country);
			$('#billing_wooccm11 option[name="'+_country+'"]').prop('selected', true);
		});

		$('body').on('xoo_wsc_cart_updated', function(){
			$('html, body').stop();
		});
		$(document).on('click', '.xoo-wsc-chng' ,function(){
			$(document).ajaxSuccess(function(e){
				console.log(e);
				$('html, body, div.xoo-wsc-body').stop();
			});
		});

		$(document).on('submit', 'form.woocommerce-form-login', function(e){
			e.preventDefault();
			$('#login-result').slideUp();
			$('button.woocommerce-form-login__submit').attr('disabled', 'disabled');
			$('button.woocommerce-form-login__submit').addClass('disabled');
			$('button.woocommerce-form-login__submit').html('<i class="fas fa-cog fa-spin"></i> LOGGING IN ...');

			var form = $(this);
			var error;
			var username = form.find("#username");
			var password = form.find("#password");
			
			var data = form.serializeArray();
			data.push({name: "action", value: 'stak_ajax_login'});
			// console.log(data);

			if( username.val() === '' ){
				$('#login-result #msg.message').addClass('error');
				$('#login-result #msg.message').html('<p>Your username/email is empty!</p>');
				$('#login-result').slideDown();
				reset_login_btn();
				return false;
			}else{
				$('#login-result #msg.message').removeClass('error');
				$('#login-result #msg.message').html('');
			}	
			if(password.val() == '' ){
				$('#login-result #msg.message').addClass('error');
				$('#login-result #msg.message').html('<p>Your password is empty!</p>');
				$('#login-result').slideDown();
				reset_login_btn();
				return false;
			}else {
				$('#login-result #msg.message').removeClass('error');
				$('#login-result #msg.message').html('');
			}

			$.ajax({
				url: stak_js_obj.ajax_url,
				type: 'POST',
				dataType: 'json',
				data: data,
				success: function(data){
					reset_login_btn();
					$('#login-result #msg.message').addClass(data.status);
					$('#login-result #msg.message').html('<p>'+data.message+'</p>');
					$('#login-result').slideDown();
					if(data.status != 'success'){
						reset_login_btn();
						return false;
					}else{
						if(data.redirect != ''){
							if(form.hasClass('in-modal')){
								location.reload();
							}else{
								window.location = data.redirect;
							}
						}
						$('#login-result').delay(3000).slideUp(function(){
							reset_login_btn();
						});
					}
				},
				error: function (jqXHR, exception) {
					reset_login_btn();
					$('#login-result #msg.message').addClass('error');
					$('#login-result #msg.message').html('<p>'+jqXHR.responseText+'</p>');
					$('#login-result').slideDown();
				}
			});
		});
		
	});

	// register form.
	$(document).on("submit", '.woocommerce-form-register', function(e){
		e.preventDefault();
		$('#register-result').slideUp();
		var reg_btn = $('button[name="register"]');
		reg_btn.attr('disabled', 'disabled');
		reg_btn.addClass('disabled');
		reg_btn.html('<i class="fas fa-cog fa-spin"></i> PROCESSING DATA ...');

		var form = $(this);
		var error;
		var reg_email = form.find("#reg_email");
		var reg_password = form.find("#reg_password");
		var agreed = $('#agree-toc').is(':checked');

		var data = form.serializeArray();
		data.push({name: "action", value: 'stak_ajax_register'});

		if( reg_email.val() === '' ){
			$('#register-result #msg.message').addClass('error');
			$('#register-result #msg.message').html('<p>Email address is required.</p>');
			$('#register-result').slideDown();
			reset_reg_btn();
			return false;
		}else{
			$('#register-result #msg.message').removeClass('error');
			$('#register-result #msg.message').html('');
		}	
		if(reg_password.val() == '' ){
			$('#register-result #msg.message').addClass('error');
			$('#register-result #msg.message').html('<p>Password is required.</p>');
			$('#register-result').slideDown();
			reset_reg_btn();
			return false;
		}else {
			$('#register-result #msg.message').removeClass('error');
			$('#register-result #msg.message').html('');
		}

		if(!agreed){
			$('#register-result #msg.message').addClass('error');
			$('#register-result #msg.message').html('<p>You need to agree to our terms and conditions.</p>');
			$('#register-result').slideDown();
			reset_reg_btn();
			return false;
		}

		$.ajax({
			url: stak_js_obj.ajax_url,
			type: 'POST',
			dataType: 'json',
			data: data,
			success: function(data){
				reset_login_btn();
				$('#register-result #msg.message').addClass(data.status);
				$('#register-result #msg.message').html('<p>'+data.message+'</p>');
				$('#register-result').slideDown();
				if(data.status != 'success'){
					reset_reg_btn();
					return false;
				}else{
					if(data.redirect != ''){
						window.location = data.redirect;
					}
					$('#register-result').delay(3000).slideUp(function(){
						reset_reg_btn();
					});
				}
			},
			error: function (jqXHR, exception) {
				reset_login_btn();
				$('#register-result #msg.message').addClass('error');
				$('#register-result #msg.message').html('<p>'+jqXHR.responseText+'</p>');
				$('#register-result').slideDown();
				reset_reg_btn();
			}
		});
	});
})( jQuery );