<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css" crossorigin="anonymous">
<style type="text/css">
	/*
	Step 1
	======
	Style your page (the product list)
	*/
	.product {
		float: left;
		margin: 10px;
	}

	.product a[data-fancybox] {
		color: #292929;
	}

	.product .product-images,
	.product .product-form {
		display: none;
	}

	/*
	Step 2
	======
	Reposition and redesign fancyBox blocks
	*/
	/* This elements contains both blocks */
	.fancybox-inner {
		position: absolute;
		top: 0;
		right: 0;
		bottom: 0;
		left: 0;
		margin: auto;
		width: calc(100% - 40px);
		height: calc(100% - 40px);
		max-width: 800px;
		max-height: 600px;
	}

	/* Left block will contain the gallery */
	.fancybox-stage {
		width: 52%;
		background: #fff;
	}

	/* Right block - close button and the form */
	.fancybox-form-wrap {
		position: absolute;
		top: 40px;
		right: 0;
		bottom: 40px;
		width: 48%;
		background: #fff;
	}

	/* Add vertical lines */
	.fancybox-form-wrap::before,
	.fancybox-form-wrap::after {
		content: '';
		position: absolute;
		top: 0;
		left: 0;
		bottom: 0;
	}

	.fancybox-form-wrap::before {
		width: 8px;
		background: #f4f4f4;
	}

	.fancybox-form-wrap::after {
		width: 1px;
		background: #e9e9e9;
	}

	/* Set position and colors for close button */
	.fancybox-button--close {
		border-width: 0;
		position: absolute;
		top: 0;
		right: 0;
		background: #F0F0F0;
		color: #222;
		padding: 7px;
	}

	.fancybox-button:hover {
		color: #111;
		background: #e4e4e4;
	}

	.fancybox-button svg path {
		stroke-width: 1;
	}

	/* Set position of the form */
	.fancybox-inner .product-form {
		overflow: auto;
		position: absolute;
		top: 50px;
		right: 0;
		bottom: 50px;
		left: 0;
		padding: 0 50px;
		text-align: center;
	}

	/*
	Step 3
	======
	Tweak fade animation
	*/
	.fancybox-inner {
		opacity: 0;
		transition: opacity .3s;
	}

	.fancybox-is-open .fancybox-inner {
		opacity: 1;
	}

	.fancybox-is-closing .fancybox-fx-fade {
		opacity: 1 !important;
		/* Prevent double-fading */
	}

	/*
	Step 2
	======
	Bullet navigation design
	*/
	.product-bullets {
		display: none;
		list-style: none;
		position: absolute;
		bottom: 0;
		left: 0;
		width: 100%;
		text-align: center;
		margin: 0;
		padding: 0;
		z-index: 99999;
		-webkit-tap-highlight-color: rgba(0, 0, 0, 0);
	}

	.product-bullets li {
		display: inline-block;
		vertical-align: top;
	}

	.product-bullets li a {
		display: block;
		height: 30px;
		width: 20px;
		position: relative;
	}

	.product-bullets li a span {
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
		width: 10px;
		height: 10px;
		border-radius: 99px;
		text-indent: -99999px;
		overflow: hidden;
		background: #fff;
		box-shadow: 0px 0px 2px rgba(0, 0, 0, 0.5);
	}

	.product-bullets li.active a span {
		background: #FF6666;
	}
	#custom-art td {
		font-size: 12px;
		padding: 0.7rem;
	}
</style>

<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<h3 class="title">Saved Art</h3>
			<p class="sub-title">
				Here you will find all your saved customised art.
				<span class="pull-right" style="color: #dd3333;">
					Printer version
					<a href="<?php echo $pdf_links['view']; ?>" target="_blank">
						<i class="far fa-file-pdf ml-2" style="margin-left: 1.5rem;"></i>
					</a>
					<a href="<?php echo $pdf_links['download']; ?>" target="_blank">
						<i class="fas fa-download ml-2" style="margin-left: 1.5rem;"></i>
					</a>
				</span>
			</p>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<div class="table-responsive">
				<table id="custom-art" class="table table-sm">
					<thead class="thead-dark">
						<tr>
							<th>Image</th>
							<th style="width: 45%;">Name</th>
							<th>Price</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$user = wp_get_current_user();
						$args = [
							'post_type' => 'custom-art',
							'meta_query' => [
								[
									'key' => 'user_id',
									'value' => $user->ID,
								]
							],
						];
						$query = new WP_Query( $args );
						// var_dump($query);
						if($query->have_posts()){
							$index = 0;
							while($query->have_posts()){
								$index++;
								$query->the_post();
								?>
								<tr>
									<?php
									$post_id = get_the_id();
									// var_dump($post_id);
									$metas = get_post_meta($post_id);
									// var_dump($metas);
									?>
									<td>
										<img src="<?php echo $metas['cropped_img'][0] ?>" width="50" style="width: 50px;">
									</td>
									<td>
										<div class="product" style="margin-top: 0!important; text-align: left;">
											<?php
											$product = wc_get_product( $metas['product_id'][0] );
											// var_dump($product);
											$sizes = '<br>'.number_format(floatval( $metas['custom_width'][0] ), 1).' x '.number_format(floatval( $metas['custom_height'][0] ), 1).' cm';
											$product_link = sprintf( '<a data-fancybox="quick-view-%s" href="%s" data-caption="%s" data-type="image">%s</a>', $index, esc_url( $metas['cropped_img'][0] ), $product->get_name(), $product->get_name().$sizes );
											
											echo $product_link;
											?>
											<div class="product-images"></div>
											<div class="product-form">
												<div class="quick-view-mobile-img">
													<img src="<?php echo $metas['cropped_img'][0]; ?>">
												</div>
												<?php $title_arr = explode(' - ', $product->get_name()); ?>
												<p><?php echo $title_arr[1]; ?></p>
												<h4> <?php echo $title_arr[0]; ?></h4>
												<p><em>CUSTOM</em></p>
												<ul>
													<li><strong>Width</strong>: <?php echo number_format(floatval( $metas['custom_width'][0] ), 1); ?> cm</li>
													<li><strong>Height</strong>: <?php echo number_format(floatval( $metas['custom_height'][0] ), 1); ?> cm</li>
													<li><strong>Medium</strong>: <?php echo $metas['medium'][0]; ?></li>
												</ul>
											</div>
										</div>
									</td>
									<td class="text-right">
										<span class="woocommerce-currency-symbol"><?php echo get_woocommerce_currency_symbol(); ?></span>
										<?php
										$price = floatval( calculate_custom_price( floatval( $metas['custom_width'][0] )*floatval( $metas['custom_height'][0] ) ) );
										if(get_woocommerce_currency_symbol() != 'HK $'){
											if(is_plugin_active('woocommerce-currency-switcher/index.php')){
												$woocs = get_option('woocs');
												$rate = floatval($woocs['USD']['rate']);
												$price = $price*$rate;
											}
										}

										echo number_format($price, 2);
										?>
									</td>
									<td>
										<form id="form-<?php echo $post_id; ?>" class="add-to-cart-custom-art">
											<input type="hidden" name="action" value="stak_custom_art_add_to_cart">
											<input type="hidden" name="art_id" value="<?php echo $post_id; ?>">
											<?php 
											foreach ($metas as $key => $val) {
												?>
												<input type="hidden" name="<?php echo $key; ?>" value="<?php echo $val[0] ?>">
												<?php 
											} 
											?>
											<button type="submit" id="add-to-cart" class="btn-stak black">ADD TO CART</button>
										</form>
										<button type="button" class="btn-del-custom-art btn-stak red mt-2" data-post="<?php echo $post_id; ?>" data-remove="post">
											<i class="fas fa-trash mr-2"></i> DELETE
										</button>
									</td>
								</tr>
								<?php
							}
						}else{
							?>
							<tr>
								<td colspan="3">
									<div class="alert alert-warning">
										You have no saved customized art. <a href="/gallery">Create One</a>!
									</div>
								</td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/customizer/libs/js/bootstrap/bootstrap.bundle.min.js?ver=<?php echo THEME_VERSION; ?>"></script>
<script type="text/javascript">
	(function( $ ) {
		$(function(){
			$('body').removeClass('style-simple');

			$('table#custom-art').on('click', '.btn-del-custom-art', function(e){
				e.preventDefault();
				var data = $(this).data();
				$(this).attr('disabled', 'disabled');
				$(this).addClass('disabled');
				$(this).html('<i class="fas fa-cog fa-spin mr-2"></i> DELETING...');
				$.ajax({
					url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
					type: 'POST',
					data: {
						action : 'stak_delete_custom_art',
						post_id : data.post,
						remove : data.remove
					},
				})
				.done(function(json) {
					var obj = $.parseJSON(json);
					console.log(obj);
					if(obj.status === 'error'){
						alert(obj.message);
					}

					if(obj.status === 'ok'){
						alert('deleted!');
						location.reload();
					}
				})
				.fail(function(error) {
					console.log(error);
				})
				.always(function() {
					console.log("complete");
				});
			});

			$('table#custom-art').on('submit', 'form.add-to-cart-custom-art', function(e){
				e.preventDefault();
				var $button = $(this).children('button#add-to-cart');
				$button.attr('disabled', 'disabled');
				$button.addClass('disabled');
				$button.html('<i class="fas fa-cog fa-spin mr-2"></i> ADDING...');
				var formdata = $(this).serialize();
				console.log(formdata);
				$.ajax({
					url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
					type: 'POST',
					data: formdata,
				})
				.done(function(json) {
					var obj = $.parseJSON(json);
					console.log(obj);
					if(obj.status === 'error'){
						alert(obj.message);
					}

					if(obj.status === 'ok'){
						alert('added to cart');
						location.reload();
					}
				})
				.fail(function(error) {
					console.log(error);
				})
				.always(function() {
					console.log("complete");
				});
			});

			$('[data-fancybox^="quick-view"]').fancybox({
				animationEffect: "fade",
				animationDuration: 700,
				margin: 0,
				gutter: 0,
				touch: {
					vertical: false
				},
				baseTpl: '<div class="fancybox-container" role="dialog" tabindex="-1">' +
					'<div class="fancybox-bg"></div>' +
					'<div class="fancybox-inner">' +
					'<div class="fancybox-stage quick-view"></div>' +
					'<div class="fancybox-form-wrap">' +
					'<button data-fancybox-close class="fancybox-button fancybox-button--close" title="{{CLOSE}}">' +
					'<svg viewBox="0 0 40 40">' +
					'<path d="M10,10 L30,30 M30,10 L10,30" />' +
					'</svg>' +
					'</button></div>' +
					'</div>' +
					'</div>',
				onInit: function(instance) {

					/*

						#1 Add product form
						===================

					*/

					// Find current form element ..
					var current = instance.group[instance.currIndex];
					instance.$refs.form = current.opts.$orig.parent().find('.product-form');

					// .. and move to the container
					instance.$refs.form.appendTo(instance.$refs.container.find('.fancybox-form-wrap'));

					/*

						#2 Create bullet navigation links
						=================================

					*/
					var list = '',
						$bullets;

					for (var i = 0; i < instance.group.length; i++) {
						list += '<li><a data-index="' + i + '" href="javascript:;"><span>' + (i + 1) + '</span></a></li>';
					}

					$bullets = $('<ul class="product-bullets">' + list + '</ul>').on('click touchstart', 'a', function() {
						var index = $(this).data('index');

						$.fancybox.getInstance(function() {
							this.jumpTo(index);
						});

					});

					instance.$refs.bullets = $bullets.appendTo(instance.$refs.stage);

				},
				beforeShow: function(instance) {

					// Mark current bullet navigation link as active
					instance.$refs.stage.find('ul:first')
						.children()
						.removeClass('active')
						.eq(instance.currIndex)
						.addClass('active');

				},
				afterClose: function(instance, current) {

					// Move form back to the place
					instance.$refs.form.appendTo(current.opts.$orig.parent());

				}
			});
		});
	})( jQuery );
</script>