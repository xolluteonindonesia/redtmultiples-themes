<?php
/**
 * Betheme Child Theme
 *
 * @package Betheme Child Theme
 * @author Muffin group
 * @link https://muffingroup.com
 */

/**
 * Child Theme constants
 * You can change below constants
 */

// white label

define('WHITE_LABEL', false);

require_once('config.php');

// disable admin bar.
add_filter('show_admin_bar', '__return_false');

/**
  * Edit my account menu order
  */

  function my_account_menu_order() {
	$menuOrder = array(
		'collections'		=> __( 'My Wishlist', 'woocommerce' ),
		'dashboard'			=> __( 'Dashboard', 'woocommerce' ),
		'customized-art'	=> __( 'Saved Art', 'woocommerce' ),
		'orders'			=> __( 'Orders', 'woocommerce' ),
		'edit-address'		=> __( 'Addresses', 'woocommerce' ),
		'edit-account'		=> __( 'Account Details', 'woocommerce' ),
		'customer-logout'	=> __( 'Logout', 'woocommerce' ),
	);
	return $menuOrder;
}

add_filter ( 'woocommerce_account_menu_items', 'my_account_menu_order' );

require_once('stak-custom-art.php');

/**
 * Enqueue Styles
 */
function admin_style() {
	wp_enqueue_style('admin-styles', get_stylesheet_directory_uri() . '/css/admin-style.css');
}
add_action('admin_enqueue_scripts', 'admin_style');

function mfnch_enqueue_styles()
{
	// enqueue the parent stylesheet
	// however we do not need this if it is empty
	// wp_enqueue_style('parent-style', get_template_directory_uri() .'/style.css');

	// enqueue the parent RTL stylesheet

	if (is_rtl()) {
		wp_enqueue_style('mfn-rtl', get_template_directory_uri() . '/rtl.css');
	}

	// enqueue the child stylesheet
	wp_dequeue_style( 'style' );
	wp_deregister_style( 'style' );

	// <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	wp_register_style( 'bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css', false, '3.4.0' );
	wp_enqueue_style( 'bootstrap' );
	// wp_enqueue_script( 'bootstrap', '//stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js', [], '3.4.1', true );
	wp_register_style( 'fontawesome', '//use.fontawesome.com/releases/v5.11.2/css/all.css', false, CHILD_THEME_VERSION );
	wp_enqueue_style( 'fontawesome' );
	wp_register_style( 'style', get_stylesheet_directory_uri() .'/style.css', false, CHILD_THEME_VERSION );
	wp_enqueue_style( 'style' );

	// adding the fancybox here.
	// var_dump(is_page( 'cart' )); exit;
	$load_script = is_singular( 'product' ) || is_page( 'my-account' ) || is_page( 'checkout' ) || is_page( 'cart' ) || is_page( 'wishlist' );
	// var_dump($load_script); exit;
	if( $load_script ){
		// var_dump('product page'); exit;
		wp_register_style( 'fancybox', get_stylesheet_directory_uri().'/customizer/libs/js/fancybox/jquery.fancybox.min.css', false, CHILD_THEME_VERSION );
		wp_enqueue_style( 'fancybox' );
		wp_enqueue_script( 'fancybox', get_stylesheet_directory_uri().'/customizer/libs/js/fancybox/jquery.fancybox.js', [], CHILD_THEME_VERSION, true );
		wp_enqueue_script( 'number', get_stylesheet_directory_uri().'/js/jquery.number.min.js', [], CHILD_THEME_VERSION, true );
		wp_enqueue_script( 'disable-autofill', get_stylesheet_directory_uri().'/js/jquery.disable-autofill.js', [], CHILD_THEME_VERSION, true );
		wp_enqueue_script( 'custom', get_stylesheet_directory_uri().'/js/custom.js', [], CHILD_THEME_VERSION, true );
		wp_localize_script( 'custom', 'stak_js_obj', [ 'ajax_url' => admin_url( 'admin-ajax.php') ] );
	}
}
add_action('wp_enqueue_scripts', 'mfnch_enqueue_styles', 101);

/**
 * Load Textdomain
 */

function mfnch_textdomain()
{
	load_child_theme_textdomain('betheme', get_stylesheet_directory() . '/languages');
	load_child_theme_textdomain('mfn-opts', get_stylesheet_directory() . '/languages');
}
add_action('after_setup_theme', 'mfnch_textdomain');

/**
 *
 * All login, registration and logout customization is here.
 * return @param
 *
 */
function filter_plugin_updates( $value ) {
	unset( $value->response['akismet/akismet.php'] );
	unset( $value->response['customer-email-verification-for-woocommerce/customer-email-verification-for-woocommerce.php'] );
	return $value;
}
add_filter( 'site_transient_update_plugins', 'filter_plugin_updates' );

add_action( 'wp_ajax_nopriv_stak_ajax_login', 'stak_ajax_login' );
add_action( 'wp_ajax_stak_ajax_login', 'stak_ajax_login' );

function stak_ajax_login()
{
	// var_dump($_POST);
	$no_verification = NOT_NEED_TO_VERIFY;
	// var_dump($no_verification);
	if(!isset($_POST['username']) || $_POST['username'] == ''){
		echo json_encode([
			'status' => 'error',
			'message' => 'username is empty!',
			'redirect' => ''
		]);
		exit;
	}
	if(!isset($_POST['password']) || $_POST['password'] == ''){
		echo json_encode([
			'status' => 'error',
			'message' => 'password is empty!',
			'redirect' => ''
		]);
		exit;
	}

	$params = [];
	$params['user_login'] = wc_clean($_POST['username']);
	$params['user_password'] = $_POST['password'];
	$params['remember'] = false;
	if( isset($_POST['rememberme'])){
		$params['remember'] = true;
	}

	$user_by_email = get_user_by( 'email', $params['user_login'] );
	$user_by_username = get_user_by( 'login', $params['user_login'] );

	if($user_by_email){
		$user_id = $user_by_email->ID;
	}

	if($user_by_username){
		$user_id = $user_by_username->ID;
	}

	if($user_id){
		$caps = get_user_meta($user_id, 'wp_capabilities', true);
		$roles = array_keys((array)$caps);
		// var_dump($roles);
		$_verifying = true;
		foreach($roles as $role){
			// var_dump(in_array($role, $no_verification));
			if(in_array($role, $no_verification)){
				$_verifying = false;
				continue;
			}
		}

		// var_dump($_verifying); exit;
		if($_verifying){
			$_url = get_home_url().'/my-account';
			if(class_exists('zorem_woo_customer_email_verification')){
				// var_dump(get_user_meta( $user_id, 'customer_email_verified', true )); exit;
				if('true' !== get_user_meta( $user_id, 'customer_email_verified', true )){
					$_url = add_query_arg( array(
							'cev_confirmation_resend' => base64_encode( $user_id ),
							), get_home_url().'/my-account' );
					// var_dump($_url); exit;
					echo json_encode([
						'status' => 'error',
						'message' => 'Error! your email address is not verified yet.<br><a href="'.$_url.'">Resend verification mail</a>?',
						'redirect' => ''
					]);
					exit;
				}else{
					$logged_in = wp_signon( $params, false );
					// var_dump($logged_in); exit;
					if ( is_wp_error($logged_in) ){
						$error_string = $logged_in->get_error_message();
						echo json_encode([
							'status' => 'error',
							'message' => $error_string,
							'redirect' => ''
						]);
						exit;
					}else{
						wp_set_current_user($logged_in->ID);
						wp_set_auth_cookie($logged_in->ID);
						echo json_encode([
							'status' => 'success',
							'message' => 'You have successfully logged in, we will redirect in you in a moment.',
							'redirect' => get_home_url().'/my-account',
						]);
						exit;
					}
				}
			}else{
				$logged_in = wp_signon( $params, false );
				// var_dump($logged_in);
				if ( is_wp_error($logged_in) ){
					$error_string = $logged_in->get_error_message();
					echo json_encode([
						'status' => 'error',
						'message' => $error_string,
						'redirect' => ''
					]);
					exit;
				}else{
					wp_set_current_user($logged_in->ID);
					wp_set_auth_cookie($logged_in->ID);
					echo json_encode([
						'status' => 'success',
						'message' => 'You have successfully logged in, we will redirect in you in a moment.',
						'redirect' => get_home_url().'/my-account',
					]);
					exit;
				}
			}
			
			
		}else{
			$logged_in = wp_signon( $params, false );
			// var_dump($logged_in);
			if ( is_wp_error($logged_in) ){
				$error_string = $logged_in->get_error_message();
				echo json_encode([
					'status' => 'error',
					'message' => $error_string,
					'redirect' => ''
				]);
				exit;
			}else{
				wp_set_current_user($logged_in->ID);
				wp_set_auth_cookie($logged_in->ID);
				echo json_encode([
					'status' => 'success',
					'message' => 'You have successfully logged in, we will redirect in you in a moment.',
					'redirect' => get_home_url().'/my-account',
				]);
				exit;
			}
		}
	}else{
		echo json_encode([
			'status' => 'error',
			'message' => 'username or email address is not found!',
			'redirect' => ''
		]);
		exit;
	}
}

add_action( 'wp_ajax_nopriv_stak_ajax_register', 'stak_ajax_register' );
add_action( 'wp_ajax_stak_ajax_register', 'stak_ajax_register' );

function stak_ajax_register()
{
	// First check the nonce, if it fails the function will break
	check_ajax_referer( 'woocommerce-register', 'woocommerce-register-nonce' );

	$generate_password = get_option( 'woocommerce_registration_generate_password' );

	if ( ! empty( $_POST['reg_email'] ) && ! empty( $_POST['reg_password'] ) ) {
		// var_dump($_POST); exit;
		$username = 'no' === get_option( 'woocommerce_registration_generate_username' ) ? $_POST['username'] : '';
		$password = 'no' === get_option( 'woocommerce_registration_generate_password' ) ? $_POST['reg_password'] : '';
		$email    = $_POST['reg_email'];
		
		$validation_error = new WP_Error();
		$validation_error = apply_filters( 'woocommerce_process_registration_errors', $validation_error, $username, $password, $email );
		// var_dump($validation_error); exit;

		if($validation_error->get_error_code() && !empty($validation_error->errors)){
			// var_dump($validation_error->get_error_code()); exit;
			echo json_encode([
				'status' => 'error',
				'code' => $validation_error->get_error_code(),
				'message' => $validation_error->get_error_message(),
				'redirect' => ''
			]);
			exit;
		}else{
			// var_dump(class_exists('zorem_woo_customer_email_verification')); exit;

			$new_customer = wc_create_new_customer( sanitize_email( $email ), wc_clean( $username ), $password );
			if(is_wp_error( $new_customer )) {
				echo json_encode([
					'status' => 'error',
					'code' => $new_customer->get_error_code(),
					'message' => $new_customer->get_error_message(),
					'redirect' => ''
				]);
				exit;
			}else{
				// var_dump($new_customer); exit;
				// if ( apply_filters( 'woocommerce_registration_auth_new_customer', true, $new_customer ) ) {
				// 	wc_set_customer_auth_cookie( $new_customer );
				// }

				if(isset($_POST['_mc4wp_subscribe_wp-registration-form']) & $_POST['_mc4wp_subscribe_wp-registration-form']){
					$mc4wp = mc4wp_get_api();
					try {
						$mc4wp->subscribe(MAILCHIMP_LIST_ID, $email);
					} catch (Exception $e) {
						var_dump($e->getMessage()); exit;
					}
					
				}

				if (class_exists('zorem_woo_customer_email_verification')) {
					$cev_enter_account_after_registration = get_option('cev_enter_account_after_registration', 0);
					// var_dump($cev_enter_account_after_registration); exit;
					if($cev_enter_account_after_registration == 0){
						$redirect = add_query_arg( array( 'cev' => base64_encode( $new_customer ), ), $redirect );
						// var_dump($redirect); exit;
						// wp_logout();
						$redirect = get_site_url().'/my-account'.$redirect;
						echo json_encode([
							'status' => 'success',
							'code' => 200,
							'message' => 'Account created successfully. redirecting...',
							'redirect' => $redirect
						]);
						exit;
					}else{
						$redirect = get_site_url().'/my-account';
						echo json_encode([
							'status' => 'success',
							'code' => 200,
							'message' => 'Account created successfully. redirecting...',
							'redirect' => $redirect
						]);
						exit;
					}
				}else{
					$redirect = get_site_url().'/my-account';
					echo json_encode([
						'status' => 'success',
						'code' => 200,
						'message' => 'Account created successfully. redirecting...',
						'redirect' => $redirect
					]);
					exit;
				}
			}
		}
	}else{
		if($generate_password == 'yes'){
			if ( empty( $_POST['reg_email']) ){
				echo json_encode([
					'status' => 'error',
					'code' => 403,
					'message' => 'Please fill in all the required information!',
					'redirect' => ''
				]);
				exit;
			}else{
				$username = 'no' === get_option( 'woocommerce_registration_generate_username' ) ? $_POST['username'] : '';				
				$email    = $_POST['reg_email'];					
				$validation_error = new WP_Error();
				$validation_error = apply_filters( 'woocommerce_process_registration_errors', $validation_error, $username, $password, $email );

				if($validation_error->get_error_code() && !empty($validation_error->errors)){
					// var_dump($validation_error->get_error_code()); exit;
					echo json_encode([
						'status' => 'error',
						'code' => $validation_error->get_error_code(),
						'message' => $validation_error->get_error_message(),
						'redirect' => ''
					]);
					exit;
				}else{
					$new_customer = wc_create_new_customer( sanitize_email( $email ), wc_clean( $username ), $password );
					if(is_wp_error( $new_customer )) {
						echo json_encode([
							'status' => 'error',
							'code' => $new_customer->get_error_code(),
							'message' => $new_customer->get_error_message(),
							'redirect' => ''
						]);
						exit;
					}else{
						// var_dump($new_customer); exit;
						// if ( apply_filters( 'woocommerce_registration_auth_new_customer', true, $new_customer ) ) {
						// 	wc_set_customer_auth_cookie( $new_customer );
						// }

						if (class_exists('zorem_woo_customer_email_verification')) {
							$cev_enter_account_after_registration = get_option('cev_enter_account_after_registration', 0);
							// var_dump($cev_enter_account_after_registration); exit;
							if($cev_enter_account_after_registration == 0){
								$redirect = add_query_arg( array( 'cev' => base64_encode( $new_customer ), ), $redirect );
								// var_dump($redirect); exit;
								// wp_logout();
								$redirect = get_site_url().'/my-account'.$redirect;
								echo json_encode([
									'status' => 'success',
									'code' => 200,
									'message' => 'Account created successfully. redirecting...',
									'redirect' => $redirect
								]);
								exit;
							}else{
								$redirect = get_site_url().'/my-account';
								echo json_encode([
									'status' => 'success',
									'code' => 200,
									'message' => 'Account created successfully. redirecting...',
									'redirect' => $redirect
								]);
								exit;
							}
						}else{
							$redirect = get_site_url().'/my-account';
							echo json_encode([
								'status' => 'success',
								'code' => 200,
								'message' => 'Account created successfully. redirecting...',
								'redirect' => $redirect
							]);
							exit;
						}
					}
				}
			}
		}else{
			echo json_encode([
				'status' => 'error',
				'code' => 403,
				'message' => 'Please fill in all the required information!',
				'redirect' => ''
			]);
			exit;
		}
	}
}

add_action( 'init', 'registration_init' );
function registration_init()
{
	// var_dump(isset($_GET['cev']) && $_GET['cev'] != ''); exit;
	if(isset($_GET['cev']) && $_GET['cev'] != ''){
		
		if(!wc_has_notice('<strong>Success:</strong> Your account has been created. Please check your email inbox for a verification mail.', 'success'))
			wc_add_notice( __( '<strong>Success:</strong> Your account has been created. Please check your email inbox for a verification mail.', 'stak' ) );
	}

	if(isset($_GET['cev_confirmation_resend']) && $_GET['cev_confirmation_resend'] != ''){
		if(!wc_has_notice('<strong>Success:</strong> Another verification email has been sent. Please check your email inbox.', 'success'))
			wc_add_notice( __( '<strong>Success:</strong> Another verification email has been sent. Please check your email inbox.', 'stak' ) );
	}
}

add_action('template_redirect', 'logout_without_confirm');
function logout_without_confirm()
{
	global $wp;
	// var_dump($wp->query_vars); exit;
	if ( isset( $wp->query_vars['customer-logout'] ) ) {
		// var_dump(home_url('my-account'));
		$logout_url = htmlspecialchars_decode( wp_logout_url( '/my-account' ) );
		// var_dump($logout_url); exit;
		// var_dump(str_replace( '&amp;', '&', $logout_url )); exit;
		// wp_redirect( $logout_url );
		header("Location: ". ( $logout_url ) );
		exit;
	}
}

function stak_login_redirect( $user_login, $user ) {
	// var_dump($user_login); exit;
	return wc_get_page_permalink( 'my-account' );
}
// add_filter( 'wp_login', 'stak_login_redirect' );

function add_cart_item_data( $cart_item_data, $product_id, $variation_id ) {
	$product = wc_get_product( $product_id );
	$price = $product->get_price();
	
	// Has our option been selected?
	if( isset( $_POST['input_frame_price'] ) && $_POST['input_frame_price'] !== 0 ) {
		// Store the overall price for the product, including the cost of the warranty
		$cart_item_data['with_frame_price'] = $price + floatval($_POST['input_frame_price']);
	}
	// var_dump($_POST); exit;
	if( isset( $_POST['calculated_price'] ) && $_POST['calculated_price'] !== '' ){
		$cart_item_data['calculated_price'] = floatval($_POST['calculated_price']);
	}

	return $cart_item_data;
}
add_filter( 'woocommerce_add_cart_item_data', 'add_cart_item_data', 10, 3 );

function before_calculate_totals( $cart_obj ) {
	if ( is_admin() && ! defined( 'DOING_AJAX' ) ) {
		return;
	}

	// Iterate through each cart item
	foreach( $cart_obj->get_cart() as $key=>$value ) {
		if( isset( $value['with_frame_price'] ) ) {
			$price = $value['with_frame_price'];
			$value['data']->set_price( ( $price ) );
		}
		// var_dump($value);
		// var_dump(isset( $value['custom_price']));
		if( isset( $value['custom_price'] ) ) {
			$price = $value['custom_price'];
			$value['data']->set_price( ( $price ) );
		}

		if( isset( $value['calculated_price'] ) ) {
			$price = $value['calculated_price'];
			$value['data']->set_price( ( $price ) );
		}
	}
	// exit;
}
add_action( 'woocommerce_before_calculate_totals', 'before_calculate_totals', 10, 1 );

// Store the custom data to cart object
add_filter( 'woocommerce_add_cart_item_data', 'save_custom_product_data', 10, 2 );
function save_custom_product_data( $cart_item_data, $product_id ) {
	$bool = false;
	$data = array();
	if( isset( $_POST['selected_frame'] ) && $_POST['selected_frame'] ) {
		$cart_item_data['custom_data']['pa_frame'] = $_POST['selected_frame'];
		$data['pa_frame'] = $_POST['selected_frame'];
		$bool = true;
	}
   
	if( $bool ) {
		// below statement make sure every add to cart action as unique line item
		$cart_item_data['custom_data']['unique_key'] = md5( microtime().rand() );
		WC()->session->set( 'custom_variations', $data );
	}
	return $cart_item_data;
}

// Store the medium to cart object
add_filter( 'woocommerce_add_cart_item_data', 'save_medium_to_cart', 30, 2 );
function save_medium_to_cart( $cart_item_data, $product_id ) {
	$bool = false;
	$data = array();
	// var_dump($_REQUEST); exit;
	$request = $_REQUEST;
	if( null !== $request['medium'] && $request['medium'] !== '' ) {
		$cart_item_data['custom_data']['pa_medium'] = $request['medium'];
		$data['pa_medium'] = $request['medium'];
		$bool = true;
	}
   
	if( $bool ) {
		// below statement make sure every add to cart action as unique line item
		$cart_item_data['custom_data']['unique_key'] = md5( microtime().rand() );
		WC()->session->set( 'custom_variations', $data );
	}
	return $cart_item_data;
}

// Store the medium to cart object
add_filter( 'woocommerce_add_cart_item_data', 'save_custom_dimension_to_cart', 30, 2 );
function save_custom_dimension_to_cart( $cart_item_data, $product_id ) {
	$data = array();
	$bool = false;
	$request = $_REQUEST;
	$img_width = 0;
	$img_height = 0;

	// var_dump($_REQUEST);

	// fix bug with 0cm sizes.
	$product = wc_get_product( $product_id );
	$sizes = $product->get_variation_attributes();
	if(is_array($sizes) && count($sizes) > 0){
		$size_str = $sizes['pa_select-size'][0];
		$size_arr = explode('-', $size_str);
		foreach($size_arr as $size){
			if($size[0] !== 'x' && $size[0] !== 'cm')
				$img_width = $size[0];
			if(isset($size[2])){
				if($size[2] !== 'x' && $size[2] !== 'cm')
					$img_height = $size[2];
			}
			
		}
	}

	if( isset($request['width']) && (null !== $request['width'] && $request['width'] !== '') ) {
		$img_width = intval($request['width']);
	}

	if( isset($request['width']) && (null !== $request['height'] && $request['height'] !== '') ) {
		$img_height = intval($request['height']);
	}

	// we pre-populated the customized measurements before adding it to the cart.
	// var_dump($request['action'] == 'stak_custom_art_add_to_cart'); exit;

	if( null !== $request['action'] && $request['action'] == 'stak_custom_art_add_to_cart' ){
		$img_width = 0.0;
		$img_height = 0.0;
		foreach($request as $key => $val){
			$cart_item_data['custom_data']['pa_'.$key] = $val;
			$data['pa_'.$key] = $val;
		}

		if( null !== $request['custom_width'] && $request['custom_width'] !== '' ) {
			$img_width = floatval($request['custom_width']);
		}

		if( null !== $request['custom_height'] && $request['custom_height'] !== '' ) {
			$img_height = floatval($request['custom_height']);
		}

		$price = calculate_custom_price($img_width*$img_height);
		// var_dump($price);
		$cart_item_data['custom_price'] = $price;

		$img_width = number_format($img_width, 1);
		$img_height = number_format($img_height, 1);
		// var_dump($img_width);
	}

	// exit;

	try {
		$cart_item_data['custom_data']['pa_img_width'] = $img_width;
		$cart_item_data['custom_data']['pa_img_height'] = $img_height;
		$data['pa_img_width'] = $img_width;
		$data['pa_img_height'] = $img_height;
		$bool = true;
	} catch (Exception $e) {
		$bool = false;
	}
	
   
	if( $bool ) {
		// below statement make sure every add to cart action as unique line item
		$cart_item_data['custom_data']['unique_key'] = md5( microtime().rand() );
		WC()->session->set( 'custom_variations', $data );
	}
	return $cart_item_data;
}

function stak_print_wishlist_price( $product )
{
	$_price = calculate_custom_price(floatval($product->width)*floatval($product->height));
	
	if(get_woocommerce_currency_symbol() != 'HK $'){
		if(is_plugin_active('woocommerce-currency-switcher/index.php')){
			$woocs = get_option('woocs');
			$rate = floatval($woocs['USD']['rate']);
			$_price = $_price*$rate;
		}
	}
	// var_dump($_price);
	echo '<span class="woocs_price_code" data-product-id="'.$product->ID.'"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">'.get_woocommerce_currency_symbol().'</span>'.number_format($_price, 2).'</span></span>';
}

function stak_print_wl_action_btn( $product, $item )
{
	// var_dump($product);
	$action_product_id = 0;
	$action_permalink = '';

	if($product->is_type('variation')){
		$action_product_id = intval( $product->get_parent_id() );
		$action_permalink = get_permalink( $action_product_id );
		$params = ['varid' => $item->get_product_id()];
		$paramstr = '?'.http_build_query($params);
		$action_permalink = $action_permalink.$paramstr;
		$product = wc_get_product( $action_product_id );
	}else{
		$action_product_id = intval( $item->get_product_id() );
		$action_permalink = get_permalink( $action_product_id );
		$product = wc_get_product( $action_product_id );
	}

	echo '<a href="'.$action_permalink.'" data-quantity="1" class="button product_type_variable add_to_cart_button button add_to_cart alt" data-product_id="'.$action_product_id.'" data-product_sku="'.$product->get_sku().'" aria-label="'.$product->get_title().'" rel="nofollow">View Artwork</a>';
}

add_action( 'wp_ajax_ajax_get_calculated_price', 'ajax_get_calculated_price' );
add_action( 'wp_ajax_nopriv_ajax_get_calculated_price', 'ajax_get_calculated_price' );
function ajax_get_calculated_price()
{
	$return = [];
	$request = $_REQUEST;
	// var_dump($request);
	$product = wc_get_product( $request['product_id'] );
	$price = floatval( calculate_custom_price( $request['width']*$request['height'] ) );
	$printed_price = $price;
	// var_dump($price);
	$curr = get_woocommerce_currency_symbol();
	// var_dump($curr);
	$rate = 0;
	if(get_woocommerce_currency_symbol() != 'HK $'){
		if(is_plugin_active('woocommerce-currency-switcher/index.php')){
			$woocs = get_option('woocs');
			$rate = floatval($woocs['USD']['rate']);
			$printed_price = $printed_price*$rate;
		}
	}

	$return['price'] = $price;
	$return['rate'] = $rate;
	// var_dump($return);
	$variation_obj = new WC_Product_variation($request['variation']);
	$stock = $variation_obj->get_stock_quantity();
	// var_dump($stock);
	$return['string'] = '<div class="woocommerce-variation-description"></div><div class="woocommerce-variation-price"><span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">'.$curr.'</span>'.number_format($printed_price, 2).'</span></span></div><div class="woocommerce-variation-availability"><p class="stock in-stock">'.$stock.' in stock</p></div>';
	echo json_encode($return);
	exit;
}

function calculate_custom_price( $size )
{
	$size = floatval($size);
	// var_dump($size);
	$sum = 0;
	$price = (float) 0.0;

	if($size >= 10000){
		$sum = $size * 0.135 * 1.1;
	}

	if($size < 10000 && $size >= 5040){
		$sum = $size * 0.135 * 1.2;
	}

	if($size < 5040 && $size >= 2520){
		$sum = $size * 0.135 * 1.45;
	}

	if($size < 2520 && $size >= 1260){
		$sum = $size * 0.135 * 1.6;
	}

	if($size < 1260 && $size > 624){
		$sum = $size * 0.135 * 1.85;
	}
	$price = ($sum * 1.2) + 1600;

	if($size <= 624)
		$price = 1787;

	return ceil($price);
}

// Displaying the custom attributes in cart and checkout items
add_filter( 'woocommerce_get_item_data', 'customizing_cart_item_data', 10, 2 );
function customizing_cart_item_data( $cart_data, $cart_item ) {
	$custom_items = array();
	if( ! empty( $cart_data ) ) $custom_items = $cart_data;

	// Get the data (custom attributes) and set them
	if( ! empty( $cart_item['custom_data']['pa_frame'] ) )
		$custom_items[] = array(
			'name'      => 'Frame',
			'value'     => $cart_item['custom_data']['pa_frame'],
		);
	return $custom_items;
}

add_filter( 'woocommerce_variable_price_html', 'custom_min_max_variable_price_html', 10, 2 );
function custom_min_max_variable_price_html( $price, $product )
{
	$prices = $product->get_variation_prices( true );
	$count  = (int) count( array_unique( $prices['price'] ));

	// When all variations prices are the same
	if( $count === 1 )
		return $price;

	$min_price = current( $prices['price'] );
	$min_keys  = current(array_keys( $prices['price'] ));

	$min_reg_price  = $prices['regular_price'][$min_keys];
	$min_price_html = wc_price( $min_price ) . $product->get_price_suffix();

	// When min price is on sale (Can be removed)
	if( $min_reg_price != $min_price ) {
		$min_price_reg_html = '<del>' . wc_price( $min_reg_price ) . $product->get_price_suffix() . '</del>';
		$min_price_html = $min_price_reg_html .'<ins>' . $min_price_html . '</ins>';
	}
	$price = sprintf( __( '<span class="small">Starting from</span> %s', 'woocommerce' ), $min_price_html );

	return $price;
}

function get_user_geo_country()
{
	$geo      = new WC_Geolocation(); // Get WC_Geolocation instance object
	$user_ip  = $geo->get_ip_address(); // Get user IP
	$user_geo = $geo->geolocate_ip( $user_ip ); // Get geolocated user data.
	// var_dump($user_geo); exit;
	$country  = $user_geo['country']; // Get the country code
	return $country; // return the country name
}

function ajax_stak_get_cart()
{
	global $woocommerce;
	// var_dump($_REQUEST);
	$return = '';
	$items = $woocommerce->cart->get_cart();
	$x = 0;
	foreach ($items as $item => $values) {
		$x++;
		$return .= '----------------------------------------------------------'."\r\n";
		$product_id = $values['data']->get_id();
		$_product =  wc_get_product( $product_id );
		// var_dump($values);
		if( isset($values['variation']) && count($values['variation']) > 0 ){
			// var_dump($values['variation']);
			$var = $values['variation'];
			// print_r($var['attribute_pa_select-size']);
			$size = null !== $var['attribute_pa_select-size'] && $var['attribute_pa_select-size'] !== '' ? $var['attribute_pa_select-size'] : '';
		}
		// var_dump($_product);
		$product_title = $_product->get_title();
		$price_num = $values['data']->get_price();
		$price_curr = get_woocommerce_currency();
		$price_curr_sym = get_woocommerce_currency_symbol($price_curr);
		$price_format = number_format( floatval( $price_num ), 2 );
		$price_str = $price_curr_sym.' '.$price_format;
		$return .= '['.$x.']'."\r\n";
		$return .= $product_title."\r\n";
		if($size && $size !== ''){
			$return .= 'Size: '.$size."\r\n";
		}
		$return .= 'Price: '.$price_str."\r\n";
		$return .= 'Quantity: '.$values['quantity']."\r\n";
		$return .= 'Sub Total: '.$price_curr_sym.' '.number_format( $values['line_subtotal'], 2 )."\r\n";
		$return .= '----------------------------------------------------------'."\r\n";
		$return .= "\r\n";
	}
	echo $return;
	exit;
}

add_action( 'wp_ajax_ajax_stak_get_cart', 'ajax_stak_get_cart' );
// If you wanted to also use the function for non-logged in users (in a theme for example)
add_action( 'wp_ajax_nopriv_ajax_stak_get_cart', 'ajax_stak_get_cart' );

function ajax_commercial_confirm()
{
	if(!is_user_logged_in()){
		$return = [
			'status' => 'failed',
			'message' => 'You are not logged in, please login first and retry!'
		];
		echo json_encode($return);
		exit;
	}

	$user = wp_get_current_user();
	$cart_data = stak_get_cart();
	// var_dump($cart_data); exit;
	// var_dump($user->display_name); exit;

	$to = QUOTE_EMAIL;
	$subject = 'A Commercial Buyer is Requesting Partial Payment Option';
	$headers = [
		'From: Global Shipping Enquiry Form <no-reply@redtmultiples.com>',
		'Reply-To: '.$user->display_name.'<'.$user->user_email.'>'
	];
	$body = '';
	$body .= '

Subject: "Request For Partial Payment Option"

Message Body:
The customer below has a purchase with total more than HK $ 50,000 (Or equivalent) and has stated that they are a commercial buyer.
This email was sent to request information about the partial payment option.

Buyer Information
--------------------------------
Full name: '.$user->display_name.'
Email:     '.$user->user_email.'

Items:
'.$cart_data.'

Thanks.
-- 
This e-mail was sent from system admin.
	';
	// var_dump($body); exit;
	if(wp_mail($to, $subject, $body, $headers)){
		$return = [
			'status' => 'success',
			'message' => 'OK!'
		];
		echo json_encode($return);
		exit;
	}else{
		$return = [
			'status' => 'error',
			'message' => 'Something went wrong, please retry in a few minutes.'
		];
		echo json_encode($return);
		exit;
	}
}
add_action( 'wp_ajax_ajax_commercial_confirm', 'ajax_commercial_confirm' );
// If you wanted to also use the function for non-logged in users (in a theme for example)
add_action( 'wp_ajax_nopriv_ajax_commercial_confirm', 'ajax_commercial_confirm' );

function ajax_own_shipping_confirm()
{
	if(!is_user_logged_in()){
		$return = [
			'status' => 'failed',
			'message' => 'You are not logged in, please login first and retry!'
		];
		echo json_encode($return);
		exit;
	}

	$user = wp_get_current_user();
	$cart_data = stak_get_cart();
	// var_dump($cart_data); exit;
	// var_dump($user->display_name); exit;

	$to = QUOTE_EMAIL;
	$subject = 'A Buyer is Requesting To Use His/Her Own Shipping';
	$headers = [
		'From: Global Shipping Enquiry Form <no-reply@redtmultiples.com>',
		'Reply-To: '.$user->display_name.'<'.$user->user_email.'>'
	];
	$body = '';
	$body .= '

Subject: "A Buyer is Requesting To Use His/Her Own Shipping"

Message Body:
The customer below has requested to use his/her own shipping.
This email was sent to request invoice and shipping fee calculation.

Buyer Information
--------------------------------
Full name: '.$user->display_name.'
Email:     '.$user->user_email.'

Items:
'.$cart_data.'

Thanks.
-- 
This e-mail was sent from system admin.
	';
	// var_dump($body); exit;
	if(wp_mail($to, $subject, $body, $headers)){
		$return = [
			'status' => 'success',
			'message' => 'OK!'
		];
		echo json_encode($return);
		exit;
	}else{
		$return = [
			'status' => 'error',
			'message' => 'Something went wrong, please retry in a few minutes.'
		];
		echo json_encode($return);
		exit;
	}
}
add_action( 'wp_ajax_ajax_own_shipping_confirm', 'ajax_own_shipping_confirm' );
// If you wanted to also use the function for non-logged in users (in a theme for example)
add_action( 'wp_ajax_nopriv_ajax_own_shipping_confirm', 'ajax_own_shipping_confirm' );

add_action( 'woocommerce_checkout_create_order', 'stak_custom_billing_phone', 9999, 2 );
function stak_custom_billing_phone( $order, $data )
{
	// var_dump(isset($_REQUEST['billing_wooccm11']));
	// var_dump($data);
	// var_dump($order);
	$numbers_only = preg_replace("/[^\d]/", "", $data['billing_phone']);
	$formatted_phone = preg_replace("/^1?(\d{3})(\d{3})(\d{4})$/", "$1-$2-$3", $numbers_only);

	if(isset($_REQUEST['billing_wooccm11']) && $_REQUEST['billing_wooccm11'] != ''){
		$billing_phone = $_REQUEST['billing_wooccm11'].'-'.$formatted_phone;
		// unset($data['billing_wooccm11']);
	}else{
		$billing_phone = $formatted_phone;
	}

	$order->set_billing_phone($billing_phone);
	// var_dump($order); exit;
	return $order;
}

function stak_prevent_checkout_on_condition( $data, $errors )
{
	if(!defined('MAX_TOTAL_PRICE'))
		define('MAX_TOTAL_PRICE', floatval(50000));
	if(!defined('MAX_NUM_QUANTITY'))
		define('MAX_NUM_QUANTITY', 5);

	// var_dump($data); exit;

	$global = false;
	// $order_frame = false;
	$over_price = false;
	$over_quantity = false;
	$string = '';

	if(!isset($_REQUEST))
		return print_response('failure', 'error', '<strong>No data!</strong> There is no data being sent.');

	$request = $_REQUEST;
	
	$global_shipping = false;
	if(isset($request['ship_to_different_address']) && $request['shipping_country'] !== 'HK'){
		$global_shipping = true;
	}
	if(!isset($request['ship_to_different_address']) && $request['billing_country'] !== 'HK'){
		$global_shipping = true;
	}
	// var_dump($global_shipping); exit;

	// checking the conditions.
	$conditions = [
		'global' => $global_shipping, // checking shipment country.
		'overprice' => check_total_price(), // checking if the total cost over the limit.
		// 'overquantity' => check_total_quantity(), // checking if the number of items inside the cart is over the limit.
		'frame' => null !== $request['frame-inquiry'],
		// 'commercial' => isset($request['commercial_buyer']) && $request['commercial_buyer'] == 'yes'
	];
	// var_dump($conditions); exit;

	foreach($conditions as $cond => $stat){
		if(null !== $errors->errors['validation'])
			break;

		if($cond === 'global' && $stat){ // shipping to country outside HK.
			$send_email = send_email('shipping', $request);
			$errors = clean_other_errors( $errors );
			// var_dump($errors); exit;

			if($send_email){
				$string .= '<p>We have detected that your shipping address country is outside <strong>"Hongkong"</strong>.</p>';
				$string .= '<p>Currently, we do not ship to locations outside Hongkong. We have received your order and will get back to you through email to inform you about the cost to ship your purchase to your shipping address.</p>';
				$errors->add('shipping', __($string, 'woocommerce'));
			}else{
				$errors->add('shipping', __('There is an error processing your order shipping!', 'woocommerce'));
			}
			break;
		}

		if($cond === 'overprice' && $stat){ 
			// price is over than the limit established.
			$send_email = send_email('order', $request);
			// var_dump($send_email); exit;
			// var_dump($errors); exit;
			if(!$send_email){
				$errors = clean_other_errors( $errors );
				$errors->add('order', __('There is an error processing your order (total price)!', 'woocommerce'));
			}
			// break;
		}
		
		/*if($cond === 'overquantity' && $stat){ 
			// item quantity inside the cart is more than the allowed num.
			$send_email = send_email('quantity', $request);
			// var_dump($send_email); exit;
			$errors = clean_other_errors( $errors );
			// var_dump($errors); exit;
			if($send_email){
				$errors->add('quantity', sprintf( __('<strong>Your order has more than %s items</strong>. We have received your order and will send an invoice to the email address provided below. Thank you for your order.', 'woocommerce'), intval(MAX_NUM_QUANTITY) ) );
			}else{
				$errors->add('quantity', __('There is an error processing your order (quantity)!', 'woocommerce'));
			}
			break;
		}*/

		if($cond === 'frame' && $stat){ // order an additional custom frame.
			// if(!isset)
			$send_email = send_email('frame', $request);
			if(!$send_email){
				$errors = clean_other_errors( $errors );
				$errors->add('frame', __('There is an error processing your order (quantity)!', 'woocommerce'));
			}
			// break;
		}

		/*if($cond === 'commercial'){
			$send_email = send_email('commercial', $request);
			if(!$send_email){
				$errors = clean_other_errors( $errors );
				$errors->add('commercial', __('There is an error processing your order (quantity)!', 'woocommerce'));
			}
			// break;
		}*/
	}
}
// add_action( 'woocommerce_after_checkout_validation', 'stak_prevent_checkout_on_condition', 9999, 2 );

function clean_other_errors( $errors )
{
	foreach ($errors->errors as $key => $string) {
		if( !in_array( $key, ['order', 'quantity', 'frame'] ) )
			$errors->remove($key);
	}
	return $errors;
}

function check_total_price()
{
	global $woocommerce;
	// var_dump($_REQUEST);
	$return = '';
	$items = $woocommerce->cart->get_cart();

	// because it's not certain to get the total cost based on order data we pick it up manually per item inside cart.
	$total = floatval(0);
	foreach ($items as $item => $values){
		$total = $total + $values['line_subtotal'];
	}
	// var_dump($total >= MAX_TOTAL_PRICE); exit;

	// $return = $total >= MAX_TOTAL_PRICE;
	return $total >= MAX_TOTAL_PRICE;
}

function check_total_quantity()
{
	global $woocommerce;
	// var_dump($_REQUEST);
	$return = '';
	$items = $woocommerce->cart->get_cart();

	// because it's not certain to get the number of quantity based on order data we pick it up manually per item inside cart.
	$total = 0;
	foreach ($items as $item => $values){
		$total = $total + $values['quantity'];
	}

	return ($total >= MAX_NUM_QUANTITY);
}

add_action('woocommerce_checkout_update_order_meta', 'stak_checkout_frame_order_meta');
function stak_checkout_frame_order_meta( $order_id ) 
{
	if ($_POST['frame-inquiry']) update_post_meta( $order_id, 'Custom Frame', 'yes' );
}

function send_email( $case, $request )
{
	$cart_data = stak_get_cart();
	switch ($case) {
		case 'shipping':
			// var_dump($request); exit;
			$to = QUOTE_EMAIL;
			$subject = 'Red T Multiples "Enquiry for Global Shipping Cost"';
			$headers = [
				'From: Global Shipping Enquiry Form <no-reply@redtmultiples.com>',
				'Reply-To: '.$request['billing_first_name'].' '.$request['billing_last_name'].'<'.$request['billing_email'].'>'
			];
			$body = '';
			$body .= '
From: '.$request['billing_first_name'].' '.$request['billing_last_name'].' <'.$request['billing_email'].'>
Subject: "Enquiry for International Shipping Cost"

Message Body:
Please send me an inquiry for international shipping address for the address below.

Full name: '.$request['billing_first_name'].' '.$request['billing_last_name'].'

Shipping address:
'.$request['billing_address_1'].'
'.$request['billing_address_2'].'
'.$request['billing_city'].', '.$request['billing_state'].', '.$request['billing_country'].' '.$request['billing_postcode'].'
Phone: '.$request['billing_phone'].'

Items:
'.$cart_data.'

Thanks.
-- 
This e-mail was sent from system admin.
			';
			// var_dump($body); exit;
			return wp_mail($to, $subject, $body, $headers);
			break;
		
		case 'order':
			// var_dump($request); exit;
			$to = QUOTE_EMAIL;
			$subject = 'Red T Multiples "Invoice Request for Order Worth More Than HKD$ '.number_format(MAX_TOTAL_PRICE, 2).'"';
			$headers = [
				'From: Global Shipping Enquiry Form <no-reply@redtmultiples.com>',
				'Reply-To: '.$request['billing_first_name'].' '.$request['billing_last_name'].'<'.$request['billing_email'].'>'
			];
			$body = '';
			$body .= '
From: '.$request['billing_first_name'].' '.$request['billing_last_name'].' <'.$request['billing_email'].'>
Subject: "Invoice Request for Order Worth More Than HKD$ '.number_format(MAX_TOTAL_PRICE, 2).'"

Message Body:
Please send me an invoice for the order below:

Full name: '.$request['billing_first_name'].' '.$request['billing_last_name'].'

Shipping address:
'.$request['billing_address_1'].'
'.$request['billing_address_2'].'
'.$request['billing_city'].', '.$request['billing_state'].', '.$request['billing_country'].' '.$request['billing_postcode'].'
Phone: '.$request['billing_phone'].'

Items:
'.$cart_data.'

Thanks.
-- 
This e-mail was sent from system admin.
			';
			// var_dump($body); exit;
			return wp_mail($to, $subject, $body, $headers);
			break;

		case 'quantity':
			// var_dump($request); exit;
			$to = QUOTE_EMAIL;
			$subject = 'Red T Multiples "Invoice Request for Order With More Than '.MAX_NUM_QUANTITY.' Items"';
			$headers = [
				'From: Global Shipping Enquiry Form <no-reply@redtmultiples.com>',
				'Reply-To: '.$request['billing_first_name'].' '.$request['billing_last_name'].'<'.$request['billing_email'].'>'
			];
			$body = '';
			$body .= '
From: '.$request['billing_first_name'].' '.$request['billing_last_name'].' <'.$request['billing_email'].'>
Subject: "Invoice Request for Order With More Than '.MAX_NUM_QUANTITY.' Items"

Message Body:
Please send me an invoice for the order below.

Full name: '.$request['billing_first_name'].' '.$request['billing_last_name'].'

Shipping address:
'.$request['billing_address_1'].'
'.$request['billing_address_2'].'
'.$request['billing_city'].', '.$request['billing_state'].', '.$request['billing_country'].' '.$request['billing_postcode'].'
Phone: '.$request['billing_phone'].'

Items:
'.$cart_data.'

Thanks.
-- 
This e-mail was sent from system admin.
			';
			// var_dump($body); exit;
			return wp_mail($to, $subject, $body, $headers);
			break;

		case 'frame':
			// var_dump($request); exit;
			$to = QUOTE_EMAIL;
			$subject = 'Red T Multiples "Enquiry for Custom Frame"';
			$headers = [
				'From: Global Shipping Enquiry Form <no-reply@redtmultiples.com>',
				'Reply-To: '.$request['billing_first_name'].' '.$request['billing_last_name'].'<'.$request['billing_email'].'>'
			];
			$body = '';
			$body .= '
From: '.$request['billing_first_name'].' '.$request['billing_last_name'].' <'.$request['billing_email'].'>
Subject: "Enquiry for Custom Frame"

Message Body:
Please send me an inquiry for the custom frame I would like to add to my purchase.

Full name: '.$request['billing_first_name'].' '.$request['billing_last_name'].'
Email: '.$request['billing_email'].'

Shipping address:
'.$request['billing_address_1'].'
'.$request['billing_address_2'].'
'.$request['billing_city'].', '.$request['billing_state'].', '.$request['billing_country'].' '.$request['billing_postcode'].'
Phone: '.$request['billing_phone'].'

Items:
'.$cart_data.'

Thanks.
-- 
This e-mail was sent from system admin.
			';
			// var_dump($body); exit;
			return wp_mail($to, $subject, $body, $headers);
			break;

		case 'commercial':
			// var_dump($request); exit;
			$to = QUOTE_EMAIL;
			$subject = 'A Commercial Buyer is Requesting Partial Payment Option';
			$headers = [
				'From: Global Shipping Enquiry Form <no-reply@redtmultiples.com>',
				'Reply-To: '.$request['billing_first_name'].' '.$request['billing_last_name'].'<'.$request['billing_email'].'>'
			];
			$body = '';
			$body .= '

Subject: "Request For Partial Payment Option"

Message Body:
The customer below has a purchase with total more than HK $ 50,000 (Or equivalent) and has stated that they are a commercial buyer.
This email was sent to request information about the partial payment option.

Buyer Information
--------------------------------
Full name: '.$request['billing_first_name'].' '.$request['billing_last_name'].'
Email:     '.$request['billing_email'].'

Address:
'.$request['billing_address_1'].'
'.$request['billing_address_2'].'
'.$request['billing_city'].', '.$request['billing_state'].', '.$request['billing_country'].' '.$request['billing_postcode'].'

Phone: '.$request['billing_phone'].'

Items:
'.$cart_data.'

Thanks.
-- 
This e-mail was sent from system admin.
			';
			// var_dump($body); exit;
			return wp_mail($to, $subject, $body, $headers);
			break;

		default:
			// code...
			break;
	}
}

function stak_get_cart()
{
	global $woocommerce;
	// var_dump($_REQUEST);
	$return = '';
	$items = $woocommerce->cart->get_cart();
	$x = 0;
	foreach ($items as $item => $values) {
		$x++;
		// var_dump($values);
		$return .= '----------------------------------------------------------'."\r\n";
		$product_id = $values['data']->get_id();
		$_product =  wc_get_product( $product_id );
		// var_dump($values);
		$size = false;
		// var_dump($values['variation']);

		if( isset($values['variation']) && !empty($values['variation']) ){
			
			$var = $values['variation'];
			// print_r($var['attribute_pa_select-size']);
			$size = null !== $var['attribute_pa_select-size'] && $var['attribute_pa_select-size'] !== '' ? $var['attribute_pa_select-size'] : false;
			// var_dump($values);
			$sizes_arr = explode('-x-', $size);
			$dim = 1.0;
			foreach($sizes_arr as $m){
				$dim = $dim * floatval(str_replace('-cm', '', $m));
			}
			// var_dump($dim);
			$_price = floatval( calculate_custom_price( $dim ) );
			// var_dump($_price);
			$values['data']->set_price($_price);
		}

		if(!$size){
			if(isset($values['custom_data']) && !empty($values['custom_data'])){
				$custom_width = floatval($values['custom_data']['pa_custom_width']);
				$custom_height = floatval($values['custom_data']['pa_custom_height']);
				$size = $custom_width.'-x-'.$custom_height.' cm';
			}
		}
		// var_dump($size);
		if(isset($values['custom_price'])){
			$values['data']->set_price($values['custom_price']);
		}
		// var_dump($_product);
		$product_title = $_product->get_title();
		$price_num = $values['data']->get_price();
		// var_dump($price_num);
		$price_curr = get_woocommerce_currency();
		$price_curr_sym = get_woocommerce_currency_symbol($price_curr);
		$price_format = number_format( floatval( $price_num ), 2 );
		$price_str = $price_curr_sym.' '.$price_format;
		$return .= '['.$x.']'."\r\n";
		$return .= $product_title."\r\n";
		if($size && $size !== ''){
			$return .= 'Size: '.$size."\r\n";
		}
		$return .= 'Price: '.$price_str."\r\n";
		$return .= 'Quantity: '.$values['quantity']."\r\n";
		$return .= 'Sub Total: '.$price_curr_sym.' '.number_format( $values['line_subtotal'], 2 )."\r\n";
		$return .= '----------------------------------------------------------'."\r\n";
		$return .= "\r\n";
	}
	// exit;
	return $return;
}

 // customizer custom URL and tags.
add_action( 'init', function(){
	add_rewrite_tag( '%product_id%', '([^&]+)' );
}, 10, 0 );

add_action( 'init', function(){
	add_rewrite_rule( '^customizer/([0-9]+)/?',
		'index.php?pagename=customizer&product_id=$matches[1]',
		'top' );
} );

// define the woocommerce_cart_item_thumbnail callback 
function filter_woocommerce_cart_item_thumbnail( $product_get_image, $cart_item, $cart_item_key ) { 
	// make filter magic happen here... 
	// return $product_get_image;
	// print_r($product_get_image);
	// var_dump($cart_item);
	// exit;
	$return = $product_get_image;
	if(isset($cart_item['custom_data']['pa_cropped_img']) && (null !== $cart_item['custom_data']['pa_cropped_img'] && $cart_item['custom_data']['pa_cropped_img'] !== '')){
		$return = '<img width="300" height="300" src="'.$cart_item['custom_data']['pa_cropped_img'].'" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset="">';
	}

	return $return;
}; 
		 
// add the filter 
add_filter( 'woocommerce_cart_item_thumbnail', 'filter_woocommerce_cart_item_thumbnail', 10, 3 );

function stak_wc_get_gallery_image_html( $attachment_id, $post, $main_image = false ) {
	$flexslider        = (bool) apply_filters( 'woocommerce_single_product_flexslider_enabled', get_theme_support( 'wc-product-gallery-slider' ) );
	$gallery_thumbnail = wc_get_image_size( 'gallery_thumbnail' );
	$thumbnail_size    = apply_filters( 'woocommerce_gallery_thumbnail_size', array( $gallery_thumbnail['width'], $gallery_thumbnail['height'] ) );
	$image_size        = apply_filters( 'woocommerce_gallery_image_size', $flexslider || $main_image ? 'woocommerce_single' : $thumbnail_size );
	$full_size         = apply_filters( 'woocommerce_gallery_full_size', apply_filters( 'woocommerce_product_thumbnails_large_size', 'full' ) );
	$thumbnail_src     = wp_get_attachment_image_src( $attachment_id, $thumbnail_size );
	$full_src          = wp_get_attachment_image_src( $attachment_id, $full_size );
	$alt_text          = trim( wp_strip_all_tags( get_post_meta( $attachment_id, '_wp_attachment_image_alt', true ) ) );
	$image             = wp_get_attachment_image(
		$attachment_id,
		$image_size,
		false,
		apply_filters(
			'woocommerce_gallery_image_html_attachment_image_params',
			array(
				'title'                   => _wp_specialchars( get_post_field( 'post_title', $post->ID ), ENT_QUOTES, 'UTF-8', true ),
				'data-caption'            => _wp_specialchars( get_post_field( 'post_title', $post->ID ), ENT_QUOTES, 'UTF-8', true ),
				'data-src'                => esc_url( $full_src[0] ),
				'data-large_image'        => esc_url( $full_src[0] ),
				'data-large_image_width'  => esc_attr( $full_src[1] ),
				'data-large_image_height' => esc_attr( $full_src[2] ),
				'class'                   => esc_attr( $main_image ? 'wp-post-image' : '' ),
			),
			$attachment_id,
			$image_size,
			$main_image
		)
	);

	return '<div data-thumb="' . esc_url( $thumbnail_src[0] ) . '" data-thumb-alt="' . esc_attr( $alt_text ) . '" class="woocommerce-product-gallery__image"><a href="' . esc_url( $full_src[0] ) . '">' . $image . '</a></div>';
}

function print_frame_selection()
{
	?>
	<div class="row">
		<div class="col-xs-12">
			<h4>Add Custom Frame</h4>
			<div class="form-group">
				<div class="checkbox">
					<label for="frame-toggler" id="toggler-check">
						<input type="checkbox" id="frame-toggler" name="frame-inquiry">
						<span>Inquire For Custom Frame</span>
					</label>
				</div>
			</div>
		</div>
		<div id="custom-frame-wrapper" class="col-xs-12" style="display: none;">
			<div class="col-xs-12">
				<div class="form-group">
					<div class="frame-label-wrapper">
						<div class="input-hidden" style="display: none;">
							<input type="radio" id="first" name="custom-frame" value="Custom frame">
							<!--<input type="radio" id="last" name="custom-frame" value="Painted natural varnish marupa wood with visible grain">-->
						</div>
						<div class="frame-selector-wrapper">
							<label class="frame-selector cursor-pointer frame-1" data-frame="first"></label>
							<span>Please proceed with your order and we will email you with a quote for framing.</span>
						</div>
						<!--<div class="frame-selector-wrapper">
							<label class="frame-selector cursor-pointer frame-2" data-frame="last"></label>
							<span>Painted natural varnish marupa wood with visible grain</span>
						</div>-->
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12">
			<hr class="horizontal-line">
		</div>
	</div>
	<?php
}

function stak_custom_order_review()
{
	?>
	<div id="cart_review" class="col-xs-12">
		<h3 id="order_review_heading"><?php esc_html_e( 'Your order', 'woocommerce' ); ?></h3>

		<?php new_cart_details(); ?>
		
	</div>
	<?php
}

function new_cart_details()
{
	foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
		$_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
		$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
		?>
		<div class="row">
			<div class="col-xs-12 cart-content-wrapper">
				<div class='col-lg-3 col-md-4 col-sm-12 col-xs-12'>
					<?php echo $thumbnail; ?>
				</div>
				<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
					<p class="text1">
						<?php 
						$sessionDimension = "";
						$productId = $_product->get_id();
						$custom_product = '';
						$medium = null !== $cart_item['custom_data']['pa_medium'] &&  $cart_item['custom_data']['pa_medium'] !== '' ?  $cart_item['custom_data']['pa_medium'] : 'Paper'; // for now everything will be paper.
						if( null !== $cart_item['custom_data']['pa_img_width'] && null !== $cart_item['custom_data']['pa_img_height'] ){
							if( ( is_numeric($cart_item['custom_data']['pa_img_width']) && is_numeric($cart_item['custom_data']['pa_img_height']) )){
								$custom_product = $cart_item['custom_data']['pa_img_width'].' x '.$cart_item['custom_data']['pa_img_height'].' cm - '.$medium; 
							}else{
								$custom_product = $medium;
							}
						}else{
							if(isset($_SESSION['customizeProduct'][$productId])){
								$postdata = $_SESSION['customizeProduct'][$productId];
								$custom_product = $postdata['width_image'].'x'.$postdata['height_image'].' cm'.' - '.$medium; 
							}else{
								$custom_product = $medium;
							}
						}
						echo apply_filters( 'woocommerce_cart_item_name', $_product->get_name().'<br>'.$custom_product, $cart_item, $cart_item_key ) . '&nbsp;'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
							
					</p>
					<p class="text1">Quantity: <?php echo apply_filters( 'woocommerce_checkout_cart_item_quantity', ' <strong class="product-quantity">' . sprintf( '&times; %s', $cart_item['quantity'] ) . '</strong>', $cart_item, $cart_item_key ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
					<p class="text1"><?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
				</div>
			</div>
		</div>
			
	<?php 
	}
}

function print_new_coupon()
{
	?>
	<div class="row">
		<div class="col-xs-12">
			<?php if ( wc_coupons_enabled() ) { ?>
				<h4 class="text2">
					<?php esc_html_e( 'Apply Coupon Code', 'woocommerce' ); ?>
				</h4>
				<input 
					type="text" 
					name="coupon_code" 
					class="input-text stak-custom-input" 
					id="stak_coupon_code" 
					value="" 
					placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>"
					>
				<!--<button style="float: right;margin-right: 139px !important;border-bottom:1px solid red;margin-top: 1px;" type="submit" class="text3" name="apply_coupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>"><?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?></button>-->
				
				<a onclick="javascript:;" class="text3 stack_custom_button" id="apply_coupon">
					<?php esc_attr_e( 'Apply coupon', 'woocommerce' );?>
				</a>
				<script>
					function applyCouponFunction()
					{
						// jQuery("form[name='checkout']").submit();
					}
				</script>
				<?php do_action( 'woocommerce_cart_coupon' ); ?>
			<?php } ?>
		</div>
	</div>
	<?php
}

function print_new_shipping_review()
{
	?>
	<div id="shipping_review" class="col-xs-12" style="display: none;">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			<p class="text4"><?php esc_html_e( 'Subtotal', 'woocommerce' ); ?> </p>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			<p class="text5"><?php wc_cart_totals_subtotal_html(); ?></p>
		</div>
		<div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				<p class="text4">Shipping </p>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				<p class="text5">Calculated at the next step</p>
				<?php do_action( 'woocommerce_review_order_before_shipping' ); ?>

				<?php wc_cart_totals_shipping_html(); ?>

				<?php do_action( 'woocommerce_review_order_after_shipping' ); ?>
			</div>
		</div>
	</div>
	<?php
}

function _commercial_available()
{
	global $woocommerce;

	if(get_woocommerce_currency_symbol() == 'HK $' && floatval($woocommerce->cart->total) > 50000){
		return true;
	}

	if(get_woocommerce_currency_symbol() == 'US $' && floatval($woocommerce->cart->total) > 5000){
		return true;
	}

	return false;
}

function _output_total_price_calc()
{
	global $woocommerce;
	?>
	<div id="total_amount">
		<h4>
			Sub Total
		</h4>
		<div class="col-xs-12" style="padding-left: 0;">
			<p id="wc-sub-total" class="text8 pull-left"><?php wc_cart_totals_subtotal_html(); ?></p>
			<?php 
				$currency_code = get_woocommerce_currency_symbol(); 
				switch ($currency_code) {
					case 'HK $':
						echo '<a class="text7 change_currency_switch" id="curr-switch" data-currency="USD">SWITCH TO US $</a>';
						break;
					
					case 'US $':
						echo '<a class="text7 change_currency_switch" id="curr-switch" data-currency="HKD">SWITCH TO HK $</a>';
						break;
				}
			?>
			<input type="hidden" id="currency_code" value="<?php echo $currency_code; ?>">
			<input type="hidden" id="sub_total_cart" value="<?php echo floatval($woocommerce->cart->total); ?>">
		</div>
		<div class="col-xs-12 hide">
			<?php echo do_shortcode("[woocs style=1 show_flags=1 width='300px' flag_position='right' txt_type='desc']"); ?>
		</div>
		<div class="showtodaypayment" id="showtodaypayment" style="display:none;">
			<hr style="border: 1px solid #d6d6d6;float: left;width: 107%;margin-left: -14px !important;margin-top: 43px;" class="horizontal-line">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				<p style="font-weight: bolder;font-size: 16px;text-align: center;" class="text6"><?php esc_html_e( "Today's Payments", "woocommerce" ); ?> </p>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				<p style="font-size: 16px;font-weight: bolder;margin-bottom: 0px;" class="text8"><?php wc_cart_totals_subtotal_html(); ?></p>
			</div>
		</div>
	</div>
	<?php
}

add_filter( 'woocommerce_product_tabs', 'stak_artist_info_tab', 10, 3 );
function stak_artist_info_tab( $tabs )
{
	if(is_array($tabs) && count($tabs) > 0){
		// we need to clear the existing tabs.
		foreach($tabs as $key => $val){
			if($key !== '_the_more_info_product')
				unset($tabs[$key]);
		}
	}
	$tabs['_the_artist_detail'] = [
		'title' => __( 'Artist Information', 'woocommerce' ),
		'priority' => 9,
		'callback'  => 'stak_artist_content_output'
	];

	$tabs = array_reverse($tabs);
	// var_dump($tabs);

	return $tabs;
}

function stak_artist_content_output()
{
	global $post;
	// var_dump($post);
	$_artist = get_field('artist_page', $post->ID);
	// var_dump($_artist);
	$mfn_builder = new Mfn_Builder_Front($_artist->ID);
	$mfn_builder->show();
	// echo '<div class="wrapper-the_artist_detail">'.$content.'</div>';
}

add_action( 'woocommerce_thankyou', 'commercial_set_to_commercial_status', 10, 1 );
function commercial_set_to_commercial_status( $order_id ) {
	if ( ! $order_id )
		return;

	// Get an instance of the WC_Product object
	$order = wc_get_order( $order_id );

	// For paid Orders with all others payment methods (paid order status "processing")
	if( null !== $order->get_meta('_commercial_payment') && $order->get_meta('_commercial_payment') == 'yes' ) {
		$order->update_status( 'commercial' );
	}

	// var_dump($order); exit;
}

add_filter( 'woocommerce_thankyou_order_received_text', 'stak_custom_thankyou_text', 20, 9 );
function stak_custom_thankyou_text( $string, $order )
{
	// var_dump($order); exit;
	// var_dump($order->get_meta('_commercial_payment'));
	if( null !== $order->get_meta('_commercial_payment') && $order->get_meta('_commercial_payment') == 'yes' ) {
		$order->update_status( 'commercial' );
	}

	// var_dump($order); exit;

	$return = $string;
	// var_dump($order->get_meta('_custom_frame'));
	$custom_frame = null !== $order->get_meta('_custom_frame') && $order->get_meta('_custom_frame') == 'yes';
	$return = '';
	if($custom_frame){
		$return .= '<div class="woocommerce-error alert alert_error" role="alert"><div class="alert_icon"><i class="icon-alert"></i></div><div class="alert_wrapper"><strong>Your order includes a request for custom framing. </strong>. We will email you with a quote for this shortly.</div><a class="close" href="#"><i class="icon-cancel"></i></a></div><br><br>'."\r\n";
		$return .= 'Thank you. Your order has been received.<br><br>'."\r\n";
	}

	$method = $order->get_payment_method();

	if($method == 'bacs'){
		$gateways = WC()->payment_gateways->get_available_payment_gateways();
		$bacs = null;

		if( $gateways ) {
			foreach( $gateways as $gateway ) {

				if( $gateway->enabled == 'yes' && $gateway->id == 'bacs' ) {

					$bacs = $gateway;

				}
			}
		}

		// var_dump($bacs);

		$accs = $bacs->account_details;

		// var_dump($accs);
		
		if(!$custom_frame){
			$return .= 'Thank you. Your order has been received.<br><br>'."\r\n";
		}

		$shipping_method = $order->get_shipping_method();
		// var_dump($shipping_method); exit;
		if($shipping_method == 'Custom Shipping')
			$return .= 'We will send you an invoice to your email address for your <strong>custom shipping request</strong>.<br><br>'."\r\n";
		
		$order_status = $order->get_status();
		// var_dump($order_status); exit;
		if($order_status == 'commercial'){
			$return .= 'We will send you an invoice to your email address for your commercial payment request.<br><br>'."\r\n";
		}
		$return .= 'Please make your payment directly into our bank account using your Order ID as the payment reference. Your order will not be processed until the funds have cleared in our account.<br><br>'."\r\n";
		$return .= 'As you would like to use your own shipping method, please advise us of the details.<br><br>'."\r\n";
		$return .= 'Send proof of payment and shipping details by email to <a href="mailto:contact@redtgroup.com">contact@redtgroup.com</a><br><br>'."\r\n";
		$return .= 'Our Bank Account(s):'."\r\n";
		$return .= '<ol>'."\r\n";
		foreach($accs as $acc){
			// var_dump($acc);
			$return .= '<li>'."\r\n";
			$return .= $acc['bank_name']."<br>\r\n";
			$return .= '<ul>'."\r\n";
			$return .= '<li>Account Name: '.$acc['account_name'].'</li>'."\r\n";
			$return .= '<li>Account Number: '.$acc['account_number'].'</li>'."\r\n";
			$return .= '<li>IBAN: '.$acc['iban'].'</li>'."\r\n";
			$return .= '<ul>'."\r\n";
			$return .= '</li>'."\r\n";
		}
		$return .= '</ol>'."\r\n";
	}

	return $return;
	
}
add_action('woocommerce_checkout_create_order', 'add_frame_before_checkout_create_order', 20, 5);
function add_frame_before_checkout_create_order( $order, $data )
{
	// var_dump($_REQUEST); exit;
	if(null !== $_REQUEST['frame-inquiry']){
		$order->update_meta_data( '_custom_frame', 'yes' );
		$order->save();
	}
}

add_action('woocommerce_checkout_create_order', 'mark_as_commercial_before_checkout_create_order', 20, 5);
function mark_as_commercial_before_checkout_create_order( $order, $data )
{
	// var_dump($_POST); exit;
	// var_dump(isset($_POST['commercial_buyer']));

	if(isset($_POST['_custom_shipping']) && $_POST['_custom_shipping'] == 1){
		$new_method_id ='flat_rate';
		// Get the customer country code
		$country_code = $order->get_shipping_country();

		// Set the array for tax calculations
		$calculate_tax_for = array(
		    'country' => $country_code,
		    'state' => '', // Can be set (optional)
		    'postcode' => '', // Can be set (optional)
		    'city' => '', // Can be set (optional)
		);

		foreach( $order->get_items( 'shipping' ) as $item_id => $item ){
			$shipping_zone = WC_Shipping_Zones::get_zone_by( 'instance_id', $item->get_instance_id() );
			$shipping_methods = $shipping_zone->get_shipping_methods();

			foreach ( $shipping_methods as $instance_id => $shipping_method ) {
				// var_dump($shipping_method->id);
				if( $shipping_method->is_enabled() && $shipping_method->id === $new_method_id ){
					$item->set_method_title( "Custom Shipping" );
					$item->set_method_id( "flat_rate" );
					$item->set_total( 0.00 );
					$item->calculate_taxes($calculate_tax_for);

					$item->save();
					break; // stop the loop
				}
			}
		}

		$order->calculate_totals();

		// var_dump($order->get_shipping_method()); exit;
	}

	if(isset($_POST['commercial_buyer']) && $_POST['commercial_buyer'] == 'yes'){
		$order->update_meta_data('_commercial_payment', 'yes', 'yes');
		$order->save();
	}

	// var_dump($order->get_meta('_commercial_payment')); exit;
}

add_action( 'init', 'stak_add_commercial_payment_order_status' );
function stak_add_commercial_payment_order_status()
{
	register_post_status( 'wc-commercial', array(
	    'label'                     => 'Commercial',
	    'public'                    => true,
	    'exclude_from_search'       => false,
	    'show_in_admin_all_list'    => true,
	    'show_in_admin_status_list' => true,
	    'label_count'               => _n_noop( 'Commercial (%s)', 'Commercial (%s)' )
	) );
}

function add_commercial_payment_to_order_statuses( $order_statuses ) {
 
    $new_order_statuses = array();
 
    // add new order status after processing
    foreach ( $order_statuses as $key => $status ) {
 
        $new_order_statuses[ $key ] = $status;
 
        if ( 'wc-processing' === $key ) {
            $new_order_statuses['wc-commercial'] = 'Commercial';
        }
    }
 
    return $new_order_statuses;
}
add_filter( 'wc_order_statuses', 'add_commercial_payment_to_order_statuses' );

add_action('woocommerce_add_order_item_meta', 'stak_add_order_item_meta', 1, 2);
function stak_add_order_item_meta( $item_id, $values )
{
	global $woocommerce, $wpdb;
	if( isset($values['custom_data']['pa_action']) && ($values['custom_data']['pa_action'] == 'save_customized_order' || $values['custom_data']['pa_action'] == 'stak_custom_art_add_to_cart') ){
		foreach($values['custom_data'] as $key => $val){
			wc_add_order_item_meta($item_id, $key, $val);
		}
	}
}

function check_if_custom( $item )
{
	$item_meta = [];
	foreach ( $item->get_formatted_meta_data() as $meta_id => $meta ) {
		$item_meta[$meta->key] = $meta->value;
	}

	return null !== $item_meta['pa_action'] && ($item_meta['pa_action'] == 'save_customized_order' || $item_meta['pa_action'] == 'stak_custom_art_add_to_cart');
}

function collect_custom_meta_from_order( $item )
{
	$item_meta = [];
	foreach ( $item->get_formatted_meta_data() as $meta_id => $meta ) {
		$item_meta[str_replace('pa_', '', $meta->key)] = $meta->value;
	}

	// var_dump($item_meta);

	return $item_meta;
}

function stak_wc_display_item_meta( $item, $args = array() ) { // original function "wc_display_item_meta".
	global $woocommerce, $post;
	$strings = array();
	$to_display = ['pa_img_width','pa_img_height','pa_medium'];
	$html    = '';
	$args    = wp_parse_args(
		$args,
		array(
			'before'       => '<ul class="wc-item-meta"><li>',
			'after'        => '</li></ul>',
			'separator'    => '</li><li>',
			'echo'         => true,
			'autop'        => false,
			'label_before' => '<strong class="wc-item-meta-label">',
			'label_after'  => ':</strong> ',
		)
	);

	foreach ( $item->get_formatted_meta_data() as $meta_id => $meta ) {
		// var_dump($meta);
		if(in_array($meta->key, $to_display)){
			$display_key 	= $meta->display_key == 'pa_medium' ?  str_replace('pa_', '',  $meta->display_key) : str_replace('pa_', 'Custom ',  $meta->display_key);
			$display_key 	= str_replace('img_', '',  $display_key);
			$display_key 	= ucwords(str_replace('_', ' ', $display_key));
			if($meta->key == 'pa_img_width' || $meta->key == 'pa_img_height'){
				if(is_numeric($meta->value)){
					$value     		= $args['autop'] ? wp_kses_post( ucwords($meta->value) ) : wp_kses_post( make_clickable( trim( ucwords($meta->value) ) ) );
					$strings[] 		= $args['label_before'] . wp_kses_post( $display_key ) . $args['label_after'] . $value . ' cm';
				}
			}else{
				$value     		= $args['autop'] ? wp_kses_post( ucwords($meta->value) ) : wp_kses_post( make_clickable( trim( ucwords($meta->value) ) ) );
				$strings[] 		= $args['label_before'] . wp_kses_post( $display_key ) . $args['label_after'] . $value;
			}
		}
		
	}

	if ( $strings ) {
		$html = $args['before'] . implode( $args['separator'], $strings ) . $args['after'];
	}

	$html = apply_filters( 'woocommerce_display_item_meta', $html, $item, $args );

	if ( $args['echo'] ) {
		echo $html; // WPCS: XSS ok.
	} else {
		return $html;
	}
}

// define the woocommerce_order_item_permalink callback 
function filter_woocommerce_order_item_permalink( $product_get_permalink_item, $item, $order ) { 
	$return = '';
	$_meta = false;

	foreach ( $item->get_formatted_meta_data() as $meta_id => $meta ) {
		// var_dump($meta);
		if( $meta->key == 'pa_cropped_img' ){
			$_meta['img'] = $meta->value;
		}
	}

	// var_dump( $product_get_permalink_item );

	if( $_meta ){
		$return = $_meta['img'];
	}else{
		$product = $item->get_product();
		$return = wp_get_attachment_url( $product->get_image_id(), 'full' );
	}

	return $return;
}
		 
// add the filter 
add_filter( 'woocommerce_order_item_permalink', 'filter_woocommerce_order_item_permalink', 10, 3 );

add_action( 'woocommerce_email_before_order_table', 'stak_add_content_specific_email', 20, 4 );
  
function stak_add_content_specific_email( $order, $sent_to_admin, $plain_text, $email ) {
	if ( $email->id == 'customer_processing_order' || $email->id == 'customer_on_hold_order' ) {
		// var_dump($order); exit;
		if(null !== $order->get_meta('_custom_frame') && $order->get_meta('_custom_frame') == 'yes'){
			echo '<p class="email-upsell-p">We will send you a quote for your framing options shortly.</p>';
		}
	}
}

// add_action( 'woocommerce_email_before_order_table', 'stak_commercial_payment_note' );
function stak_commercial_payment_note($order, $sent_to_admin, $plain_text, $email)
{
	var_dump(!$sent_to_admin && ($order->has_status( 'commercial' ) && $order->get_payment_method() == 'bacs')); exit;

	if( !$sent_to_admin && ($order->status === 'commercial' && $order->get_payment_method() == 'bacs') ){
		$account_details = get_option(
			'woocommerce_bacs_accounts',
			array(
				array(
					'account_name'   => get_option( 'account_name' ),
					'account_number' => get_option( 'account_number' ),
					'sort_code'      => get_option( 'sort_code' ),
					'bank_name'      => get_option( 'bank_name' ),
					'iban'           => get_option( 'iban' ),
					'bic'            => get_option( 'bic' ),
				),
			)
		);
		// var_dump($account_details); exit;
		$bacs_accounts = apply_filters( 'woocommerce_bacs_accounts', $account_details );
		// var_dump($bacs_accounts); exit;

		if ( ! empty( $bacs_accounts ) ) {
			$account_html = '';
			$has_details  = false;

			foreach ( $bacs_accounts as $bacs_account ) {
				$bacs_account = (object) $bacs_account;

				if ( $bacs_account->account_name ) {
					$account_html .= '<h3 class="wc-bacs-bank-details-account-name">' . wp_kses_post( wp_unslash( $bacs_account->account_name ) ) . ':</h3>' . PHP_EOL;
				}

				$account_html .= '<ul class="wc-bacs-bank-details order_details bacs_details">' . PHP_EOL;

				// BACS account fields shown on the thanks page and in emails.
				$account_fields = apply_filters(
					'woocommerce_bacs_account_fields',
					array(
						'bank_name'      => array(
							'label' => __( 'Bank', 'woocommerce' ),
							'value' => $bacs_account->bank_name,
						),
						'account_number' => array(
							'label' => __( 'Account number', 'woocommerce' ),
							'value' => $bacs_account->account_number,
						),
						'sort_code'      => array(
							'label' => $sortcode,
							'value' => $bacs_account->sort_code,
						),
						'iban'           => array(
							'label' => __( 'IBAN', 'woocommerce' ),
							'value' => $bacs_account->iban,
						),
						'bic'            => array(
							'label' => __( 'BIC', 'woocommerce' ),
							'value' => $bacs_account->bic,
						),
					),
					$order_id
				);

				foreach ( $account_fields as $field_key => $field ) {
					if ( ! empty( $field['value'] ) ) {
						$account_html .= '<li class="' . esc_attr( $field_key ) . '">' . wp_kses_post( $field['label'] ) . ': <strong>' . wp_kses_post( wptexturize( $field['value'] ) ) . '</strong></li>' . PHP_EOL;
						$has_details   = true;
					}
				}

				$account_html .= '</ul>';
			}

			if ( $has_details ) {
				echo '<section class="woocommerce-bacs-bank-details"><h2 class="wc-bacs-bank-details-heading">' . esc_html__( 'Our bank details', 'woocommerce' ) . '</h2>' . wp_kses_post( PHP_EOL . $account_html ) . '</section>';
			}
		}
	}
}

add_action( 'woocommerce_thankyou', 'stak_send_printer_data', 10, 1 );
function stak_send_printer_data( $order_id )
{
	if ( ! $order_id ){
		return;	
	}

	// prepare the email content first.
	$to = get_option( 'woocommerce_printer_email', 1 );
	$headers = array('Content-Type: text/html; charset=UTF-8');
	$subject = 'A new order has been created #'.$order_id;
	$content = '<table>';
	$content .= '<thead>';
	$content .= '<tr>';
	$content .= '<th>#</th>';
	$content .= '<th>Product Name</th>';
	$content .= '<th>SKU</th>';    
	$content .= '<th>Original Image</th>';
	$content .= '<th>Mockup Image</th>';
	$content .= '<th>Printer Image</th>';
	$content .= '</tr>';
	$content .= '</thead>';
	$content .= '<tbody>';

	// now we collect the order data.
	$order = wc_get_order( $order_id ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.OverrideProhibited
	$x = 0;
	foreach ( $order->get_items() as $item_id => $item ){
		$x++;
		$content .= '<tr>';
		$content .= '<td>'.$x.'</td>';
		$order_item_data = $item->get_data();
		// var_dump($item);
		$item_meta = [];
		foreach ( $item->get_formatted_meta_data() as $meta_id => $meta ){
			// var_dump($meta);
			$item_meta[$meta->key] = $meta->value;
		}
		// var_dump($item_meta);
		$is_custom = null !== $item_meta['pa_action'] && ($item_meta['pa_action'] == 'save_customized_order' || $item_meta['pa_action'] == 'stak_custom_art_add_to_cart');
		// var_dump($is_custom);
		$product = $item->get_product();
		$product_data = $product->get_data();
		$product_name = $item->get_name();
		if($is_custom){
			$product_name = $product_name.' - '.number_format(floatval($item_meta['pa_custom_width'])).' x '.number_format(floatval($item_meta['pa_custom_height'])).' cm (CUSTOM)';
		}
		$content .= '<td>'.sprintf('<a href="%s">%s</a>', $product->get_permalink( $item ), $product_name).'</td>';
		$content .= '<td>'.$product->get_sku().'</td>';
		$original_img = wp_get_attachment_url( $product->get_image_id(), 'full' );
		$content .= '<td>'.sprintf('<a href="%s"><img src="%s" style="width:120px;"></a>', $original_img, $original_img).'</td>';
		$cropped_img = '';
		if($is_custom)
			$cropped_img = sprintf('<a href="%s"><img src="%s" style="width:120px;"></a>', $item_meta['pa_cropped_img'], $item_meta['pa_cropped_img']);
		$content .= '<td>'.$cropped_img.'</td>';
		$content .= '<td>'.$cropped_img.'</td>';
		$content .= '</tr>';
		$content .= '<tr>';
		$content .= '<td colspan="6" style="border-bottom:4px solid #000; amrgin-bottom: 15px;">';
		$content .= '<table>';
		$content .= '<tr>';
		$content .= '<th>Crop</th>';
		$content .= '<th>Contrast</th>';	
		$content .= '<th>Brightness</th>';
		$content .= '<th>Medium</th>';	
		$content .= '<th>Quantity</th>';
		$content .= '</tr>';
		$content .= '<tr>';
		if($is_custom){
			$crop = number_format(floatval($item_meta['pa_custom_width'])).' x '.number_format(floatval($item_meta['pa_custom_height'])).' cm';
		}else{
			$var_id = $item->get_variation_id();
			// var_dump($var_id);
			$variation = new WC_Product_Variation( $var_id );
			// var_dump($variation);
			$var_data = $variation->get_data();
			$crop = number_format(floatval($var_data['width'])).' x '.number_format(floatval($var_data['height'])).' cm';
		}
		$content .= '<td>'.$crop.'</td>';
		$content .= '<td></td>';
		$content .= '<td></td>';
		$content .= '<td>Paper</td>';
		$content .= '<td>'.$item->get_quantity().'</td>';
		$content .= '<tr>';
		$content .= '</table>';
		$content .= '</td>';
		$content .= '</tr>';
		
	}

	$content .= '</tbody>';
	$content .= '</table>';

	// testing the email output.
	// print_r($content); exit;
	try {
		// var_dump($to != get_bloginfo('admin_email')); exit;
		wp_mail( $to, $subject, $content, $headers );

		// if($to != get_bloginfo('admin_email'))
		// 	wp_mail( get_bloginfo('admin_email'), $subject, $content, $headers );
	} catch (Exception $e) {
		echo sprintf('<p><strong>[%s]</strong> : %s</p>', $e->getCode(), $e->getMessage());
	}
}

function stak_variable_add_to_cart()
{
	$request = $_REQUEST;
	// var_dump($request);
	if(!isset($request['product_id'])){
		echo json_encode([
			'status' => 'error',
			'post_id' => intval($request['art_id']),
			'message' => 'The product is missing from the record!'
		]);
		exit;
	}

	$product_id = intval( $request['product_id'] );

	if(!isset($request['variation_id'])){
		echo json_encode([
			'status' => 'error',
			'post_id' => intval($request['art_id']),
			'message' => 'The variable is missing from the record!'
		]);
		exit;
	}

	$variation_id = intval( $request['variation_id'] );

	if(!isset($request['variation'])){
		echo json_encode([
			'status' => 'error',
			'post_id' => intval($request['art_id']),
			'message' => 'The attribute is missing from the record!'
		]);
		exit;
	}
	$variation = $request['variation'];
	// var_dump($variation); exit;
	try {
		$added = WC()->cart->add_to_cart($product_id, 1, $variation_id, $variation);
	} catch (Exception $e) {
		echo json_encode([
			'status' => 'error',
			'post_id' => intval($request['art_id']),
			'message' => $e->getMessage()
		]);
		exit;
	}

	echo json_encode([
		'status' => 'ok',
		'post_id' => intval($request['art_id']),
		'message' => $added
	]);
	exit;
}
add_action( 'wp_ajax_stak_variable_add_to_cart', 'stak_variable_add_to_cart' );
// If you wanted to also use the function for non-logged in users (in a theme for example)
add_action( 'wp_ajax_nopriv_stak_variable_add_to_cart', 'stak_variable_add_to_cart' );

// shipping cost every 5 items.
function stak_shipping_cost_tiers( $cost, $method )
{
	// var_dump($method->get_label()); exit;
	if( $method->get_label() == 'Flat rate' ){
		$cart_item_count = WC()->cart->get_cart_contents_count();
		// $cart_item_count = 12; // testing value. comment this when go live.

		$var = ceil($cart_item_count/5);
		// var_dump($var); exit;
		$cost = $var*$cost;
	}

	return $cost;
}
// add_filter( 'woocommerce_shipping_rate_cost', 'stak_shipping_cost_tiers', 10, 2 );

if(function_exists('set_pdf_print_support')) {
	set_pdf_print_support(array('post', 'page', 'product'));
}

// define the wc_add_to_cart_params callback 
function filter_wc_add_to_cart_params( $array ) { 
	// make filter magic happen here... 
	// var_dump( $array ); 
	if(isset($array['i18n_view_cart'])){
		$array['i18n_view_cart'] = 'View cart';
	}

	return $array;
}; 
		 
// add the filter 
add_filter( 'wc_add_to_cart_params', 'filter_wc_add_to_cart_params', 10, 1 );

add_filter( 'woocommerce_product_query_tax_query', 'stak_filter_products', 10, 2 );
function stak_filter_products( $tax_query, $query )
{
	// var_dump(is_shop()); exit;
	if(!is_shop())
		return $tax_query;

	if (isset($_GET['swz_category_color']) && !empty($_GET['swz_category_color'])) {
		$tax_query[] = array(
			'taxonomy' => 'product_cat',
			'field' => 'slug',
			'terms' => $_GET['swz_category_color'],
		);
	}
	if (isset($_GET['swz_category_artist']) && !empty($_GET['swz_category_artist'])) {
		$tax_query[] = array(
			'taxonomy' => 'product_cat',
			'field' => 'slug',
			'terms' => $_GET['swz_category_artist'],
		);
	}

	return $tax_query;
}

add_action( 'wp_ajax_nopriv_stak_add_wishlist_guest', 'stak_add_wishlist_guest' );
add_action( 'wp_ajax_stak_add_wishlist_guest', 'stak_add_wishlist_guest' );
function stak_add_wishlist_guest()
{
	global $wpdb;
	// var_dump($_POST);

	if(!session_id())
		session_start();

	if(isset($_POST['add_to_wishlist'])){
		$product_id = $_POST['add_to_wishlist'];
		$_SESSION['stak_wl_arts'][] = $product_id;

		echo json_encode(
			[
				'status' => 'ok',
				'message' => 'product is added to wishlist!'
			]
		);
	}

	exit;
}

add_action( 'init' , 'set_wishlist_variation_session' );
function set_wishlist_variation_session()
{
	if(isset( $_GET['varid'] )){

		if(!session_id())
			session_start();

		$item = wc_get_product( intval( $_GET['varid'] ) );
		// var_dump($item); exit;

		$stak_wl_url = get_permalink( $item->get_parent_id() );
		// var_dump($stak_wl_url); exit;

		// now we set the requested variation item.
		$_SESSION['stak_wishlist_var'] = intval( $_GET['varid'] );

		header("Location: ".$stak_wl_url);
		exit;
	}
}

add_action( 'init', 'check_stak_wl_session' );
function check_stak_wl_session()
{
	if ( !is_user_logged_in() )
		return;

	if(isset($_SESSION['stak_wl_arts']) && count($_SESSION['stak_wl_arts']) > 0){
		$products = $_SESSION['stak_wl_arts'];
		foreach($products as $key => $id){
			// var_dump($id);
			$atts = [];
			$product_id = intval( $id );
			$product = wc_get_product( $product_id );
			// var_dump($product->get_type());

			$user = wp_get_current_user();
			// var_dump($user);

			if ( ! defined( 'YITH_WCWL' ) )
				return;

			$_REQUEST = [
				'add_to_wishlist' => $product_id,
				'user_id' => $user->id,
				// 'wishlist_id' => $wishlist->get_id()
			];

			try {
				YITH_WCWL()->add();

				$return  = 'true';
				$message = apply_filters( 'yith_wcwl_product_added_to_wishlist_message', get_option( 'yith_wcwl_product_added_text' ) );
			} catch ( YITH_WCWL_Exception $e ) {
				$return  = $e->getTextualCode();
				$message = apply_filters( 'yith_wcwl_error_adding_to_wishlist_message', $e->getMessage() );
			} catch ( Exception $e ) {
				$return  = 'error';
				$message = apply_filters( 'yith_wcwl_error_adding_to_wishlist_message', $e->getMessage() );
			}

			$wishlist = YITH_WCWL_Wishlist_Factory::get_current_wishlist();
			// var_dump($wishlist); exit;
			if(!$wishlist)
				return;

			// $product_id   = isset( $_REQUEST['add_to_wishlist'] ) ? intval( $_REQUEST['add_to_wishlist'] ) : false;
			$fragments    = isset( $_REQUEST['fragments'] ) ? $_REQUEST['fragments'] : false;

			if ( in_array( $return, array( 'exists', 'true' ) ) ) {
				// search for related fragments
				if ( ! empty( $fragments ) && ! empty( $product_id ) ) {
					foreach ( $fragments as $id => $options ) {
						if ( strpos( $id, 'add-to-wishlist-' . $product_id ) ) {
							$fragments[ $id ]['wishlist_url']      = $wishlist_url;
							$fragments[ $id ]['added_to_wishlist'] = 'true' == $return;
						}
					}
				}
			}

			// var_dump($wishlist_url); exit;
			unset($_SESSION['stak_wl_arts'][$key]);
		}
		header('Location: /wishlist');
		exit;
	}
}

add_action( 'init' , 'check_custom_art_session' );
function check_custom_art_session()
{
	global $wpdb;
	if ( !is_user_logged_in() )
		return;

	if(!session_id())
		session_start();

	// var_dump(isset($_SESSION['custom_art'])); exit;

	if(isset($_SESSION['custom_art'])){
		$custom_arts = $_SESSION['custom_art'];
		$user = wp_get_current_user();
		// var_dump($custom_arts); exit;
		// var_dump($user);
		foreach($custom_arts as $sess_id => $params){
			$string = 'Custom Art Collection '.$user->display_name.' - '.$params['id'].'-'.$sess_id;
			$post_params = [
				'post_title' => $string,
				'post_content' => $string,
				'post_status' => 'publish',
				'post_type' => 'custom-art',
				'comment_status' => 'closed'
			];

			$post_id = wp_insert_post($post_params);
			update_post_meta($post_id, 'user_id', $user->ID);
			foreach($params as $key => $val){
				update_post_meta($post_id, $key, $val);
			}
			// var_dump($params);
			unset($custom_arts[$sess_id]);
		}
		unset($_SESSION['custom_art']);
		header("Location: /my-account/customized-art/");
		exit;
	}
}

function json_phone_codes()
{
	return '
		[{"name":"Afghanistan","alpha2Code":"AF","callingCodes":["93"]},{"name":"Åland Islands","alpha2Code":"AX","callingCodes":["358"]},{"name":"Albania","alpha2Code":"AL","callingCodes":["355"]},{"name":"Algeria","alpha2Code":"DZ","callingCodes":["213"]},{"name":"American Samoa","alpha2Code":"AS","callingCodes":["1684"]},{"name":"Andorra","alpha2Code":"AD","callingCodes":["376"]},{"name":"Angola","alpha2Code":"AO","callingCodes":["244"]},{"name":"Anguilla","alpha2Code":"AI","callingCodes":["1264"]},{"name":"Antarctica","alpha2Code":"AQ","callingCodes":["672"]},{"name":"Antigua and Barbuda","alpha2Code":"AG","callingCodes":["1268"]},{"name":"Argentina","alpha2Code":"AR","callingCodes":["54"]},{"name":"Armenia","alpha2Code":"AM","callingCodes":["374"]},{"name":"Aruba","alpha2Code":"AW","callingCodes":["297"]},{"name":"Australia","alpha2Code":"AU","callingCodes":["61"]},{"name":"Austria","alpha2Code":"AT","callingCodes":["43"]},{"name":"Azerbaijan","alpha2Code":"AZ","callingCodes":["994"]},{"name":"Bahamas","alpha2Code":"BS","callingCodes":["1242"]},{"name":"Bahrain","alpha2Code":"BH","callingCodes":["973"]},{"name":"Bangladesh","alpha2Code":"BD","callingCodes":["880"]},{"name":"Barbados","alpha2Code":"BB","callingCodes":["1246"]},{"name":"Belarus","alpha2Code":"BY","callingCodes":["375"]},{"name":"Belgium","alpha2Code":"BE","callingCodes":["32"]},{"name":"Belize","alpha2Code":"BZ","callingCodes":["501"]},{"name":"Benin","alpha2Code":"BJ","callingCodes":["229"]},{"name":"Bermuda","alpha2Code":"BM","callingCodes":["1441"]},{"name":"Bhutan","alpha2Code":"BT","callingCodes":["975"]},{"name":"Bolivia (Plurinational State of)","alpha2Code":"BO","callingCodes":["591"]},{"name":"Bonaire, Sint Eustatius and Saba","alpha2Code":"BQ","callingCodes":["5997"]},{"name":"Bosnia and Herzegovina","alpha2Code":"BA","callingCodes":["387"]},{"name":"Botswana","alpha2Code":"BW","callingCodes":["267"]},{"name":"Bouvet Island","alpha2Code":"BV","callingCodes":[""]},{"name":"Brazil","alpha2Code":"BR","callingCodes":["55"]},{"name":"British Indian Ocean Territory","alpha2Code":"IO","callingCodes":["246"]},{"name":"United States Minor Outlying Islands","alpha2Code":"UM","callingCodes":[""]},{"name":"Virgin Islands (British)","alpha2Code":"VG","callingCodes":["1284"]},{"name":"Virgin Islands (U.S.)","alpha2Code":"VI","callingCodes":["1 340"]},{"name":"Brunei Darussalam","alpha2Code":"BN","callingCodes":["673"]},{"name":"Bulgaria","alpha2Code":"BG","callingCodes":["359"]},{"name":"Burkina Faso","alpha2Code":"BF","callingCodes":["226"]},{"name":"Burundi","alpha2Code":"BI","callingCodes":["257"]},{"name":"Cambodia","alpha2Code":"KH","callingCodes":["855"]},{"name":"Cameroon","alpha2Code":"CM","callingCodes":["237"]},{"name":"Canada","alpha2Code":"CA","callingCodes":["1"]},{"name":"Cabo Verde","alpha2Code":"CV","callingCodes":["238"]},{"name":"Cayman Islands","alpha2Code":"KY","callingCodes":["1345"]},{"name":"Central African Republic","alpha2Code":"CF","callingCodes":["236"]},{"name":"Chad","alpha2Code":"TD","callingCodes":["235"]},{"name":"Chile","alpha2Code":"CL","callingCodes":["56"]},{"name":"China","alpha2Code":"CN","callingCodes":["86"]},{"name":"Christmas Island","alpha2Code":"CX","callingCodes":["61"]},{"name":"Cocos (Keeling) Islands","alpha2Code":"CC","callingCodes":["61"]},{"name":"Colombia","alpha2Code":"CO","callingCodes":["57"]},{"name":"Comoros","alpha2Code":"KM","callingCodes":["269"]},{"name":"Congo","alpha2Code":"CG","callingCodes":["242"]},{"name":"Congo (Democratic Republic of the)","alpha2Code":"CD","callingCodes":["243"]},{"name":"Cook Islands","alpha2Code":"CK","callingCodes":["682"]},{"name":"Costa Rica","alpha2Code":"CR","callingCodes":["506"]},{"name":"Croatia","alpha2Code":"HR","callingCodes":["385"]},{"name":"Cuba","alpha2Code":"CU","callingCodes":["53"]},{"name":"Curaçao","alpha2Code":"CW","callingCodes":["599"]},{"name":"Cyprus","alpha2Code":"CY","callingCodes":["357"]},{"name":"Czech Republic","alpha2Code":"CZ","callingCodes":["420"]},{"name":"Denmark","alpha2Code":"DK","callingCodes":["45"]},{"name":"Djibouti","alpha2Code":"DJ","callingCodes":["253"]},{"name":"Dominica","alpha2Code":"DM","callingCodes":["1767"]},{"name":"Dominican Republic","alpha2Code":"DO","callingCodes":["1809","1829","1849"]},{"name":"Ecuador","alpha2Code":"EC","callingCodes":["593"]},{"name":"Egypt","alpha2Code":"EG","callingCodes":["20"]},{"name":"El Salvador","alpha2Code":"SV","callingCodes":["503"]},{"name":"Equatorial Guinea","alpha2Code":"GQ","callingCodes":["240"]},{"name":"Eritrea","alpha2Code":"ER","callingCodes":["291"]},{"name":"Estonia","alpha2Code":"EE","callingCodes":["372"]},{"name":"Ethiopia","alpha2Code":"ET","callingCodes":["251"]},{"name":"Falkland Islands (Malvinas)","alpha2Code":"FK","callingCodes":["500"]},{"name":"Faroe Islands","alpha2Code":"FO","callingCodes":["298"]},{"name":"Fiji","alpha2Code":"FJ","callingCodes":["679"]},{"name":"Finland","alpha2Code":"FI","callingCodes":["358"]},{"name":"France","alpha2Code":"FR","callingCodes":["33"]},{"name":"French Guiana","alpha2Code":"GF","callingCodes":["594"]},{"name":"French Polynesia","alpha2Code":"PF","callingCodes":["689"]},{"name":"French Southern Territories","alpha2Code":"TF","callingCodes":[""]},{"name":"Gabon","alpha2Code":"GA","callingCodes":["241"]},{"name":"Gambia","alpha2Code":"GM","callingCodes":["220"]},{"name":"Georgia","alpha2Code":"GE","callingCodes":["995"]},{"name":"Germany","alpha2Code":"DE","callingCodes":["49"]},{"name":"Ghana","alpha2Code":"GH","callingCodes":["233"]},{"name":"Gibraltar","alpha2Code":"GI","callingCodes":["350"]},{"name":"Greece","alpha2Code":"GR","callingCodes":["30"]},{"name":"Greenland","alpha2Code":"GL","callingCodes":["299"]},{"name":"Grenada","alpha2Code":"GD","callingCodes":["1473"]},{"name":"Guadeloupe","alpha2Code":"GP","callingCodes":["590"]},{"name":"Guam","alpha2Code":"GU","callingCodes":["1671"]},{"name":"Guatemala","alpha2Code":"GT","callingCodes":["502"]},{"name":"Guernsey","alpha2Code":"GG","callingCodes":["44"]},{"name":"Guinea","alpha2Code":"GN","callingCodes":["224"]},{"name":"Guinea-Bissau","alpha2Code":"GW","callingCodes":["245"]},{"name":"Guyana","alpha2Code":"GY","callingCodes":["592"]},{"name":"Haiti","alpha2Code":"HT","callingCodes":["509"]},{"name":"Heard Island and McDonald Islands","alpha2Code":"HM","callingCodes":[""]},{"name":"Holy See","alpha2Code":"VA","callingCodes":["379"]},{"name":"Honduras","alpha2Code":"HN","callingCodes":["504"]},{"name":"Hong Kong","alpha2Code":"HK","callingCodes":["852"]},{"name":"Hungary","alpha2Code":"HU","callingCodes":["36"]},{"name":"Iceland","alpha2Code":"IS","callingCodes":["354"]},{"name":"India","alpha2Code":"IN","callingCodes":["91"]},{"name":"Indonesia","alpha2Code":"ID","callingCodes":["62"]},{"name":"Côte d\'Ivoire","alpha2Code":"CI","callingCodes":["225"]},{"name":"Iran (Islamic Republic of)","alpha2Code":"IR","callingCodes":["98"]},{"name":"Iraq","alpha2Code":"IQ","callingCodes":["964"]},{"name":"Ireland","alpha2Code":"IE","callingCodes":["353"]},{"name":"Isle of Man","alpha2Code":"IM","callingCodes":["44"]},{"name":"Israel","alpha2Code":"IL","callingCodes":["972"]},{"name":"Italy","alpha2Code":"IT","callingCodes":["39"]},{"name":"Jamaica","alpha2Code":"JM","callingCodes":["1876"]},{"name":"Japan","alpha2Code":"JP","callingCodes":["81"]},{"name":"Jersey","alpha2Code":"JE","callingCodes":["44"]},{"name":"Jordan","alpha2Code":"JO","callingCodes":["962"]},{"name":"Kazakhstan","alpha2Code":"KZ","callingCodes":["76","77"]},{"name":"Kenya","alpha2Code":"KE","callingCodes":["254"]},{"name":"Kiribati","alpha2Code":"KI","callingCodes":["686"]},{"name":"Kuwait","alpha2Code":"KW","callingCodes":["965"]},{"name":"Kyrgyzstan","alpha2Code":"KG","callingCodes":["996"]},{"name":"Lao People\'s Democratic Republic","alpha2Code":"LA","callingCodes":["856"]},{"name":"Latvia","alpha2Code":"LV","callingCodes":["371"]},{"name":"Lebanon","alpha2Code":"LB","callingCodes":["961"]},{"name":"Lesotho","alpha2Code":"LS","callingCodes":["266"]},{"name":"Liberia","alpha2Code":"LR","callingCodes":["231"]},{"name":"Libya","alpha2Code":"LY","callingCodes":["218"]},{"name":"Liechtenstein","alpha2Code":"LI","callingCodes":["423"]},{"name":"Lithuania","alpha2Code":"LT","callingCodes":["370"]},{"name":"Luxembourg","alpha2Code":"LU","callingCodes":["352"]},{"name":"Macao","alpha2Code":"MO","callingCodes":["853"]},{"name":"Macedonia (the former Yugoslav Republic of)","alpha2Code":"MK","callingCodes":["389"]},{"name":"Madagascar","alpha2Code":"MG","callingCodes":["261"]},{"name":"Malawi","alpha2Code":"MW","callingCodes":["265"]},{"name":"Malaysia","alpha2Code":"MY","callingCodes":["60"]},{"name":"Maldives","alpha2Code":"MV","callingCodes":["960"]},{"name":"Mali","alpha2Code":"ML","callingCodes":["223"]},{"name":"Malta","alpha2Code":"MT","callingCodes":["356"]},{"name":"Marshall Islands","alpha2Code":"MH","callingCodes":["692"]},{"name":"Martinique","alpha2Code":"MQ","callingCodes":["596"]},{"name":"Mauritania","alpha2Code":"MR","callingCodes":["222"]},{"name":"Mauritius","alpha2Code":"MU","callingCodes":["230"]},{"name":"Mayotte","alpha2Code":"YT","callingCodes":["262"]},{"name":"Mexico","alpha2Code":"MX","callingCodes":["52"]},{"name":"Micronesia (Federated States of)","alpha2Code":"FM","callingCodes":["691"]},{"name":"Moldova (Republic of)","alpha2Code":"MD","callingCodes":["373"]},{"name":"Monaco","alpha2Code":"MC","callingCodes":["377"]},{"name":"Mongolia","alpha2Code":"MN","callingCodes":["976"]},{"name":"Montenegro","alpha2Code":"ME","callingCodes":["382"]},{"name":"Montserrat","alpha2Code":"MS","callingCodes":["1664"]},{"name":"Morocco","alpha2Code":"MA","callingCodes":["212"]},{"name":"Mozambique","alpha2Code":"MZ","callingCodes":["258"]},{"name":"Myanmar","alpha2Code":"MM","callingCodes":["95"]},{"name":"Namibia","alpha2Code":"NA","callingCodes":["264"]},{"name":"Nauru","alpha2Code":"NR","callingCodes":["674"]},{"name":"Nepal","alpha2Code":"NP","callingCodes":["977"]},{"name":"Netherlands","alpha2Code":"NL","callingCodes":["31"]},{"name":"New Caledonia","alpha2Code":"NC","callingCodes":["687"]},{"name":"New Zealand","alpha2Code":"NZ","callingCodes":["64"]},{"name":"Nicaragua","alpha2Code":"NI","callingCodes":["505"]},{"name":"Niger","alpha2Code":"NE","callingCodes":["227"]},{"name":"Nigeria","alpha2Code":"NG","callingCodes":["234"]},{"name":"Niue","alpha2Code":"NU","callingCodes":["683"]},{"name":"Norfolk Island","alpha2Code":"NF","callingCodes":["672"]},{"name":"Korea (Democratic People\'s Republic of)","alpha2Code":"KP","callingCodes":["850"]},{"name":"Northern Mariana Islands","alpha2Code":"MP","callingCodes":["1670"]},{"name":"Norway","alpha2Code":"NO","callingCodes":["47"]},{"name":"Oman","alpha2Code":"OM","callingCodes":["968"]},{"name":"Pakistan","alpha2Code":"PK","callingCodes":["92"]},{"name":"Palau","alpha2Code":"PW","callingCodes":["680"]},{"name":"Palestine, State of","alpha2Code":"PS","callingCodes":["970"]},{"name":"Panama","alpha2Code":"PA","callingCodes":["507"]},{"name":"Papua New Guinea","alpha2Code":"PG","callingCodes":["675"]},{"name":"Paraguay","alpha2Code":"PY","callingCodes":["595"]},{"name":"Peru","alpha2Code":"PE","callingCodes":["51"]},{"name":"Philippines","alpha2Code":"PH","callingCodes":["63"]},{"name":"Pitcairn","alpha2Code":"PN","callingCodes":["64"]},{"name":"Poland","alpha2Code":"PL","callingCodes":["48"]},{"name":"Portugal","alpha2Code":"PT","callingCodes":["351"]},{"name":"Puerto Rico","alpha2Code":"PR","callingCodes":["1787","1939"]},{"name":"Qatar","alpha2Code":"QA","callingCodes":["974"]},{"name":"Republic of Kosovo","alpha2Code":"XK","callingCodes":["383"]},{"name":"Réunion","alpha2Code":"RE","callingCodes":["262"]},{"name":"Romania","alpha2Code":"RO","callingCodes":["40"]},{"name":"Russian Federation","alpha2Code":"RU","callingCodes":["7"]},{"name":"Rwanda","alpha2Code":"RW","callingCodes":["250"]},{"name":"Saint Barthélemy","alpha2Code":"BL","callingCodes":["590"]},{"name":"Saint Helena, Ascension and Tristan da Cunha","alpha2Code":"SH","callingCodes":["290"]},{"name":"Saint Kitts and Nevis","alpha2Code":"KN","callingCodes":["1869"]},{"name":"Saint Lucia","alpha2Code":"LC","callingCodes":["1758"]},{"name":"Saint Martin (French part)","alpha2Code":"MF","callingCodes":["590"]},{"name":"Saint Pierre and Miquelon","alpha2Code":"PM","callingCodes":["508"]},{"name":"Saint Vincent and the Grenadines","alpha2Code":"VC","callingCodes":["1784"]},{"name":"Samoa","alpha2Code":"WS","callingCodes":["685"]},{"name":"San Marino","alpha2Code":"SM","callingCodes":["378"]},{"name":"Sao Tome and Principe","alpha2Code":"ST","callingCodes":["239"]},{"name":"Saudi Arabia","alpha2Code":"SA","callingCodes":["966"]},{"name":"Senegal","alpha2Code":"SN","callingCodes":["221"]},{"name":"Serbia","alpha2Code":"RS","callingCodes":["381"]},{"name":"Seychelles","alpha2Code":"SC","callingCodes":["248"]},{"name":"Sierra Leone","alpha2Code":"SL","callingCodes":["232"]},{"name":"Singapore","alpha2Code":"SG","callingCodes":["65"]},{"name":"Sint Maarten (Dutch part)","alpha2Code":"SX","callingCodes":["1721"]},{"name":"Slovakia","alpha2Code":"SK","callingCodes":["421"]},{"name":"Slovenia","alpha2Code":"SI","callingCodes":["386"]},{"name":"Solomon Islands","alpha2Code":"SB","callingCodes":["677"]},{"name":"Somalia","alpha2Code":"SO","callingCodes":["252"]},{"name":"South Africa","alpha2Code":"ZA","callingCodes":["27"]},{"name":"South Georgia and the South Sandwich Islands","alpha2Code":"GS","callingCodes":["500"]},{"name":"Korea (Republic of)","alpha2Code":"KR","callingCodes":["82"]},{"name":"South Sudan","alpha2Code":"SS","callingCodes":["211"]},{"name":"Spain","alpha2Code":"ES","callingCodes":["34"]},{"name":"Sri Lanka","alpha2Code":"LK","callingCodes":["94"]},{"name":"Sudan","alpha2Code":"SD","callingCodes":["249"]},{"name":"Suriname","alpha2Code":"SR","callingCodes":["597"]},{"name":"Svalbard and Jan Mayen","alpha2Code":"SJ","callingCodes":["4779"]},{"name":"Swaziland","alpha2Code":"SZ","callingCodes":["268"]},{"name":"Sweden","alpha2Code":"SE","callingCodes":["46"]},{"name":"Switzerland","alpha2Code":"CH","callingCodes":["41"]},{"name":"Syrian Arab Republic","alpha2Code":"SY","callingCodes":["963"]},{"name":"Taiwan","alpha2Code":"TW","callingCodes":["886"]},{"name":"Tajikistan","alpha2Code":"TJ","callingCodes":["992"]},{"name":"Tanzania, United Republic of","alpha2Code":"TZ","callingCodes":["255"]},{"name":"Thailand","alpha2Code":"TH","callingCodes":["66"]},{"name":"Timor-Leste","alpha2Code":"TL","callingCodes":["670"]},{"name":"Togo","alpha2Code":"TG","callingCodes":["228"]},{"name":"Tokelau","alpha2Code":"TK","callingCodes":["690"]},{"name":"Tonga","alpha2Code":"TO","callingCodes":["676"]},{"name":"Trinidad and Tobago","alpha2Code":"TT","callingCodes":["1868"]},{"name":"Tunisia","alpha2Code":"TN","callingCodes":["216"]},{"name":"Turkey","alpha2Code":"TR","callingCodes":["90"]},{"name":"Turkmenistan","alpha2Code":"TM","callingCodes":["993"]},{"name":"Turks and Caicos Islands","alpha2Code":"TC","callingCodes":["1649"]},{"name":"Tuvalu","alpha2Code":"TV","callingCodes":["688"]},{"name":"Uganda","alpha2Code":"UG","callingCodes":["256"]},{"name":"Ukraine","alpha2Code":"UA","callingCodes":["380"]},{"name":"United Arab Emirates","alpha2Code":"AE","callingCodes":["971"]},{"name":"United Kingdom of Great Britain and Northern Ireland","alpha2Code":"GB","callingCodes":["44"]},{"name":"United States of America","alpha2Code":"US","callingCodes":["1"]},{"name":"Uruguay","alpha2Code":"UY","callingCodes":["598"]},{"name":"Uzbekistan","alpha2Code":"UZ","callingCodes":["998"]},{"name":"Vanuatu","alpha2Code":"VU","callingCodes":["678"]},{"name":"Venezuela (Bolivarian Republic of)","alpha2Code":"VE","callingCodes":["58"]},{"name":"Viet Nam","alpha2Code":"VN","callingCodes":["84"]},{"name":"Wallis and Futuna","alpha2Code":"WF","callingCodes":["681"]},{"name":"Western Sahara","alpha2Code":"EH","callingCodes":["212"]},{"name":"Yemen","alpha2Code":"YE","callingCodes":["967"]},{"name":"Zambia","alpha2Code":"ZM","callingCodes":["260"]},{"name":"Zimbabwe","alpha2Code":"ZW","callingCodes":["263"]}]
	';
}

function stak_fetch_phone_codes()
{
	// $response = wp_remote_get(REST_PHONE_ALL, [
	// 	'timeout' => 300
	// ]);
	// var_dump($response);
	$json = json_phone_codes();
	// print_r($json); exit;
	$obj = json_decode($json);
	// print_r($obj); exit;
	// <select name="billing_wooccm11" id="billing_wooccm11" class="select " data-required="1" data-placeholder="">
	$return = '';
	foreach($obj as $data){
		$return .= '<option value="+'.$data->callingCodes[0].'" name="'.$data->alpha2Code.'">'.$data->name.'(+'.$data->callingCodes[0].')</option>';
	}
	// var_dump($return); 
	echo $return;
	exit;
}
add_action( 'wp_ajax_stak_fetch_phone_codes', 'stak_fetch_phone_codes' );
// If you wanted to also use the function for non-logged in users (in a theme for example)
add_action( 'wp_ajax_nopriv_stak_fetch_phone_codes', 'stak_fetch_phone_codes' );

remove_action('woocommerce_register_form', 'wc_registration_privacy_policy_text', 20);

add_filter( 'woocommerce_package_rates', 'stak_shipping_count_rates_cost', 10, 2 );
function stak_shipping_count_rates_cost( $rates, $pack )
{
	$file = get_stylesheet_directory().'/data/shipping-rates.csv';
	// var_dump($file);
	$rows = array_map('str_getcsv', file($file));
	$header = array_shift($rows);
	$countries = [];

	foreach ($rows as $row) {
		$countries[] = array_combine($header, $row);
	}
	// var_dump($countries);
	$ship_rates = [];
	foreach($countries as $country){
		$ship_rates[$country['code']] = [
			'A' => floatval($country['A']),
			'B' => floatval($country['B']),
			'C' => floatval($country['C']),
			'D' => floatval($country['D'])
		];
	}
	// var_dump($ship_rates);
	// var_dump($rates); exit;
	$has_rate = isset($rates['flat_rate:24']) ? true : false;
	$hk_rate = isset($rates['flat_rate:12']);
	$rate_id = null;
	foreach($rates as $id => $val){
		$rate_id = $id;
		continue;
	}
	// var_dump($pack);
	$contents = isset($pack['contents']) ? $pack['contents'] : null;
	$cart = WC()->cart->get_cart();
	// var_dump($cart); 
	$sizes = stak_get_cart_sizes($contents);
	// var_dump($sizes); exit;
	$ship_rate = null;
	if($has_rate){
		$country = $pack['destination']['country'];
		$ship_rate = isset($ship_rates[$country]) ? $ship_rates[$country] : null;
		// var_dump($ship_rate); exit;
		$rate = stak_calculate_shipping_cost( $sizes, $ship_rate, $cart );
		if(!$rate){
			return false;
			exit;
		}
		$rates[$rate_id]->cost = floatval($rate);
		$rates[$rate_id]->label = 'SF E-Commerce Express';
	}else{
		if($hk_rate){
			// $num_pack = ceil($cart_number/intval(NUM_PER_PACK));
			$cost = 0.00;
			$total_cost = 0.00;
			foreach($cart as $id => $item){
				// var_dump($item);
				$num_pack = ceil($item['quantity']/intval(NUM_PER_PACK));
				$cost = $num_pack*100.00;
				// var_dump($cost);
				$total_cost = $total_cost + $cost;
			}
			// var_dump($total_cost); exit;
			$rates[$rate_id]->cost = $total_cost;
		}
	}
	// var_dump($rates);

	parse_str($_REQUEST['post_data'], $_post);
	// var_dump(isset($_post['_custom_shipping']) && $_post['_custom_shipping'] == 1); exit;
	if(isset($_post['_custom_shipping']) && $_post['_custom_shipping'] == 1){
		foreach($rates as $key => $rate){
			$rates[$key]->cost = 0.00;
			$rates[$key]->label = 'Custom Shipping';
		}
		$rates[$rate_id]->cost = 0.00;
		$rates[$rate_id]->label = 'Custom Shipping';
	}
	// $session = WC()->session;
	// $session->set('shipping_method_counts', [count($rates)]);
	// $session->set('chosen_shipping_methods', [$rates[0]->id]);

	// var_dump($rates); exit;

	// exit;
	return $rates;
}

// add_action('woocommerce_checkout_update_order_review', 'stak_woocommerce_checkout_update_order_review', 10, 2);
function stak_woocommerce_checkout_update_order_review($posted_data)
{
	// var_dump($_REQUEST['shipping_method']); exit;
	if(!isset($_REQUEST['post_data']))
		return $rates;

	// $_post = $_REQUEST['post_data'];
	parse_str($_REQUEST['post_data'], $_post);
	// var_dump(isset($_post['_custom_shipping']) && $_post['_custom_shipping'] == 1); exit;
	if(isset($_post['_custom_shipping']) && $_post['_custom_shipping'] == 1){
		
	}
	try {
		WC()->cart->calculate_shipping();
	} catch (Exception $e) {
		var_dump($e->getCode().' - '.$e->getMessage());
	}
	
	return;
}

function stak_calculate_shipping_cost( $sizes, $ship_rate, $cart )
{
	$cost = 0.00;
	$total_cost = 0.00;
	$max_num_per_pack = intval(NUM_PER_PACK);
	$num_pack = 0;
	$length = 0.00;
	$class = false;

	if(null !== $ship_rate){
		foreach($cart as $id => $item){
			$num_pack = ceil($item['quantity']/$max_num_per_pack);
			// var_dump($num_pack);
			$length = floatval($sizes[$id]);
			// var_dump($length);
			// var_dump($ship_rate);

			// classified the cost based on size ($length).
			if(floatval($length) <= 56.00)
				$class = 'A';
			if(floatval($length) > 56.00 && floatval($length) <= 86.40)
				$class = 'B';
			if(floatval($length) > 86.40 && floatval($length) <= 106.80)
				$class = 'C';
			if(floatval($length) > 106.80 && floatval($length) <= 157.60)
				$class = 'D';

			if(!$class)
				return false;

			$cost = floatval(floatval($ship_rate[$class]) * $num_pack);
			$total_cost = $total_cost + $cost;
		}
		
		// var_dump($total_cost); exit;
	}

	return floatval($total_cost);
}

function stak_get_cart_sizes( $contents )
{
	$sizes = [];
	if(null !== $contents){
		// var_dump($contents);
		foreach($contents as $id => $content){
			if($content['variation_id'] !== 0){
				$item = wc_get_product($content['variation_id']);
				// var_dump($content);
				if(floatval($item->width) < floatval($item->height)){
					$sizes[$id] = floatval($item->width);
				}elseif(floatval($item->height) < floatval($item->width)){
					$sizes[$id] = floatval($item->height);
				}else{
					$sizes[$id] = floatval($item->height);
				}
			}else{
				// var_dump($content);
				if(($content['custom_data']['pa_img_width'] != '' && $content['custom_data']['pa_img_width'] != 'c') && ($content['custom_data']['pa_img_height'] != '' && $content['custom_data']['pa_img_height'] != 'c')){
					$width = floatval($content['custom_data']['pa_img_width']);
					$height = floatval($content['custom_data']['pa_img_height']);
					if(floatval($width) < floatval($height)){
						$sizes[$id] = floatval($width);
					}elseif(floatval($height) < floatval($width)){
						$sizes[$id] = floatval($height);
					}else{
						$sizes[$id] = floatval($height);
					}
				}
			}
		}
	}
	// var_dump($sizes);
	return $sizes;
}

// shipping with no rate.
add_action( 'woocommerce_after_checkout_validation', 'stak_no_shipping_rate', 9999, 2 );
function stak_no_shipping_rate( $data, $errors )
{
	// var_dump($errors);
	$_errors = $errors->errors;
	// var_dump($_errors);
	if(isset($_errors['shipping'])){
		// there is no shipping available.
		$errors->remove('shipping');
		$string = '<p>We have detected that your shipping address country is outside our shipping service coverage.</p>';
		$string .= '<p>Please send an email to <a href="'.get_option( 'admin_email' ).'">'.get_option( 'admin_email' ).'</a>.</p>';
		// $string .= '<p>We have received your order and will get back to you through email to inform you about the cost to ship your purchase to your shipping address.</p>';
		$errors->add('shipping', __($string, 'woocommerce'));
	}

	if(isset($errors->errors['required-field']) && count($errors->errors['required-field']) > 0){
		$reqs = $errors->errors['required-field'];
		foreach($reqs as $key => $string){
			if($string == '<strong>Billing Phone Number</strong> is a required field.')
				unset($reqs[$key]);
			if(strpos($string, '<strong>Billing </strong>') !== false){
				$reqs[$key] = '<strong>Billing Phone Number</strong> is a required field.';
			}
		}
		if(count($reqs) > 0){
			$errors->errors['required-field'] =  $reqs;
		}else{
			$errors->remove('required-field');
		}
		
	}
	// var_dump($errors->errors); exit;
}

function stak_woocommerce_in_cart_product( $item_prod_id ) { 
	// var_dump($item_prod_id); exit;
    return $item_prod_id;
};
add_filter( 'woocommerce_in_cart_product', 'stak_woocommerce_in_cart_product', 999, 2 );