<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.8.0
 */

defined( 'ABSPATH' ) || exit;

wc_print_notices();
wp_enqueue_style('swz_wc_fontawesome','https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
do_action( 'woocommerce_before_cart' ); ?>
<style>
.woocommerce table.swz_wc.shop_table tbody td{
	border-color: #000;
}
.woocommerce table.swz_wc.shop_table{
	border:none;
}
#butbrclr{
	border: 2px solid #7e7e7e7e !important;
	border-radius: 8px;
}
/*
Step 1
======
Style your page (the product list)
*/
.product {
	float: left;
	margin: 10px;
}

.product .product-images,
.product .product-form {
	display: none;
}

/*
Step 2
======
Reposition and redesign fancyBox blocks
*/
/* This elements contains both blocks */
.fancybox-inner {
	position: absolute;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
	margin: auto;
	width: calc(100% - 40px);
	height: calc(100% - 40px);
	max-width: 800px;
	max-height: 600px;
}

/* Left block will contain the gallery */
.fancybox-stage {
	width: 52%;
	background: #fff;
}

/* Right block - close button and the form */
.fancybox-form-wrap {
	position: absolute;
	top: 40px;
	right: 0;
	bottom: 40px;
	width: 48%;
	background: #fff;
}

/* Add vertical lines */
.fancybox-form-wrap::before,
.fancybox-form-wrap::after {
	content: '';
	position: absolute;
	top: 0;
	left: 0;
	bottom: 0;
}

.fancybox-form-wrap::before {
	width: 8px;
	background: #f4f4f4;
}

.fancybox-form-wrap::after {
	width: 1px;
	background: #e9e9e9;
}

/* Set position and colors for close button */
.fancybox-button--close {
	border-width: 0;
	position: absolute;
	top: 0;
	right: 0;
	background: #F0F0F0;
	color: #222;
	padding: 7px;
}

.fancybox-button:hover {
	color: #111;
	background: #e4e4e4;
}

.fancybox-button svg path {
	stroke-width: 1;
}

/* Set position of the form */
.fancybox-inner .product-form {
	overflow: auto;
	position: absolute;
	top: 50px;
	right: 0;
	bottom: 50px;
	left: 0;
	padding: 0 50px;
	text-align: center;
}

/*
Step 3
======
Tweak fade animation
*/
.fancybox-inner {
	opacity: 0;
	transition: opacity .3s;
}

.fancybox-is-open .fancybox-inner {
	opacity: 1;
}

.fancybox-is-closing .fancybox-fx-fade {
	opacity: 1 !important;
	/* Prevent double-fading */
}

/*
Step 2
======
Bullet navigation design
*/
.product-bullets {
	display: none;
	list-style: none;
	position: absolute;
	bottom: 0;
	left: 0;
	width: 100%;
	text-align: center;
	margin: 0;
	padding: 0;
	z-index: 99999;
	-webkit-tap-highlight-color: rgba(0, 0, 0, 0);
}

.product-bullets li {
	display: inline-block;
	vertical-align: top;
}

.product-bullets li a {
	display: block;
	height: 30px;
	width: 20px;
	position: relative;
}

.product-bullets li a span {
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
	width: 10px;
	height: 10px;
	border-radius: 99px;
	text-indent: -99999px;
	overflow: hidden;
	background: #fff;
	box-shadow: 0px 0px 2px rgba(0, 0, 0, 0.5);
}

.product-bullets li.active a span {
	background: #FF6666;
}
</style>
<form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
	<?php do_action( 'woocommerce_before_cart_table' ); ?>

	<table class="swz_wc shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
		<thead>

			<tr>
				<th class="product-thumbnail">ARTWORK</th>
				<th class="product-name"><?php esc_html_e( 'TITLE', 'woocommerce' ); ?></th>
				<th class="product-price"><?php esc_html_e( 'PRICE PER ARTWORK', 'woocommerce' ); ?></th>
				<th class="product-quantity"><?php esc_html_e( 'QUANTITY', 'woocommerce' ); ?></th>
				<th class="product-remove">ACTION</th>
				<th class="product-subtotal"><?php esc_html_e( 'TOTAL', 'woocommerce' ); ?></th>
				
			</tr>
		</thead>
		<tbody>
			<?php do_action( 'woocommerce_before_cart_contents' ); ?>

			<?php
			$index = 0;
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				// var_dump($cart_item);
				$index++;
				$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
					$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
					?>
					<tr class="woocommerce-cart-form__cart-item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">

						<td class="product-thumbnail">
						<?php
						$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
						// print_r($_product->get_image());
						if(isset($cart_item['custom_data']['pa_cropped_img']) && null !== $cart_item['custom_data']['pa_cropped_img']){
							$thumbnail = '<img width="300" height="300" src="'.$cart_item['custom_data']['pa_cropped_img'].'" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset="'.$cart_item['custom_data']['pa_cropped_img'].' 300w, '.$cart_item['custom_data']['pa_cropped_img'].' 150w, '.$cart_item['custom_data']['pa_cropped_img'].' 85w, '.$cart_item['custom_data']['pa_cropped_img'].' 80w" sizes="(max-width: 300px) 100vw, 300px">';
						}

						if ( ! $product_permalink ) {
							echo wp_kses_post( $thumbnail );
						} else {
							printf( '<a class="img_trigger" href="javascript:;" data-view="%s">%s</a>', 'quick-view-'.$index, wp_kses_post( $thumbnail ) );
						}
						?>
						</td>

						<td class="product-name" data-title="<?php esc_attr_e( 'TITLE', 'woocommerce' ); ?>">
							<div class="product">
								<?php
								// var_dump($cart_item);
								$sessionDimension = "";
								$productId = $_product->get_id();
								$medium = null !== $cart_item['custom_data']['pa_medium'] &&  $cart_item['custom_data']['pa_medium'] !== '' ?  ucfirst($cart_item['custom_data']['pa_medium']) : 'Paper'; // for now everything will be paper.

								// we'll replace the session value with a more reliable and accurate data.
								/*if(isset($_SESSION['customizeProduct'][$productId])){
									$postdata = $_SESSION['customizeProduct'][$productId];
									$sessionDimension = ' - '.$postdata['width_image'].'x'.$postdata['height_image'].' cm - '.$medium; 
								}else{
									$sessionDimension = ' - $cart_item['custom_data']'.$medium;
								}*/
								$custom = (isset($cart_item['custom_data']['pa_action']) && null !== $cart_item['custom_data']['pa_action']) && ($cart_item['custom_data']['pa_action'] == 'save_customized_order' || $cart_item['custom_data']['pa_action'] == 'stak_custom_art_add_to_cart');
								// var_dump($custom);
								$custom_product = '';
								if( $custom ){
									if( is_numeric( $cart_item['custom_data']['pa_img_width'] ) && is_numeric( $cart_item['custom_data']['pa_img_height'] ) ){
										$custom_product = ' - '.$cart_item['custom_data']['pa_img_width'].' x '.$cart_item['custom_data']['pa_img_height'].' cm - '.$medium;
									}
								}else{
									if(isset($_SESSION['customizeProduct'][$productId])){
										$postdata = $_SESSION['customizeProduct'][$productId];
										$custom_product = ' - '.$postdata['width_image'].'x'.$postdata['height_image'].' cm'; 
									}
									$custom_product = $custom_product.' - '.$medium;
								}

								if ( ! $product_permalink ) {
									echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' );
								} else {
									$img_url = isset($cart_item['custom_data']['pa_cropped_img']) && null !== $cart_item['custom_data']['pa_cropped_img'] ? $cart_item['custom_data']['pa_cropped_img'] : wp_get_attachment_url( $_product->get_image_id(), 'full' );
									echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a data-fancybox="quick-view-%s" href="%s" data-caption="%s" data-type="image">%s</a>', $index, esc_url( $img_url ), $_product->get_name(), $_product->get_name().$custom_product ), $cart_item, $cart_item_key ) );
								}

								// Meta data.
								echo wc_get_formatted_cart_item_data( $cart_item ); // PHPCS: XSS ok.

								// Backorder notification.
								if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
									echo wp_kses_post( apply_filters( 'woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>' ) );
								}
								?>

								<div class="product-images"></div>
								<div class="product-form">
									<?php $title_arr = explode(' - ', $_product->get_name()); ?>
									<div class="quick-view-mobile-img">
										<?php echo $thumbnail; ?>
									</div>
									<p><?php echo $title_arr[1]; ?></p>
									<h4> <?php echo $title_arr[0]; ?></h4>
									<p><em><?php echo $custom ? 'CUSTOM' : 'DEFAULT'; ?></em></p>
									<ul>
										<?php if( null !== $cart_item['custom_data']['pa_img_width'] && is_numeric( $cart_item['custom_data']['pa_img_width'] ) ): ?>
											<li><strong>Width</strong>: <?php echo $cart_item['custom_data']['pa_img_width']; ?> cm</li>
										<?php endif; ?>
										<?php if( null !== $cart_item['custom_data']['pa_img_height'] && is_numeric( $cart_item['custom_data']['pa_img_height'] ) ): ?>
											<li><strong>Height</strong>: <?php echo $cart_item['custom_data']['pa_img_height']; ?> cm</li>
										<?php endif; ?>
										<li><strong>Medium</strong>: <?php echo $medium; ?></li>
									</ul>
									<p style="text-align: center; display: none;">
										<a href="javascript:;" class="button2 btn-payment"
											style="
												float: none!important;
												position: relative;
												top: 25px;
											"
											>
											PURCHASE ANOTHER
										</a>
									</p>
								</div>
							</div>
						</td>

						<td class="product-price" data-title="<?php esc_attr_e( 'PRICE PER ARTWORK', 'woocommerce' ); ?>">
							<?php
								echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
							?>
						</td>

						<td class="product-quantity" data-title="<?php esc_attr_e( 'QUANTITY', 'woocommerce' ); ?>">
						<?php
						if ( $_product->is_sold_individually() ) {
							$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
						} else {
							$product_quantity = woocommerce_quantity_input( array(
								'input_name'   => "cart[{$cart_item_key}][qty]",
								'input_value'  => $cart_item['quantity'],
								'max_value'    => $_product->get_max_purchase_quantity(),
								'min_value'    => '0',
								'product_name' => $_product->get_name(),
							), $_product, false );
						}

						echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item ); // PHPCS: XSS ok.
						?>
						</td>

						

						<td class="product-remove">
							<?php
								echo apply_filters( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
									'woocommerce_cart_item_remove_link',
									sprintf(
										'<a href="%s" class="" aria-label="%s" data-product_id="%s" data-product_sku="%s"><span class="fa fa-trash-o"></span></a>',
										esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
										esc_html__( 'Remove this item', 'woocommerce' ),
										esc_attr( $product_id ),
										esc_attr( $_product->get_sku() )
									),
									$cart_item_key
								);
							?>
						</td>
						<td class="product-subtotal" data-title="<?php esc_attr_e( 'TOTAL', 'woocommerce' ); ?>">
							<?php
								echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
							?>
						</td>

					</tr>
					<?php
				}
			}
			?>

			<?php do_action( 'woocommerce_cart_contents' ); ?>
			<tr class="woocommerce-footer-table">
				<td colspan="6">
					<div class="column one">
						<a style="float:left;" href="<?php echo site_url('/gallery') ?>" class="button alt black-color" id="butbrclr" data-process="continue">Continue Creating</a>
						<a style="float:right;" href="<?php echo site_url('/checkout') ?>" class="button alt black-color" id="butbrclr" data-process="checkout">Checkout</a>
					</div>
				</td>
			</tr>

			<tr style="display:none;">
				<td colspan="6" class="actions">

					<?php if ( wc_coupons_enabled() ) { ?>
						<div class="coupon">
							<label for="coupon_code"><?php esc_html_e( 'Coupon:', 'woocommerce' ); ?></label> <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" /> <button type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>"><?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?></button>
							<?php do_action( 'woocommerce_cart_coupon' ); ?>
						</div>
					<?php } ?>

					<button type="submit" class="button update_cart_btn" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>"><?php esc_html_e( 'Update cart', 'woocommerce' ); ?></button>

					<?php do_action( 'woocommerce_cart_actions' ); ?>

					<?php
						// WC < 3.4 backward compatibility
						if( version_compare( WC_VERSION, '3.4', '<' ) ){
							wp_nonce_field( 'woocommerce-cart' );
						} else {
							wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' );
						}
					?>

				</td>
			</tr>

			<?php do_action( 'woocommerce_after_cart_contents' ); ?>
		</tbody>
	</table>
	<?php do_action( 'woocommerce_after_cart_table' ); ?>
</form>
<script>
jQuery(".woocommerce-cart-form").on("change",".qty",function(){
	jQuery(".update_cart_btn").click();
});
jQuery(".moveFromcart").click(function(){
	var processStatus = $(this).data('process');
	if(processStatus == 'checkout'){
		window.location = "<?php echo site_url('/checkout') ?>";		
	}else{
		window.location = "<?php echo site_url('/shop') ?>";
	}
	return true;
});
</script>
<script type="text/javascript">
	(function( $ ) {
		'use strict';

		$(function(){
			$('body').on('click', '.img_trigger', function(e){
				e.preventDefault();
				var toggle = $(this).attr('data-view');
				$('a[data-fancybox="'+toggle+'"]').trigger('click');
			});
			$('[data-fancybox^="quick-view"]').fancybox({
				animationEffect: "fade",
				animationDuration: 700,
				margin: 0,
				gutter: 0,
				touch: {
					vertical: false
				},
				baseTpl: '<div class="fancybox-container" role="dialog" tabindex="-1">' +
					'<div class="fancybox-bg"></div>' +
					'<div class="fancybox-inner">' +
					'<div class="fancybox-stage quick-view"></div>' +
					'<div class="fancybox-form-wrap">' +
					'<button data-fancybox-close class="fancybox-button fancybox-button--close" title="{{CLOSE}}">' +
					'<svg viewBox="0 0 40 40">' +
					'<path d="M10,10 L30,30 M30,10 L10,30" />' +
					'</svg>' +
					'</button></div>' +
					'</div>' +
					'</div>',
				onInit: function(instance) {

					/*

						#1 Add product form
						===================

					*/

					// Find current form element ..
					var current = instance.group[instance.currIndex];
					instance.$refs.form = current.opts.$orig.parent().find('.product-form');

					// .. and move to the container
					instance.$refs.form.appendTo(instance.$refs.container.find('.fancybox-form-wrap'));

					/*

						#2 Create bullet navigation links
						=================================

					*/
					var list = '',
						$bullets;

					for (var i = 0; i < instance.group.length; i++) {
						list += '<li><a data-index="' + i + '" href="javascript:;"><span>' + (i + 1) + '</span></a></li>';
					}

					$bullets = $('<ul class="product-bullets">' + list + '</ul>').on('click touchstart', 'a', function() {
						var index = $(this).data('index');

						$.fancybox.getInstance(function() {
							this.jumpTo(index);
						});

					});

					instance.$refs.bullets = $bullets.appendTo(instance.$refs.stage);

				},
				beforeShow: function(instance) {

					// Mark current bullet navigation link as active
					instance.$refs.stage.find('ul:first')
						.children()
						.removeClass('active')
						.eq(instance.currIndex)
						.addClass('active');

				},
				afterClose: function(instance, current) {

					// Move form back to the place
					instance.$refs.form.appendTo(current.opts.$orig.parent());

				}
			});
		});
	})( jQuery );
	
</script>

<?php do_action( 'woocommerce_before_cart_collaterals' ); ?>

<div class="cart-collaterals">
	<?php
		/**
		 * Cart collaterals hook.
		 *
		 * @hooked woocommerce_cross_sell_display
		 * @hooked woocommerce_cart_totals - 10
		 */
		//do_action( 'woocommerce_cart_collaterals' );
	?>
</div>

<?php do_action( 'woocommerce_after_cart' ); ?>
