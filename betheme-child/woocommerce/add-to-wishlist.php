<?php
/**
 * Add to wishlist template
 *
 * @author Your Inspiration Themes
 * @package YITH WooCommerce Wishlist
 * @version 2.0.0
 */

if ( ! defined( 'YITH_WCWL' ) ) {
	exit;
} // Exit if accessed directly

global $product;
// var_dump(intval( $product->get_id() ));
// var_dump($exists);
// $exists = YITH_WCWL()->is_product_in_wishlist( intval( $product->get_id() ) );

if(!$exists){
	if(isset($_SESSION['stak_wishlist_var']) && $_SESSION['stak_wishlist_var'] != ''){
		$exists = YITH_WCWL()->is_product_in_wishlist( intval( $_SESSION['stak_wishlist_var'] ) );
		$product_id = intval( $_SESSION['stak_wishlist_var'] );
		unset($_SESSION['stak_wishlist_var']);
	}else{
		$var_ids = $product->get_children();
		// var_dump($var_ids);
		foreach($var_ids as $id) {
			if(YITH_WCWL()->is_product_in_wishlist( intval( $id ) )){
				$exists = true;
				$product_id = intval($id);
			}
		}
	}
}

// var_dump($exists);
?>
<input type="hidden" name="variation_id" id="variation_id" value="<?php echo $exists ? intval($product_id) : ''; ?>" />
<div class="yith-wcwl-add-to-wishlist add-to-wishlist-<?php echo $product_id ?>">
	<?php if( ! ( $disable_wishlist && ! is_user_logged_in() ) ): ?>
		<div class="yith-wcwl-add-button <?php echo ( $exists && ! $available_multi_wishlist ) ? 'hide': '' ?>" style="display:<?php echo ( $exists ) ? 'none': 'inline-block' ?>; float: right;">

			<?php // yith_wcwl_get_template( 'add-to-wishlist-' . $template_part . '.php', $atts ); ?>
			<?php if(is_user_logged_in()): ?>
				<a href="<?php echo esc_url( add_query_arg( 'add_to_wishlist', $product_id ) )?>" rel="nofollow" data-product-id="<?php echo $product_id ?>" data-product-type="<?php echo $product_type?>" class="<?php echo $link_classes ?> addToWishlist" >
					<?php echo $icon ?>
					<span><i class="far fa-heart"></i></span>
				</a>
			<?php else: ?>
				<a href="javascript:;" rel="nofollow" data-product-id="<?php echo $product_id ?>" data-product-type="<?php echo $product_type?>" data-toggle="modal" data-target="#loginFormModal" class="<?php echo $link_classes ?> addToWishlist" >
				    <?php echo $icon ?>
				    <span><i class="far fa-heart"></i></span>
				</a>
			<?php endif; ?>
			<img src="<?php echo esc_url( YITH_WCWL_URL . 'assets/images/wpspin_light.gif' ) ?>" class="ajax-loading" alt="loading" width="16" height="16" style="visibility:hidden" />

		</div>

		<div class="yith-wcwl-wishlistaddedbrowse" style="display: none; float: right;">
			<!-- <span class="feedback"><?php // echo $product_added_text ?></span> -->
			<a class="stak_wl remove_from_wishlist" data-product-id="<?php echo $product_id; ?>" href="<?php echo esc_url( add_query_arg( 'remove_from_wishlist', $product_id ) )?>" rel="nofollow">
				<?php // echo apply_filters( 'yith-wcwl-browse-wishlist-label', $browse_wishlist_text, $product_id, $icon ); ?>
				<span class="feedback"><i class="fas fa-heart"></i></span>
			</a>
		</div>

		<div class="yith-wcwl-wishlistexistsbrowse <?php echo ( $exists && ! $available_multi_wishlist ) ? 'show' : 'hide' ?>" style="display:<?php echo ( $exists ) ? 'block' : 'none' ?>">
			<a class="stak_wl remove_from_wishlist" data-product-id="<?php echo $product_id; ?>" href="<?php echo esc_url( add_query_arg( 'remove_from_wishlist', $product_id ) )?>" rel="nofollow">
				<?php // echo apply_filters( 'yith-wcwl-browse-wishlist-label', $browse_wishlist_text, $product_id, $icon ); ?>
				<span class="feedback"><i class="fas fa-heart"></i></span>
			</a>

			<!-- <span class="feedback" onclick="window.location.href='<?php echo esc_url( $wishlist_url ); ?>'">
				<i class="fas fa-heart"></i>
			</span> -->
			<!-- <div class="stak-tooltip">
				<p>
					<?php echo $already_in_wishslist_text ?>
					<a href="<?php echo esc_url( $wishlist_url ) ?>" rel="nofollow">
						<?php echo apply_filters( 'yith-wcwl-browse-wishlist-label', $browse_wishlist_text, $product_id, $icon )?>
					</a>
				</p>
			</div> -->
		</div>
		<a 
			class="pull-right stak-full-width" 
			href="javascript:;" 
			onclick="javascript:document.getElementsByClassName('woocommerce-product-gallery__trigger')[0].click();"
		>
			View Fullscreen
			<img src="<?php echo site_url('/wp-content/uploads/fullscreen.png'); ?>"/>
		</a>
		<div class="yith-wcwl-wishlistaddresponse"></div>
		
	<?php else: ?>
		<a href="<?php echo esc_url( add_query_arg( array( 'wishlist_notice' => 'true', 'add_to_wishlist' => $product_id ), get_permalink( wc_get_page_id( 'myaccount' ) ) ) )?>" rel="nofollow" class="<?php echo str_replace( 'add_to_wishlist', '', $link_classes ) ?>" >
			<?php echo $icon ?>
			<?php echo $label ?>
		</a>
	<?php endif; ?>

</div>

<div class="clear"></div>

<?php if(!is_user_logged_in()): ?>
	<!-- modal for login/register payment -->
	<?php include_once(get_stylesheet_directory().'/woocommerce/includes/modal-login-register.php'); ?>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
	<script type="text/javascript">
		(function( $ ) {
			'use strict';
			$(function(){
				$(document).on('click', 'a.addToWishlist', function(e){
					e.preventDefault();
					// e.stopImmediatePropagation();
					// $.blockUI({ message: null });
					var is_modal = $(this).attr('data-toggle') == 'modal' ? true : false;
					if(is_modal){
						$.ajax({
							url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
							type: 'POST',
							data: {
								action: 'stak_add_wishlist_guest',
								add_to_wishlist: $(this).data('product-id'),
								product_type: $(this).data('product-type'),
							},
							success: function(data){
								console.log(data);
								$('div.yith-wcwl-add-button').hide();
								$('div.yith-wcwl-wishlistaddedbrowse').show();
								if(data.status == 'ok'){
									$('input#redirect-to').val('wishlist');
									$('#loginFormModal').modal({
										backdrop: 'static',
										keyboard: false,
										show: true
									});
								}
							},
							error: function(error){
								console.log(error);
							}
						});
					}
				});
				$('#loginFormModal').on('hidden.bs.modal', function(e){
					setTimeout(function(){
						// $.unblockUI();
					}, 1500);
					
				});
			});
		})( jQuery );
	</script>
<?php endif; ?>

<script type="text/javascript">
	(function( $ ) {
		'use strict';

		function retrieve_fragments( search ) {
		    var options = {},
		        fragments = null;

		    if( search ){
		        if( typeof search === 'object' ){
		            search = $.extend( {
		                s: '',
		                container: $(document),
		                firstLoad: false
		            }, search );

		            fragments = search.container.find( '.wishlist-fragment' );

		            if( search.s ){
		                fragments = fragments.not('[data-fragment-ref]').add(fragments.filter('[data-fragment-ref="' + search.s + '"]'));
		            }

		            if( search.firstLoad ){
		                fragments = fragments.filter( '.on-first-load' );
		            }
		        }
		        else {
		            fragments = $('.wishlist-fragment');

		            if (typeof search == 'string' || typeof search == 'number') {
		                fragments = fragments.not('[data-fragment-ref]').add(fragments.filter('[data-fragment-ref="' + search + '"]'));
		            }
		        }
		    }
		    else{
		        fragments = $('.wishlist-fragment');
		    }

		    fragments.each( function(){
		        var t = $(this),
		            id = t.attr('class');

		        options[ id ] = t.data('fragment-options');
		    } );

		    return options;
		}

		$(function(){
			$(document).on('click', 'a.stak_wl.remove_from_wishlist', function(e){
				e.preventDefault();
				$.blockUI({ message: null });
				$.ajax({
					url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
					type: 'POST',
					data: {
						action: 'remove_from_wishlist',
						remove_from_wishlist: $(this).data('product-id'),
						fragments: retrieve_fragments( $(this).data('product-id') )
					},
					success: function(data){
						console.log(data);
						if(data.fragments !== undefined){
							location.reload();
						}
					},
					error: function(error){
						console.log(error);
					}
				});
			});
		});
	})( jQuery );
</script>
