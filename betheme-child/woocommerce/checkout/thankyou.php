<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;
$order_items = $order->get_items( apply_filters( 'woocommerce_purchase_order_item_types', 'line_item' ) );
?>
<style>
    .progressbar-swz-wc {
        counter-reset: step;
    }
    .progressbar-swz-wc li {
        list-style: none;
        display: inline-block;
        width: 17.33%;
        position: relative;
        text-align: center;
        cursor: pointer;
        color: #5a5a5a;
        font-family:Lato;
        font-size:12px;
        font-weight:700;
    }
    .progressbar-swz-wc li:before {
        content: "";
        width: 7px;
        height: 7px;
        line-height : 30px;
        border: none;

        border-radius: 100%;
        display: block;
        text-align: center;
        margin: 12px auto 5px auto;
        background-color: #5a5a5a;
    }
    .progressbar-swz-wc li.active:before {
        background-color: #DF0B13;
    }
    .progressbar-swz-wc li:after {
        content: "";
        position: absolute;
        width: 98%;
        height: 1px;
        background-color: #000;
        top: 15px;
        left: -50%;
        z-index : 1;
    }
    .progressbar-swz-wc li:first-child:after {
        content: none;
    }
    .progressbar-swz-wc li.active {
        color: #5a5a5a;
    }
    .progressbar-swz-wc li.active:before {
        border-color: #5a5a5a;
    }
    .progressbar-swz-wc li.active + li:after {
        background-color: #000;
    }
    .progressBarContainer{
        width: 100%;
        padding-top: 2%;
        margin-left: 0.5%;
        float:left;
        margin-bottom:4%;
    }
    .progressbar-swz-wc{
        float:left;
        margin:0;
        width:100%;

    }
    .extra-spacing-block{
        display:none;
        color:#FFF;
        font-size:12px;
    }
</style>
<div class="woocommerce-order">

	<?php if ( $order ) :

		do_action( 'woocommerce_before_thankyou', $order->get_id() ); ?>

        <div class="progressBarContainer">
            <ul class="progressbar-swz-wc">
                <li class="step-1"><span class="extra-spacing-block">*</span><span>CUSTOMIZE</span></li>
                <li class="step-2"><span>MEDIUM & FRAME</span></li>
                <li class="step-3"><span class="extra-spacing-block">*</span><span>SHIPPING</span></li>
                <li class="step-4"><span class="extra-spacing-block">*</span><span>BILLING</span></li>
                <li class="step-5 active"><span class="extra-spacing-block">*</span><span>CONFIRMATION</span></li>
            </ul>
        </div>

		<?php if ( $order->has_status( 'failed' ) ) : ?>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php esc_html_e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
				<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php esc_html_e( 'Pay', 'woocommerce' ); ?></a>
				<?php if ( is_user_logged_in() ) : ?>
					<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php esc_html_e( 'My account', 'woocommerce' ); ?></a>
				<?php endif; ?>
			</p>

		<?php else : ?>

			<div class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you. Your order has been received.', 'woocommerce' ), $order ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></div>

			<!-- <ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">

				<li class="woocommerce-order-overview__order order">
					<?php esc_html_e( 'Order number:', 'woocommerce' ); ?>
					<strong><?php echo $order->get_order_number(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
				</li>

				<li class="woocommerce-order-overview__date date">
					<?php esc_html_e( 'Date:', 'woocommerce' ); ?>
					<strong><?php echo wc_format_datetime( $order->get_date_created() ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
				</li>

				<?php if ( is_user_logged_in() && $order->get_user_id() === get_current_user_id() && $order->get_billing_email() ) : ?>
					<li class="woocommerce-order-overview__email email">
						<?php esc_html_e( 'Email:', 'woocommerce' ); ?>
						<strong><?php echo $order->get_billing_email(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
					</li>
				<?php endif; ?>

				<li class="woocommerce-order-overview__total total">
					<?php esc_html_e( 'Total:', 'woocommerce' ); ?>
					<strong><?php echo $order->get_formatted_order_total(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
				</li>

				<?php if ( $order->get_payment_method_title() ) : ?>
					<li class="woocommerce-order-overview__payment-method method">
						<?php esc_html_e( 'Payment method:', 'woocommerce' ); ?>
						<strong><?php echo wp_kses_post( $order->get_payment_method_title() ); ?></strong>
					</li>
				<?php endif; ?>

			</ul>  -->

		<?php endif; ?>
		<div style="display:block;"> 
		<?php //do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() ); ?>
		<?php do_action( 'woocommerce_thankyou', $order->get_id() ); ?>
		</div>
	<?php else : ?>

		<div class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you. Your order has been received.', 'woocommerce' ), null ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></div>

	<?php endif; ?>

</div>
<?php //print_r($order_items); ?>


	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

<div class="changecurrency" style="display:none">
<?php echo do_shortcode('[aelia_currency_selector_widget widget_type="dropdown" ]'); ?>
</div>


<style type="text/css">
.topimage
{
	width:100%;
	float:left;
	margin-top:-8px !important;
}

.line1
{
	margin-top:20px;
	margin-bottom:2px;
	font-size:22px;
}

.line2
{
	margin-top:10px;
}
.sidepanel
{
	background-color:#f5f5f5;
	padding-top:18px;
	height:43pc;
}
.image
{
	float:left;
	width:50%;
	height:50px;
}
.text1
{
	margin-left:-24px !important;
	margin-bottom:0px;
}
.horizontal-line
{
	border:1px solid #d6d6d6;
	float:left;
	width:107%; 
	margin-left:-14px !important; 
	margin-top:43px;
}
.text2
{
	margin-top:20px;
	float:left;
	width:100%;
	font-size:19px;
	margin-left:20px;
}
.textbox
{
	margin-left: 25px;
    border-top: none;
    border-left: none;
    border-right: none;
    border-bottom: 2px solid #999999;
    width: 154px;
    background-color: #f5f5f5;
	height: 23px;
    padding: 10px;
    padding-left: 13px;
}
.text4
{
	text-align:center;
}
.text3
{
	float:right;
	margin-right:139px !important;
	border-bottom:1px solid red;
	margin-top:1px;
}
.text5
{
	font-size:14px;
}
.text6
{
	font-weight:bolder;
	font-size:16px;
	text-align:center;
}
.text7
{
	letter-spacing:1px;
	float:right;
	font-size:11px;
	font-weight:bold;
	margin-right:32px;
	border-bottom:1px solid red;
}
.text8
{
	font-size:16px; 
	font-weight:bolder;
	margin-bottom:0px;
}
.button2
{
	background-color: #acbddb;
    letter-spacing: 2px;
    float: left;
    /* margin-left: 9px; */
    font-weight: bolder;
    padding: 5px;
    border-radius: 27px;
    padding-left: 27px;
    width: 237px;
    padding-right: 27px;
    font-size: 11px;
    margin-top: 10px;
    text-align: center;
    margin-left: 73px;
}
@media screen and (max-width: 600px) and (min-width: 300px) 
{
	.button2
	{
		margin-left:32px !important;
	}
	.text1
	{
		margin-top:21px;
		margin-left:0px !important;
	}
	.text3
	{
		margin-right:53px !important;
	}
	.sidepanel
	{
		height:53pc;
		margin-top:24px;
	}
	.topimage
	{
		width:112%;
		height:20px;
		padding-top:10px;
	}
}
</style>