<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout.
if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
	echo esc_html( apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) ) );
	return;
}

?>
<?php if(isset($_GET['commercial']) && $_GET['commercial'] == 'ok'): ?>
	<div class="container-fluid">
		<div class="row">
			<div class="paragraph col-xs-12">
				<h3 class="heading3">Thank you.</h3>
				<p class="para">
					Email (Invoice) is on the way.
				</p>
				<br>
			</div>
		</div>
	</div>
<?php elseif(isset($_GET['own-shipping']) && $_GET['own-shipping'] == 'ok'): ?>
	<div class="container-fluid">
		<div class="row">
			<div class="paragraph col-xs-12">
				<h3 class="heading3">Thank you.</h3>
				<p class="para">
					Email (Invoice) is on the way.
				</p>
				<br>
			</div>
		</div>
	</div>
<?php else: ?>
	<input type="hidden" id="_user_logged_in" value="<?php echo (is_user_logged_in() ? 'true' : 'false'); ?>">
	<div class="container-fluid">
		<div class="progressBarContainer">
			<ul class="progressbar-swz-wc">
				<li class="step-1"><span class="extra-spacing-block">*</span><span>CUSTOMIZE</span></li>
				<li class="step-2"><span>MEDIUM & FRAME</span></li>
				<li class="step-3 active"><span class="extra-spacing-block">*</span><span>SHIPPING</span></li>
				<li class="step-4"><span class="extra-spacing-block">*</span><span>BILLING</span></li>
				<li class="step-5"><span class="extra-spacing-block">*</span><span>CONFIRMATION</span></li>
			</ul>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row" id="paragraph">
			<div class="paragraph col-xs-12">
				<h3 class="heading3">Shipping Terms & Conditions</h3>
				<p class="para">
					We aim to action all orders within 7 days of receiving an order.  They should reach addresses on Hong Kong Island and Kowloon within 2-3 weeks, the rest of the world within 4 weeks.  This is subject to seasonal variation and indicative only.  We cannot accept any liability whatsoever for delayed delivery caused by any third party.  We specify the method and address for delivery when you make your order.  Risk of damage or loss of the Goods passes to you on delivery.  Should you have any queries regarding your delivery please contact us.
				</p>
				<p class="para">
					We respects the privacy of our users. The <a href="/terms-and-conditions">Privacy Policy</a> explains how we collect, use, disclose, and safeguard your information when you visit and/or shopping on our website.
				</p>
				<br>
			</div>
			<div class="col-xs-12">
				<div class="woocommerce-NoticeGroup woocommerce-NoticeGroup-checkout p0">
					<ul class="woocommerce-error" role="alert">
						<li>
							Due to COVID-19 we are unable to ship to certain countries at this time.  If there are any issues regarding the shipping of your order, we will be in touch with you directly.  Local orders in Hong Kong are currently not affected
						</li>
					</ul>
				</div>
			</div>
			<div class="col-xs-12">
				<?php // stak_custom_order_review(); ?>
			</div>
			<?php
			$has_checked = 0;
			if(isset($_GET['currency']) && $_GET['currency'] !== '')
				$has_checked = 1;
			?>
			<div class="col-xs-12">
				<div class="form-group">
					<div class="checkbox">
						<label for="shipping-agreement">
							<input type="checkbox" name="shipping-agreement" id="shipping-agreement" class="box4" style="width: 18px;"
								<?php echo $has_checked == 1 ? ' checked="checked"' : '' ?>
							>
							I agree to the "Shipping Terms & Conditions"
						</label>			
					</div>
				</div>
				
				<p class="line">
					<button type="button" style="" class="button1 btn-payment" id="process_checkout">
						CONTINUE TO PAYMENT
					</button>
					
					<input type="hidden" name="has_checked" id="has_checked" value="<?php echo $has_checked; ?>">
				</p>
			</div>
		</div>

		<div class="row" id="billing-form" style="display: none;">
			<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">
				<div class="col-xs-12">
					<?php if ( $checkout->get_checkout_fields() ) : ?>

						<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

						<div class="col2-set" id="customer_details">
							
							<div class="col-1" id="customerdetail_col_1">
								
								<?php do_action( 'woocommerce_checkout_billing' ); ?>

							</div>

							<div class="col-2">
								<?php do_action( 'woocommerce_checkout_shipping' ); ?>
							</div>
						</div>

						<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

					<?php endif; ?>
				</div>
				<div class="col-xs-12 col-md-6">
					<?php print_frame_selection(); ?>
					<?php print_new_coupon(); ?>
					<div class="row">

						<?php print_new_shipping_review(); ?>
						
					</div>
				</div>
				<div class="col-xs-12 col-md-6">
					<div class="col-xs-12 px-xs-0">
						<?php _output_total_price_calc(); ?>
					</div>
					<div class="col-xs-12" style="margin: 10px 0 0px;">
						<hr class="horizontal-line">
					</div>
					<div class="col-xs-12 px-xs-0">
						<div>
							<h4>
								Commercial Buyer
							</h4>
							<input type="hidden" name="commercial_buyer" id="commercial_buyer" value="">
							<input type="hidden" name="_custom_shipping" id="_custom_shipping" value="0">
							<button
								type="button" 
								class="button2 btn-payment btn-block" 
								id="paymentinvoice"
								data-toggle="modal" data-target="#commModal">
								COMMERCIAL PAYMENT
							</button>
						</div>
					</div>
				</div>

				<?php do_action( 'woocommerce_checkout_before_order_review_heading' ); ?>
					<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>
				<div class="col-xs-12" style="margin-top: 25px;">
					
						<div class="woocommerce-checkout-review-order" style="padding-top: 15px;">
							<?php do_action( 'woocommerce_checkout_order_review' ); ?>
						</div>

				</div>
					<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
			</form>
		</div>

	</div>
	<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>

	<!-- modal for commercial payment -->
	<div class="modal fade" id="commModal" tabindex="-1" role="dialog" aria-labelledby="commModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">COMMERCIAL PAYMENT</h4>
				</div>
				<div class="modal-body">
					<p>
						If your order is over HK$50,000 (or USD equivalent) you can opt to pay in one of two ways: 
					</p>
					<ol>
						<li>Pay in full now - simply go back to the checkout and complete the payment process.</li>
						<li>Request invoicing over email for 10% upfront to secure the order and 90% to process the order, due 14 days from receipt of the invoice. Your order will remain unprocessed in your cart until deposit is made.</li>
					</ol>
					<p>
						Click this 'Yes' box to request invoicing for our commercial payment scheduled to be sent over email. 
					</p>
					<hr>
					<?php $_disabled = !_commercial_available() ? ' disabled' : ''; ?>
					<div class="row hide">
						<div class="col-xs-12">
							<div class="radio">
								<label>
									<input type="radio" name="commercial_confirm" id="commercial_confirm" value="1">
									Yes, please email me the invoice
								</label>
							</div>
						</div>
						<div class="col-xs-12">
							<div class="radio">
								<label>
									<input type="radio" name="commercial_confirm" id="commercial_confirm" value="0" checked>
									No, continue to checkout
								</label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-7">
							<button id="_commercial_ok" type="button" class="btn btn-block customized_homepage_button">
								Yes, please email me the invoice
							</button>
						</div>
						<div class="col-xs-12 col-md-5">
							<button type="button" class="btn btn-block btn-default" data-dismiss="modal">
								No, continue to checkout
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- modal for optout shipping -->
	<div class="modal fade" id="optOutShipping" tabindex="-1" role="dialog" aria-labelledby="optOutShippingLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">USE YOUR OWN SHIPPING</h4>
				</div>
				<div class="modal-body">
					<p>
						If you wish to use your own shipping service, click this 'Yes' box to request invoicing and payment over email. Your order will remain unprocessed in your cart until payment is made and shipping details received.
					</p>
					<hr>
					<div class="row">
						<div class="col-xs-12 col-md-7">
							<button id="_opt_out_shipping" type="button" class="btn btn-block customized_homepage_button">
								Yes, please email me the invoice
							</button>
						</div>
						<div class="col-xs-12 col-md-5">
							<button type="button" class="btn btn-block btn-default" data-dismiss="modal">
								No, continue to checkout
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- modal for login/register payment -->
	<?php include_once(get_stylesheet_directory().'/woocommerce/includes/modal-login-register.php'); ?>

	<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
	<script type="text/javascript">
		(function( $ ) {
			'use strict';

			function continue_payment() {
				var agreeToShipping = $('#shipping-agreement').prop('checked');
				if(!agreeToShipping){
					alert('You have to agree to our shipping terms and condition first.');
					return false;
				}
				$('.progressbar-swz-wc .active').removeClass('active');
				$('.progressbar-swz-wc .step-4').addClass('active');

				$('div#paragraph').slideUp(400);
				$('div#billing-form').delay(400).slideDown(600);
				 
				// $("#paymentinvoice").hide();
				// $("#showtodaypayment").show();

				// $('html, body').animate({
				// 	scrollTop: $(".progressBarContainer").offset().top-190
				// }, 1000);

			}

			function print_applied_coupon()
			{
				// console.log($('#xoo-wsc-coupon-code').length);
				// console.log($('#currency_code').val());
				var html_amount_str = $('tr.order-total td').html();
				
				html_amount_str = html_amount_str.replace(/(<([^>]+)>)/ig, "");
				html_amount_str = html_amount_str.replace($('#currency_code').val(), '');
				html_amount_str = $.trim(html_amount_str);
				// console.log(html_amount_str);

				var _amount = parseFloat(html_amount_str.replace(',', ''));
				console.log(_amount);
				var html_amount = '<strong><span class="woocs_special_price_code"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">'+$('#currency_code').val()+'</span> '+$.number(_amount, 2)+'</span></span></strong>';
				$('p#wc-sub-total').html(html_amount);
				// $('#total_amount').delay(6000).unblock();
				window.scrollTo(0, 0);
			}

			function fetch_phone_codes()
			{
				$.ajax({
					url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
					type: 'GET',
					data: {action: 'stak_fetch_phone_codes'},
					dataType: 'html',
					success: function(html) {
						console.log(html);
						$('#billing_wooccm11').html(html);
						var _country = $('#billing_country').val();
						console.log(_country);
						$('#billing_wooccm11 option[name="'+_country+'"]').prop('selected', true);
					},
					error: function(errorThrown){
						console.log(errorThrown);
					}
				});
			}

			$(function(){
				// dom ready
				// $('table.woocommerce-checkout-review-order-table').remove();
				$('p#wc-sub-total').html('<i class="fas fa-cog fa-spin"></i> CALCULATING THE PRICE...');
				print_applied_coupon();

				var checked = $('#has_checked').val();
				console.log(checked == 1 && $('#shipping-agreement').prop('checked'));
				if(checked == 1 && $('#shipping-agreement').prop('checked')){
					continue_payment();
				}

				if($('#_user_logged_in').val() == 'false'){
					$('#loginFormModal').modal({
						backdrop: 'static',
						keyboard: false,
						show: true
					});
				}

				fetch_phone_codes();

				$('body').on('change', '#billing_country', function(){
					var _country = $(this).val();
					console.log(_country);
					$('#billing_wooccm11 option[name="'+_country+'"]').prop('selected', true);
				});

				$('body').on('click', '#process_checkout', function(event){
					event.preventDefault();
					continue_payment();
				});

				$('body').on('change', '#shipping-agreement', function(){
					var currency = $('input#currency_code').val();
					var sub_total = $('input#sub_total_cart').val();

					if(currency == 'HK $' && sub_total >= 50000){
						console.log('over than 50K');
						// $('#commModal').modal('show');
					}

					if(currency == 'US $' && sub_total >= 5000){
						console.log('over than 50K');
						// $('#commModal').modal('show');
					}
				});

				$('body').on('click', '#_commercial_ok', function(e){
					var currency = $('input#currency_code').val();
					var sub_total = $('input#sub_total_cart').val();

					if($('input#billing_first_name').val() === ''){
						alert('Billing first name cannot empty!');
						$('#commModal').modal('hide');
						$('input#billing_first_name').focus();
						return false;
					}
					if($('input#billing_last_name').val() === ''){
						alert('Billing last name cannot empty!');
						$('#commModal').modal('hide');
						$('input#billing_last_name').focus();
						return false;
					}
					console.log($('#billing_country').val());
					if($('#billing_country').val() === ''){
						alert('Billing country cannot empty!');
						$('#commModal').modal('hide');
						$('#billing_country').focus();
						return false
					}
					if($('input#billing_address_1').val() === ''){
						alert('Billing address cannot empty!');
						$('#commModal').modal('hide');
						$('input#billing_address_1').focus();
						return false;
					}
					if($('input#billing_postcode').val() === ''){
						alert('Billing postal code/zip code cannot empty!');
						$('#commModal').modal('hide');
						$('input#billing_postcode').focus();
						return false;
					}
					if($('input#billing_email').val() === ''){
						alert('Billing email address cannot empty!');
						$('#commModal').modal('hide');
						$('input#billing_email').focus();
						return false;
					}

					if(currency == 'HK $' && sub_total < 50000){
						console.log('less than 50K');
						alert('Sorry! This request is available for orders over HK $50,000 or US $5,000.');
						return false;
					}

					if(currency == 'US $' && sub_total < 5000){
						console.log('less than 50K');
						alert('Sorry! This request is available for orders over HK $50,000 or US $5,000.');
						return false;
					}

					$(this).html('<i class="fas fa-cog fa-spin"></i> PROCESSING REQUEST...');
					
					$.ajax({
						url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
						type: 'GET',
						data: {action: 'ajax_commercial_confirm', user_id: '<?php echo get_current_user_id(); ?>'},
						success:function(json) {
							var obj = $.parseJSON(json);
							console.log(obj);
							if(obj.status === 'failed'){
								alert(obj.message);
								window.location.href = '/my-account';
							}

							if(obj.status === 'success'){
								$('input#commercial_buyer').val('yes');
								$('input#payment_method_stripe').prop('checked', false);
								$('input#payment_method_bacs').prop('checked', true);
								$('input#terms').prop('checked', true);
								$('li.wc_payment_method.payment_method_stripe').hide('fast');

								$('#commModal').modal('hide');
								$('button#place_order').trigger('click');
							}

							if(obj.status === 'error'){
								alert(obj.message);
							}
						},
						error: function(errorThrown){
							console.log(errorThrown);
						}
					});
				});

				$('body').on('click', '#_opt_out_shipping', function(e){

					$(this).html('<i class="fas fa-cog fa-spin"></i> PROCESSING REQUEST...');
					
					$.ajax({
						url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
						type: 'GET',
						data: {action: 'ajax_own_shipping_confirm', user_id: '<?php echo get_current_user_id(); ?>'},
						success:function(json) {
							$('#_custom_shipping').val('1');
							var obj = $.parseJSON(json);
							console.log(obj);
							if(obj.status === 'failed'){
								alert(obj.message);
								window.location.href = '/my-account';
							}

							if(obj.status === 'success'){
								$('#optOutShipping').modal('hide');
								let dest = $('#billing_country').val();
								/*if(dest === 'HK'){
									$('#billing_country').val('ID');
									$('body').trigger( 'update_checkout' );
								}else{
									$('#billing_country').val('HK');
									$('body').trigger( 'update_checkout' );
								}*/
								$('#billing_country').val(dest);
								$('#billing_country').trigger( 'change' );
								$('input#payment_method_stripe').prop('checked', false);
								$('input#payment_method_bacs').prop('checked', true);
								$('input#terms').prop('checked', true);
								$('li.wc_payment_method.payment_method_stripe').hide('fast');
								console.log($('#_custom_shipping').val());
								console.log($('#_custom_shipping').val());
								// setTimeout(function(){
								// 	$('button#place_order').trigger('click');
								// }, 5000);
								
								$(document).ajaxSuccess(function(event, xhr, settings){
									// console.log(event);
									// console.log(xhr.responseText);
									// return false;
									xhr.done(function(){
										setTimeout(function(){
											$('button#place_order').trigger('click');
										}, 5000);
									});
								});
							}

							if(obj.status === 'error'){
								alert(obj.message);
							}
						},
						error: function(errorThrown){
							console.log(errorThrown);
						}
					});
				});

				$('body').on('change', '#commercial_confirm', function(){
					var checked = $('#commercial_confirm:checked').val();
					console.log(checked);

					if(checked == 1){
						
						var currency = $('input#currency_code').val();
						var sub_total = $('input#sub_total_cart').val();

						if(currency == 'HK $' && sub_total < 50000){
							console.log('less than 50K');
							alert('Sorry! This request is available for orders over HK $50,000 or US $5,000.');
							return false;
						}

						if(currency == 'US $' && sub_total < 5000){
							console.log('less than 50K');
							alert('Sorry! This request is available for orders over HK $50,000 or US $5,000.');
							return false;
						}

						$('input#commercial_buyer').val('yes');

						$.ajax({
							url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
							type: 'GET',
							data: {action: 'ajax_commercial_confirm', user_id: '<?php echo get_current_user_id(); ?>'},
							success:function(json) {
								var obj = $.parseJSON(json);
								console.log(obj);
								if(obj.status === 'failed'){
									alert(obj.message);
									window.location.href = '/my-account';
								}

								if(obj.status === 'success'){
									window.location.href = '/checkout?commercial=ok';
								}

								if(obj.status === 'error'){
									alert(obj.message);
									return false;
								}
							},
							error: function(errorThrown){
								console.log(errorThrown);
							}
						});
					}

					if(checked == 0){
						$('input#commercial_buyer').val('no');
					}
				});

				$('body').on('click', '.woocommerce-remove-coupon', function(event){
					$(document).ajaxStart(function(){
						console.log('recalculating...');
						$('p#wc-sub-total').html('<i class="fas fa-cog fa-spin"></i> CALCULATING THE PRICE...');
					});
					$(document).ajaxSuccess(function(){
						print_applied_coupon();
						// if($('#xoo-wsc-coupon-code').length){
						// 	location.reload();
						// }
					});
				});

				$('body').on('click', 'a#apply_coupon', function(event){
					event.preventDefault();
					$('p#wc-sub-total').html('<i class="fas fa-cog fa-spin"></i> CALCULATING THE PRICE...');
					var coupon = $('#stak_coupon_code').val();
					if(coupon === ''){
						alert('please enter a valid coupon code!');
						$('#stak_coupon_code').focus();
						return false;
					}

					$('input#coupon_code').val(coupon);
					$('button[name="apply_coupon"]').trigger('click');

					$(document).ajaxStart(function(){
						console.log('recalculating...');
						$('p#wc-sub-total').html('<i class="fas fa-cog fa-spin"></i> CALCULATING THE PRICE...');
					});
					$(document).ajaxSuccess(function(){
						print_applied_coupon();
						// if($('#xoo-wsc-coupon-code').length){
						// 	location.reload();
						// }
					});
				});

				$('tr.order-total td').on('change', function(){
					console.log($(this).html);
				});

				$('body').on('click', '.frame-selector-wrapper', function(){
					$('label.frame-selector').removeClass('active');
					var label = $(this).children('label.frame-selector');
					var selector = label.attr('data-frame');

					$('input[type="radio"]#'+selector).trigger('click');
					label.addClass('active');
				});
				// $('body').on('click', '#toggler-check', function(){
				// 	$(this).children('input[type="checkbox"]').trigger('click');
				// });
				$('body').on('change', '#frame-toggler', function(){
					var checked = $(this).prop('checked');
					// console.log(checked);

					if(checked){
						$('#custom-frame-wrapper').show('slow');
					}else{
						$('#custom-frame-wrapper').hide('fast');
					}
					
				});

				$('#shippingModal').on('shown.bs.modal', function (e) {
					$.ajax({
						url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
						type: 'GET',
						data: {action: 'ajax_stak_get_cart', user_id: '<?php echo get_current_user_id(); ?>'},
						success:function(data) {
							// This outputs the result of the ajax request
							console.log(data);
							$('#shippingModal #cart-info').val(data);
						},
						error: function(errorThrown){
							console.log(errorThrown);
						}
					});
				});
				$('#inquiryModal').on('hidden.bs.modal', function (e) {
					$('#frame-toggler').prop('checked', false);
				});
				// $('body').on('click', '.btn-payment', function(event){
				// 	event.preventDefault();
				// 	continue_payment();
				// });
				$('body').on('click', '#curr-switch', function(event){
					event.preventDefault();
					var to_curr = $(this).attr('data-currency');
					$('li[data-currency="'+to_curr+'"]').trigger('click');
				});
			});
		})( jQuery );
	</script>
<?php endif; ?>