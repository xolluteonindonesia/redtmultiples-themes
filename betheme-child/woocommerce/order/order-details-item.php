<?php
/**
 * Order Item Details
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-details-item.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {
	return;
}

?>
<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_order_item_class', 'woocommerce-table__line-item order_item', $item, $order ) ); ?>">

	<td class="woocommerce-table__product-name product-name">
		<div class="product-img">
			<?php $img_src = apply_filters( 'woocommerce_order_item_permalink', $is_visible ? $product->get_permalink( $item ) : '', $item, $order ); ?>
			<img src="<?php echo $img_src; ?>">
		</div>
		<div class="product">
			<?php
			
			$is_visible        = $product && $product->is_visible();
			$product_permalink = apply_filters( 'woocommerce_order_item_permalink', $is_visible ? $product->get_permalink( $item ) : '', $item, $order );

			echo apply_filters( 'woocommerce_order_item_name', $product_permalink ? sprintf( '<a data-fancybox="quick-view-%s" href="%s" data-caption="%s" data-type="image">%s</a>', $index, $product_permalink, $item->get_name(), $item->get_name() ) : $item->get_name(), $item, $is_visible ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

			$qty          = $item->get_quantity();
			$refunded_qty = $order->get_qty_refunded_for_item( $item_id );

			if ( $refunded_qty ) {
				$qty_display = '<del>' . esc_html( $qty ) . '</del> <ins>' . esc_html( $qty - ( $refunded_qty * -1 ) ) . '</ins>';
			} else {
				$qty_display = esc_html( $qty );
			}

			echo apply_filters( 'woocommerce_order_item_quantity_html', ' <strong class="product-quantity">' . sprintf( '&times; %s', $qty_display ) . '</strong>', $item ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

			do_action( 'woocommerce_order_item_meta_start', $item_id, $item, $order, false );

			stak_wc_display_item_meta( $item ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

			do_action( 'woocommerce_order_item_meta_end', $item_id, $item, $order, false );
			$custom = check_if_custom( $item );
			// var_dump($custom);
			?>
			<div class="product-images"></div>
			<div class="product-form">
				<?php $title_arr = explode(' - ', $item->get_name()); ?>
				<div class="quick-view-mobile-img">
					<img src="<?php echo $product_permalink; ?>">
				</div>
				<p><?php echo $title_arr[1]; ?></p>
				<h4> <?php echo $title_arr[0]; ?></h4>
				<p><em><?php echo $custom ? 'CUSTOM' : 'DEFAULT'; ?></em></p>
				<?php
				if($custom){
					stak_wc_display_item_meta( $item );
					$custom_meta = collect_custom_meta_from_order( $item );
					// var_dump($custom_meta);
					?>
					<form id="form-<?php echo $item_id; ?>" class="stak-add-to-cart" style="margin-top: 25px;">
						<input type="hidden" name="action" value="stak_custom_art_add_to_cart">
						<input type="hidden" name="art_id" value="<?php echo $item_id; ?>">
						<?php 
						foreach ($custom_meta as $key => $val) {
							if($key != 'action'){
								?>
								<input type="hidden" name="<?php echo $key; ?>" value="<?php echo $val; ?>">
								<?php
							}
						} 
						?>
						<button type="submit" id="add-to-cart" class="btn-stak black">PURCHASE ANOTHER</button>
					</form>
					<?php
				}else{
					$product_id = $item->get_product_id();
					$var_id = $item->get_variation_id();
					$variation = new WC_Product_Variation( $var_id );
					$var_data = $variation->get_data();
					$attributes = $variation->get_attributes();
					// var_dump($attributes);
					?>
					<ul class="wc-item-meta">
						<li>
							<strong class="wc-item-meta-label">Width</strong>: <?php echo number_format(floatval($var_data['width'])); ?> cm
						</li>
						<li>
							<strong class="wc-item-meta-label">Height</strong>: <?php echo number_format(floatval($var_data['height'])); ?> cm
						</li>
					</ul>
					<form id="form-<?php echo $item_id; ?>" class="stak-add-to-cart" style="margin-top: 25px;">
						<input type="hidden" name="action" value="stak_variable_add_to_cart">
						<input type="hidden" name="product_id" value="<?php echo $product_id; ?>">
						<input type="hidden" name="variation_id" value="<?php echo $var_id; ?>">
						<input type="hidden" name="art_id" value="<?php echo $item_id; ?>">
						<?php
							foreach( $attributes as $key => $value ) {
								$var_title = str_replace('pa_', '', $key);
								$var_title = ucwords(str_replace('-', ' ', $var_title));
								?>
								<input type="hidden" name="variation[<?php echo $var_title; ?>]" value="<?php echo $value; ?>">
								<?php
							}
						?>
						
						<button type="submit" id="add-to-cart" class="btn-stak black">PURCHASE ANOTHER</button>
					</form>
					<?php
				}
				 
				?>
			</div>
		</div>
	</td>

	<td class="woocommerce-table__product-total product-total">
		<?php echo $order->get_formatted_line_subtotal( $item ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
	</td>

</tr>

<?php if ( $show_purchase_note && $purchase_note ) : ?>

<tr class="woocommerce-table__product-purchase-note product-purchase-note">

	<td colspan="2"><?php echo wpautop( do_shortcode( wp_kses_post( $purchase_note ) ) ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></td>

</tr>

<?php endif; ?>
