<?php
/**
 * Order details
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-details.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;

$order = wc_get_order( $order_id ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.OverrideProhibited

if ( ! $order ) {
	return;
}

$order_items           = $order->get_items( apply_filters( 'woocommerce_purchase_order_item_types', 'line_item' ) );
$show_purchase_note    = $order->has_status( apply_filters( 'woocommerce_purchase_note_order_statuses', array( 'completed', 'processing' ) ) );
$show_customer_details = is_user_logged_in() && $order->get_user_id() === get_current_user_id();
$downloads             = $order->get_downloadable_items();
$show_downloads        = $order->has_downloadable_item() && $order->is_download_permitted();

if ( $show_downloads ) {
	wc_get_template(
		'order/order-downloads.php',
		array(
			'downloads'  => $downloads,
			'show_title' => true,
		)
	);
}
?>
<style type="text/css">
	/*
	Step 1
	======
	Style your page (the product list)
	*/
	.product {
	    float: left;
	    margin: 10px;
	}

	.product .product-images,
	.product .product-form {
	    display: none;
	}

	/*
	Step 2
	======
	Reposition and redesign fancyBox blocks
	*/
	/* This elements contains both blocks */
	.fancybox-inner {
	    position: absolute;
	    top: 0;
	    right: 0;
	    bottom: 0;
	    left: 0;
	    margin: auto;
	    width: calc(100% - 40px);
	    height: calc(100% - 40px);
	    max-width: 800px;
	    max-height: 600px;
	}

	/* Left block will contain the gallery */
	.fancybox-stage {
	    width: 52%;
	    background: #fff;
	}

	/* Right block - close button and the form */
	.fancybox-form-wrap {
	    position: absolute;
	    top: 40px;
	    right: 0;
	    bottom: 40px;
	    width: 48%;
	    background: #fff;
	}

	/* Add vertical lines */
	.fancybox-form-wrap::before,
	.fancybox-form-wrap::after {
	    content: '';
	    position: absolute;
	    top: 0;
	    left: 0;
	    bottom: 0;
	}

	.fancybox-form-wrap::before {
	    width: 8px;
	    background: #f4f4f4;
	}

	.fancybox-form-wrap::after {
	    width: 1px;
	    background: #e9e9e9;
	}

	/* Set position and colors for close button */
	.fancybox-button--close {
		border-width: 0;
	    position: absolute;
	    top: 0;
	    right: 0;
	    background: #F0F0F0;
	    color: #222;
	    padding: 7px;
	}

	.fancybox-button:hover {
	    color: #111;
	    background: #e4e4e4;
	}

	.fancybox-button svg path {
	    stroke-width: 1;
	}

	/* Set position of the form */
	.fancybox-inner .product-form {
	    overflow: auto;
	    position: absolute;
	    top: 50px;
	    right: 0;
	    bottom: 50px;
	    left: 0;
	    padding: 0 50px;
	    text-align: center;
	}

	/*
	Step 3
	======
	Tweak fade animation
	*/
	.fancybox-inner {
	    opacity: 0;
	    transition: opacity .3s;
	}

	.fancybox-is-open .fancybox-inner {
	    opacity: 1;
	}

	.fancybox-is-closing .fancybox-fx-fade {
	    opacity: 1 !important;
	    /* Prevent double-fading */
	}

	/*
	Step 2
	======
	Bullet navigation design
	*/
	.product-bullets {
	    display: none;
	    list-style: none;
	    position: absolute;
	    bottom: 0;
	    left: 0;
	    width: 100%;
	    text-align: center;
	    margin: 0;
	    padding: 0;
	    z-index: 99999;
	    -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
	}

	.product-bullets li {
	    display: inline-block;
	    vertical-align: top;
	}

	.product-bullets li a {
	    display: block;
	    height: 30px;
	    width: 20px;
	    position: relative;
	}

	.product-bullets li a span {
	    position: absolute;
	    top: 50%;
	    left: 50%;
	    transform: translate(-50%, -50%);
	    width: 10px;
	    height: 10px;
	    border-radius: 99px;
	    text-indent: -99999px;
	    overflow: hidden;
	    background: #fff;
	    box-shadow: 0px 0px 2px rgba(0, 0, 0, 0.5);
	}

	.product-bullets li.active a span {
	    background: #FF6666;
	}
</style>
<section class="woocommerce-order-details">
	<?php do_action( 'woocommerce_order_details_before_order_table', $order ); ?>

	<h2 class="woocommerce-order-details__title"><?php esc_html_e( 'Order details', 'woocommerce' ); ?></h2>

	<table class="woocommerce-table woocommerce-table--order-details shop_table order_details">

		<thead>
			<tr>
				<th class="woocommerce-table__product-name product-name"><?php esc_html_e( 'Product', 'woocommerce' ); ?></th>
				<th class="woocommerce-table__product-table product-total"><?php esc_html_e( 'Total', 'woocommerce' ); ?></th>
			</tr>
		</thead>
<div class="product">
		<tbody>
			<?php
			do_action( 'woocommerce_order_details_before_order_table_items', $order );
			$x = 0;

			foreach ( $order_items as $item_id => $item ) {
				// var_dump($item);
				$x++;
				$product = $item->get_product();

				wc_get_template(
					'order/order-details-item.php',
					array(
						'order'              => $order,
						'item_id'            => $item_id,
						'item'               => $item,
						'show_purchase_note' => $show_purchase_note,
						'purchase_note'      => $product ? $product->get_purchase_note() : '',
						'product'            => $product,
						'index'              => $x
					)
				);
			}
			if(null !== $order->get_meta('_custom_frame') && $order->get_meta('_custom_frame') !== ''){
				?>
				<td class="woocommerce-table__product-name product-name">
					<div class="product" style="margin-bottom: 15px;">
						<span>
							<strong>Frame:</strong><br>
							<?php echo $order->get_meta('_custom_frame'); ?>
						</span>
					</div>
				</td>
				<td class="woocommerce-table__product-total product-total">
					<span class="woocommerce-Price-amount amount">Will be Quoted</span>
				</td>
				<?php
			}

			do_action( 'woocommerce_order_details_after_order_table_items', $order );
			?>
		</tbody>

		<tfoot>
			<?php
			foreach ( $order->get_order_item_totals() as $key => $total ) {
				?>
					<tr>
						<th scope="row"><?php echo esc_html( $total['label'] ); ?></th>
						<td><?php echo ( 'payment_method' === $key ) ? esc_html( $total['value'] ) : wp_kses_post( $total['value'] ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></td>
					</tr>
					<?php
			}
			?>
			<?php if ( $order->get_customer_note() ) : ?>
				<tr>
					<th><?php esc_html_e( 'Note:', 'woocommerce' ); ?></th>
					<td><?php echo wp_kses_post( nl2br( wptexturize( $order->get_customer_note() ) ) ); ?></td>
				</tr>
			<?php endif; ?>
		</tfoot>
	</table>

	<?php do_action( 'woocommerce_order_details_after_order_table', $order ); ?>
</section>
<script type="text/javascript">
	(function( $ ) {
		'use strict';

		$(function(){
			$('body').on('submit', 'form.stak-add-to-cart', function(e){
				e.preventDefault();
				var $button = $(this).children('button#add-to-cart');
				$button.attr('disabled', 'disabled');
				$button.addClass('disabled');
				$button.html('<i class="fas fa-cog fa-spin"></i> ADDDING...');
				var formdata = $(this).serialize();
				console.log(formdata);
				$.ajax({
					url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
					type: 'POST',
					data: formdata,
				})
				.done(function(json) {
					var obj = $.parseJSON(json);
					// console.log(obj); return false;
					if(obj.status === 'error'){
						alert(obj.message);
					}

					if(obj.status === 'ok'){
						alert('added to cart');
						location.reload();
					}
				})
				.fail(function(error) {
					console.log(error);
				})
				.always(function() {
					console.log("complete");
				});
			});
			$('[data-fancybox^="quick-view"]').fancybox({
			    animationEffect: "fade",
			    animationDuration: 700,
			    margin: 0,
			    gutter: 0,
			    touch: {
			        vertical: false
			    },
			    baseTpl: '<div class="fancybox-container" role="dialog" tabindex="-1">' +
			        '<div class="fancybox-bg"></div>' +
			        '<div class="fancybox-inner">' +
			        '<div class="fancybox-stage quick-view"></div>' +
			        '<div class="fancybox-form-wrap">' +
			        '<button data-fancybox-close class="fancybox-button fancybox-button--close" title="{{CLOSE}}">' +
			        '<svg viewBox="0 0 40 40">' +
			        '<path d="M10,10 L30,30 M30,10 L10,30" />' +
			        '</svg>' +
			        '</button></div>' +
			        '</div>' +
			        '</div>',
			    onInit: function(instance) {

			        /*

			            #1 Add product form
			            ===================

			        */

			        // Find current form element ..
			        var current = instance.group[instance.currIndex];
			        instance.$refs.form = current.opts.$orig.parent().find('.product-form');

			        // .. and move to the container
			        instance.$refs.form.appendTo(instance.$refs.container.find('.fancybox-form-wrap'));

			        /*

			            #2 Create bullet navigation links
			            =================================

			        */
			        var list = '',
			            $bullets;

			        for (var i = 0; i < instance.group.length; i++) {
			            list += '<li><a data-index="' + i + '" href="javascript:;"><span>' + (i + 1) + '</span></a></li>';
			        }

			        $bullets = $('<ul class="product-bullets">' + list + '</ul>').on('click touchstart', 'a', function() {
			            var index = $(this).data('index');

			            $.fancybox.getInstance(function() {
			                this.jumpTo(index);
			            });

			        });

			        instance.$refs.bullets = $bullets.appendTo(instance.$refs.stage);

			    },
			    beforeShow: function(instance) {

			        // Mark current bullet navigation link as active
			        instance.$refs.stage.find('ul:first')
			            .children()
			            .removeClass('active')
			            .eq(instance.currIndex)
			            .addClass('active');

			    },
			    afterClose: function(instance, current) {

			        // Move form back to the place
			        instance.$refs.form.appendTo(current.opts.$orig.parent());

			    }
			});
		});
	})( jQuery );
	
</script>
<?php
if ( $show_customer_details ) {
	wc_get_template( 'order/order-details-customer.php', array( 'order' => $order ) );
}
