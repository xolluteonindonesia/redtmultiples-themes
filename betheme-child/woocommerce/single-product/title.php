<?php
/**
 * Single Product title
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/title.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author     WooThemes
 * @package    WooCommerce/Templates
 * @version    1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if( mfn_opts_get('shop-product-title') != 'sub' ){

	// var_dump(get_the_title());
	$title_str = get_the_title();
	// var_dump($title_str);
	$title_arr = explode('&#8211;', $title_str);
	// var_dump($title_arr);
	?>
	<style type="text/css">
		.title-wrapper h3 {
			font-size: 16px;
			margin-top: 0;
		}
	</style>
	<div class="title-wrapper">
		<h3><?php echo (isset($title_arr[1]) ? trim($title_arr[1]) : ''); ?></h3>
		<h1 itemprop="name" class="product_title entry-title"><?php echo isset($title_arr[0]) ? trim($title_arr[0]) : ''; ?></h1>
	</div>
	<?php

	// the_title( '<h1 itemprop="name" class="product_title entry-title">', '</h1>' );

}