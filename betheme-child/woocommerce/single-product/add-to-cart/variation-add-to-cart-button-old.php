<?php
/**
 * Single variation cart button
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;
?>
<style>
.line9 {
    font-size: 19px;
    float: left;
    margin-left: 0;
    font-weight: bold;
    margin-top: 7px;
}
.line9-1 {
    font-size: 26px;
    color: #ef1414;
}
div.quantity,.yith-wcwl-add-to-wishlist,.product_meta{
	display:none!important;
}
#addNewCartVal{
	-webkit-appearance: caret;
	border-radius: 8px;
    height: 38px;
    width: 15%;
    padding-left: 10px;
    float: left;
    margin-top: 10px;
}
button.button1,.button1 {
	display: inline-block;
    text-transform: uppercase;
    border: 1px solid #7b7b7b;
    text-align: center;
    height: 44px;
    border-radius: 8px;
    padding-top: 11px;
    font-size: 16px;
    font-weight: bold;
    color: #7b7b7b;
    margin-top: 13px;
    width: 47%;
	text-decoration:none!important;
}
a.button1:hover {
    background: #000;
    color: #fff;
}
button.button1{
	color: #7b7b7b !important;
	margin-left: 0;
	margin-top: 0;
}
.ui-tabs .ui-tabs-nav li.ui-state-active a:after{
	background:#000;
}
.ui-tabs .ui-tabs-nav li.ui-state-active a,.ui-tabs .ui-tabs-nav li a{
	color:#7b7b7b;
	text-transform: uppercase;
}
.woocommerce .product div.entry-summary .ui-tabs .ui-tabs-nav li a {
    padding-bottom: 6px!important;
}
#opt_product_frame_outer {
	margin: 15px 0 0;
	padding-right: 0px;
	padding-left: 0px;
}
#opt_product_frame_outer label.frame-selector {
	background-color: #999;
	margin-right: 15px;
	width: 25px;
	height: 25px;
}
#opt_product_frame_outer label.frame-selector:last-child {
	margin-right: 0;
}
#opt_product_frame_outer label.frame-selector.active {
	border: solid 3px #e6363d;
}
.cursor-pointer {
	cursor: pointer;
}
#opt_product_frame_outer label.frame-1 {
	background: url("<?php echo get_stylesheet_directory_uri(); ?>/img/frame1.jpg");
}
#opt_product_frame_outer label.frame-2 {
	background: url("<?php echo get_stylesheet_directory_uri(); ?>/img/frame2.jpg");
}
.woocommerce-variation.single_variation {
	display: none!important;
}
.new_woocommerce_price_wrapper {
	border-top: solid 1px #333;
	margin-top: 15px;
	padding-left: 0;
	padding-right: 0;
	padding-top: 15px;
}
.new-variations-block {
	padding-left: 0;
	padding-right: 0;
	margin-top: 12px;
}
.pl-0 {
	padding-left: 0;
}
#purchase {
	padding-right: 0;
}
#purchase p {
	margin: 0;
}
.woocommerce .product div.entry-summary .ui-tabs .ui-tabs-nav li a {
    padding: 14px 17px!important;
}
p.price span.small {
	font-size: 16px;
	margin-right: 15px;
	position: relative;
	top: -2px;
}

</style>
<?php $availableStock = $product->get_stock_quantity(); ?>
<div class="woocommerce-variation-add-to-cart variations_button">
	<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>
	<?php if($availableStock): ?>
	<p class="line9">
		<span class="line9-1"><?php echo $availableStock; ?></span> of 100 editions available		
	</p>
	<?php endif; ?>

	<?php
	do_action( 'woocommerce_before_add_to_cart_quantity' );

	woocommerce_quantity_input( array(
		'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
		'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
		'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
	) );

	do_action( 'woocommerce_after_add_to_cart_quantity' );
	?>
	<div class="new-variations-block col-xs-12"></div>
	<div class="col-xs-12 pl-0">
		<select id="addNewCartVal">
			<option value="0">Qty</option>
			<?php $cartNewVals = range(1,10); ?>
			<?php foreach($cartNewVals as $cartVal): ?>
			<option value="<?php echo $cartVal; ?>"><?php echo $cartVal; ?></option>
			<?php endforeach; ?>
		</select>
	</div>
	<div id="opt_product_frame_outer" class="col-xs-12">
		<p>CHOSE FRAME (optional)</p>
		<input type="hidden" id="input_frame_price" name="input_frame_price" value="0" />
		<input type="hidden" id="frame_price" value="0" />
		<input type="hidden" id="selected_frame" name="selected_frame" value="no"/>
		<div class="frame-label-wrapper">			
			<label class="frame-selector cursor-pointer frame-1" data-frame="frame type 1"></label>	
			<label class="frame-selector cursor-pointer frame-2" data-frame="frame type 2"></label>	
		</div>		
	</div>
	<div id="new_price" class="new_woocommerce_price_wrapper col-xs-12 text-center"></div>
	<div id="purchase" class="col-lg-12 text-center" style="padding-left:0;">
		<p><button type="submit" class="button1">Buy it now<?php //echo esc_html( $product->single_add_to_cart_text() ); ?></button></p>
		<p style="margin-top: 12px;">Or</p>
		<p><a class="button1" href="<?php echo site_url().'/customize-product/?p_id='.$product->get_id(); ?>">CUSTOMIZE</a><p>

		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
	</div>

	<input type="hidden" name="add-to-cart" value="<?php echo absint( $product->get_id() ); ?>" />
	<input type="hidden" name="product_id" value="<?php echo absint( $product->get_id() ); ?>" />
	<input type="hidden" name="variation_id" class="variation_id" value="0" />
</div>
<script>
	(function( $ ) {
		'use strict';

		var frame_prices = ['ne',5480,4270,3020,1880,1330];

		function addCommas(str) {
		    str += '';
		    var x = str.split('.');
		    var x1 = x[0];
		    var x2 = x.length > 1 ? '.' + x[1] : '';
		    var rgx = /(\d+)(\d{3})/;
		    while (rgx.test(x1)) {
		        x1 = x1.replace(rgx, '$1' + ',' + '$2');
		    }
		    return x1 + x2;
		}

		// dom ready
		$(function(){
			$('.new-variations-block').html($('.variations-block').html());
			$('.variations-block').html('')
			$('body').on('click', 'label.frame-selector', function(){
				$('label.frame-selector').removeClass('active');
				$(this).addClass('active');
				var selected_frame = $(this).attr('data-frame');
				$('#selected_frame').val(selected_frame);

				var woo_price_html = $('.woocommerce-variation.single_variation').html();
				$('#new_price').html(woo_price_html);
				var selectedSize = $('input.setCropSize:checked');
				console.log(selectedSize.val());
				if(typeof selectedSize.val() === 'undefined'){
					alert('You have to select the size first to select the frame!');
					return false;
				}

				var current_price_html = $('.woocommerce-Price-amount').html();
				var $jQueryObject = $("<div/>").html(current_price_html);
				$jQueryObject.find('span').remove();
				var current_price = $jQueryObject.html();
				current_price = parseFloat(current_price);
				console.log(current_price);

				//now we calculate the dimension.
				var pic_width = selectedSize.attr('data-width'), pic_height = selectedSize.attr('data-height');
				var pic_size = parseInt(pic_width*pic_height);
				var n;
				console.log(pic_size);
				if(pic_size > 21600)
					n = 0;
				if(pic_size <= 21600 && pic_size > 15900)
					n = 1;
				if(pic_size <= 15900 && pic_size > 9980)
					n = 2;
				if(pic_size <= 9980 && pic_size > 4990)
					n = 3;
				if(pic_size <= 4990 && pic_size > 2495)
					n = 4;
				if(pic_size <= 2495)
					n = 5;

				var frame_price = frame_prices[n];
				console.log(frame_price);

				var new_price = current_price + frame_price;
				$('#frame_price').val(frame_price);
				$('#input_frame_price').val(frame_price.toFixed(2));
				var new_price_str = addCommas(new_price.toFixed(2));
				var new_html = '<span class="woocommerce-Price-currencySymbol">$</span>'+new_price_str;
				$('#new_price span.woocommerce-Price-amount.amount').html(new_html);
			});
		});
	})( jQuery );

	jQuery(document).ready(function($){
		$("#addNewCartVal").click(function(){
			var qtyVal = $(this).val();
			$( "input[name='quantity']" ).val(qtyVal);
		});
	});
</script>
