<?php
/**
 * Variable product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/variable.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.5
 */

defined( 'ABSPATH' ) || exit;

global $product;

$attribute_keys  = array_keys( $attributes );
$variations_json = wp_json_encode( $available_variations );
$variations_attr = function_exists( 'wc_esc_json' ) ? wc_esc_json( $variations_json ) : _wp_specialchars( $variations_json, ENT_QUOTES, 'UTF-8', true );

do_action( 'woocommerce_before_add_to_cart_form' ); ?>
<!-- <style>

</style> | moved to style.css for better usage.-->
<form class="variations_form cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data' data-product_id="<?php echo absint( $product->get_id() ); ?>" data-product_variations="<?php echo $variations_attr; // WPCS: XSS ok. ?>">
	<?php do_action( 'woocommerce_before_variations_form' ); ?>

	<?php if ( empty( $available_variations ) && false !== $available_variations ) : ?>
		<p class="stock out-of-stock"><?php echo esc_html( apply_filters( 'woocommerce_out_of_stock_message', __( 'This product is currently out of stock and unavailable.', 'woocommerce' ) ) ); ?></p>
	<?php else : ?>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 variations-block" style="padding:0;">
			<p>CHOOSE SIZE</p>
			<?php
			$sizes = [];
			foreach($available_variations as $var){
				// var_dump($var);
				$key = $var['attributes']['attribute_pa_select-size'];
				// var_dump($key);
				$sizes[$key]['name'] = html_entity_decode($var['dimensions_html']);
				$sizes[$key]['width'] = $var['dimensions']['width'];
				$sizes[$key]['height'] = $var['dimensions']['height'];
				$sizes[$key]['var_id'] = $var['variation_id'];
			}
			// var_dump($sizes);
			$sizes = array_reverse($sizes);
			$counter = 1;
			foreach($sizes as $key => $el){
				if($el['name'] !== 'N/A'):
				?>
				<div class="label-holder" style="padding:0;">
					<input type="radio" class="setCropSize" style="display:none;" name="temp_attribute_pa_select-size" id="field_<?php echo $key; ?>" value="<?php echo $key; ?>" data-variation="<?php echo $el['var_id']; ?>" data-width="<?php echo $el['width']; ?>" data-height="<?php echo $el['height']; ?>"/>
					<label class="selectCrop" for="field_<?php echo $key; ?>" var-id="<?php echo $el['var_id']; ?>">
						<span class="line8"><?php echo $el['name']; ?></span>
					</label>
				</div>
				<?php
				endif;
				$counter++;
			}
			?>
		</div>
		<table class="variations" cellspacing="0" style="display:none;">
			<tbody>
				<?php foreach ( $attributes as $attribute_name => $options ) : ?>
				<?php //if($attribute_name == 'pa_select-size'){continue;} ?>
					<tr <?php echo ($attribute_name == 'pa_select-size') ? 'style="display:none;"' : ''; ?>>
						<td class="label"><label for="<?php echo esc_attr( sanitize_title( $attribute_name ) ); ?>"><?php echo wc_attribute_label( $attribute_name ); // WPCS: XSS ok. ?></label></td>
						<td class="value">
							<?php
								wc_dropdown_variation_attribute_options( array(
									'options'   => $options,
									'attribute' => $attribute_name,
									'product'   => $product,
								) );
								echo end( $attribute_keys ) === $attribute_name ? wp_kses_post( apply_filters( 'woocommerce_reset_variations_link', '<a class="reset_variations" href="#">' . esc_html__( 'Clear', 'woocommerce' ) . '</a>' ) ) : '';
							?>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

		<div class="single_variation_wrap">
			<?php
				/**
				 * Hook: woocommerce_before_single_variation.
				 */
				do_action( 'woocommerce_before_single_variation' );

				/**
				 * Hook: woocommerce_single_variation. Used to output the cart button and placeholder for variation data.
				 *
				 * @since 2.4.0
				 * @hooked woocommerce_single_variation - 10 Empty div for variation data.
				 * @hooked woocommerce_single_variation_add_to_cart_button - 20 Qty and cart button.
				 */
				do_action( 'woocommerce_single_variation' );

				/**
				 * Hook: woocommerce_after_single_variation.
				 */
				do_action( 'woocommerce_after_single_variation' );
			?>
		</div>
	<?php endif; ?>

	<?php do_action( 'woocommerce_after_variations_form' ); ?>
</form>

<?php
do_action( 'woocommerce_after_add_to_cart_form' );
