<?php
/**
 * Single variation cart button
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 * updated studio akar: moved customization to make the file more compatible with WC updates.
 */

defined( 'ABSPATH' ) || exit;

global $product;
?>

<?php $availableStock = $product->get_stock_quantity(); ?>
<div class="woocommerce-variation-add-to-cart variations_button">
	<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>
	<?php if($availableStock): ?>
	<p class="line9">
		<span class="line9-1"><?php echo $availableStock; ?></span> of 100 editions available		
	</p>
	<?php endif; ?>

	<?php
	do_action( 'woocommerce_before_add_to_cart_quantity' );

	woocommerce_quantity_input( array(
		'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
		'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
		'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
	) );

	do_action( 'woocommerce_after_add_to_cart_quantity' );

	if($product)
		wc_get_template('single-product/customizer/product-customizer.php', ['product' => $product], '', '');
	?>
	
</div>

