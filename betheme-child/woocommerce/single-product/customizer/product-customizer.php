<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12" style="padding: 0;">
			<div class="new-variations-block col-xs-12 col-sm-6 col-md-6"></div>
			<div class="col-xs-12 col-sm-6 col-md-6">
				<div class="error-msg" style="/*display: block;*/">
					<div class="alert alert-danger">
						<p>Please choose a size!</p>
					</div>
				</div>
			</div>
		</div>

		<div class="col-xs-12 pl-0">
			<select id="addNewCartVal">
				<option value="0">Qty</option>
				<?php $cartNewVals = range(1,10); ?>
				<?php foreach($cartNewVals as $cartVal): ?>
				<option value="<?php echo $cartVal; ?>"><?php echo $cartVal; ?></option>
				<?php endforeach; ?>
			</select>
		</div>
		<div id="opt_product_frame_outer" class="col-xs-12">
			<p>Hahnemühle PhotoRag Bright White matte 310gsm paper</p>
			<!-- for now we add it as a hidden input. -->
			<input type="hidden" name="medium" id="medium" value="paper">

			<!--<p>CHOSE FRAME (optional)</p>-->
			<!--<input type="hidden" id="input_frame_price" name="input_frame_price" value="0" />
			<input type="hidden" id="frame_price" value="0" />
			<input type="hidden" id="selected_frame" name="selected_frame" value="no"/>
			<div class="frame-label-wrapper">			
				<span><label class="frame-selector cursor-pointer frame-1" data-frame="frame type 1"></label><span>Painted black paint marupa wood with visible grain</span></span>	
				<span><label class="frame-selector cursor-pointer frame-2" data-frame="frame type 2"></label><span>Painted natural varnish marupa wood with visible grain</span></span>	
			</div>	-->	
		</div>
		<div id="new_price" class="new_woocommerce_price_wrapper col-lg-12 col-xs-12 text-left"></div>
	</div>
	<div class="row">
		<div id="purchase" class="col-lg-12">
			<div class="row">
				<div class="col-xs-6 col-md-8">
					<button type="submit" class="btn btn-block customized_homepage_button">
						Buy it now
					</button>
					<?php //echo esc_html( $product->single_add_to_cart_text() ); ?>
				</div>
				<div class="col-xs-6 col-md-4">
					
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6 col-md-8">
					<p class="text-center mid-split">Or</p>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6 col-md-8">
					<a 
						class="btn btn-block customized_homepage_button" 
						href="javascript:;"
						data-fancybox
						data-type="iframe"
						data-src="<?php echo site_url().'/customizer/'.$product->get_id(); ?>"
						>
						CUSTOMIZE
					</a>
				</div>
			</div>
			
			<?php if (DEV_MODE): ?>
				<p class="text-left" style="margin-top: 25px;"><a class="customized_homepage_button" href="<?php echo site_url().'/customize-product/?p_id='.$product->get_id(); ?>">OLD CUSTOMIZE</a><p>
			<?php endif ?>

			<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
		</div>

		<input type="hidden" name="add-to-cart" value="<?php echo absint( $product->get_id() ); ?>" />
		<input type="hidden" id="product_id" name="product_id" value="<?php echo absint( $product->get_id() ); ?>" />
		<input type="hidden" id="calculated_price" name="calculated_price" value="">
		
	</div>
</div>

<script>
	(function( $ ) {
		'use strict';

		var frame_prices = ['ne',5480,4270,3020,1880,1330];

		function addCommas(str) {
		    str += '';
		    var x = str.split('.');
		    var x1 = x[0];
		    var x2 = x.length > 1 ? '.' + x[1] : '';
		    var rgx = /(\d+)(\d{3})/;
		    while (rgx.test(x1)) {
		        x1 = x1.replace(rgx, '$1' + ',' + '$2');
		    }
		    return x1 + x2;
		}

		// dom ready
		$(function(){
			$('body').on('click', '#curr-switch', function(event){
				event.preventDefault();
				var to_curr = $(this).attr('data-currency');
				$('li[data-currency="'+to_curr+'"]').trigger('click');
			});

			$('body').on('click', 'a.addToWishlist', function(e){
				$(document).ajaxSuccess(function(){
					$('div.yith-wcwl-wishlistaddedbrowse').fadeIn(500);
				});
			});
			
			$('.new-variations-block').prepend($('.variations-block').html());
			$('.variations-block').html('');
			var _sel_var_id = $('input#variation_id').val();
			if(_sel_var_id !== ''){
				console.log(_sel_var_id);
				setTimeout(function(){
					$('label[var-id="'+_sel_var_id+'"]').trigger('click');
					var input_id = $('label[var-id="'+_sel_var_id+'"]').attr('for');
					$('input#'+input_id).attr('selected', 'selected');
				}, 5000);
			}
			var win_width = $(window).width(), win_height = $(window).height();
			var f_width = parseInt(0.95*win_width);
			var f_height = parseInt(0.85*win_height);

			$('[data-fancybox]').fancybox({
				iframe : {
					preload : true,
					css : {
						width : f_width,
						height : f_height
					}
				}
			});

			// old function.
			// $(".setCropSize").change(function(){
			// 	$("#pa_select-size").val($(this).val()).trigger('change');
			// 	$('label.frame-selector').removeClass('active');
			// 	var woo_price_html = $('.woocommerce-variation.single_variation').html();
			// 	$('#new_price').html(woo_price_html);
			// 	$('#addNewCartVal').fadeIn();
			// });
			$('form.variations_form.cart').submit(function(event){
				var size_selected = $('.setCropSize').is(':checked');
				$('div.xoo-wsc-notice-box').css('display', 'none');
				// console.log(size_selected);
				if(!size_selected){
					$('.error-msg').fadeIn(1000, function(){
						$('.error-msg').delay(3000).fadeOut(500);
					});
					return false;
				}
			});

			$('body').on('change', '.setCropSize', function(event) {
				event.preventDefault();
				/* Act on the event */
				var d = $(this).data();
				// console.log(d);
				$("#pa_select-size").val($(this).val()).trigger('change');
				$('#new_price').html('<span class="blinking">calculating price</span>');

				$.ajax({
					url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
					type: 'POST',
					data: {
						action: 'ajax_get_calculated_price',
						width: d.width,
						height: d.height,
						variation: d.variation,
						product_id: $('input#product_id').val()
					},
				})
				.done(function(json) {
					var obj = $.parseJSON(json);
					// console.log(obj);
					$('#calculated_price').val(obj.price);
					$('#new_price').html(obj.string);
				})
				.fail(function(error) {
					console.log(error);
				})
				.always(function() {
					console.log("complete");
				});
				
			});

			$('body').on('click', 'label.frame-selector', function(){
				$('label.frame-selector').removeClass('active');
				$(this).addClass('active');
				var selected_frame = $(this).attr('data-frame');
				$('#selected_frame').val(selected_frame);

				var woo_price_html = $('.woocommerce-variation.single_variation').html();
				$('#new_price').html(woo_price_html);
				var selectedSize = $('input.setCropSize:checked');
				console.log(selectedSize.val());
				if(typeof selectedSize.val() === 'undefined'){
					alert('You have to select the size first to select the frame!');
					return false;
				}

				var current_price_html = $('.woocommerce-Price-amount').html();
				var $jQueryObject = $("<div/>").html(current_price_html);
				$jQueryObject.find('span').remove();
				var current_price = $jQueryObject.html();
				current_price = parseFloat(current_price);
				console.log(current_price);

				//now we calculate the dimension.
				var pic_width = selectedSize.attr('data-width'), pic_height = selectedSize.attr('data-height');
				var pic_size = parseInt(pic_width*pic_height);
				var n;
				console.log(pic_size);
				if(pic_size > 21600)
					n = 0;
				if(pic_size <= 21600 && pic_size > 15900)
					n = 1;
				if(pic_size <= 15900 && pic_size > 9980)
					n = 2;
				if(pic_size <= 9980 && pic_size > 4990)
					n = 3;
				if(pic_size <= 4990 && pic_size > 2495)
					n = 4;
				if(pic_size <= 2495)
					n = 5;

				var frame_price = frame_prices[n];
				console.log(frame_price);

				var new_price = current_price + frame_price;
				$('#frame_price').val(frame_price);
				$('#input_frame_price').val(frame_price.toFixed(2));
				var new_price_str = addCommas(new_price.toFixed(2));
				var new_html = '<span class="woocommerce-Price-currencySymbol">$</span>'+new_price_str;
				$('#new_price span.woocommerce-Price-amount.amount').html(new_html);
			});
		});
	})( jQuery );

	jQuery(document).ready(function($){
		$("#addNewCartVal").click(function(){
			var qtyVal = $(this).val();
			$( "input[name='quantity']" ).val(qtyVal);
		});
	});
</script>