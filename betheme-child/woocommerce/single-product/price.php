<?php
/**
 * Single Product Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

?>
<p class="<?php echo esc_attr( apply_filters( 'woocommerce_product_price_class', 'price' ) );?> stak-price">
	<?php echo $product->get_price_html(); ?>
	<?php 
		$currency_code = get_woocommerce_currency_symbol(); 
		switch ($currency_code) {
			case 'HK $':
				echo '<a class="text7 change_currency_switch pull-none" id="curr-switch" data-currency="USD">SWITCH TO US $</a>';
				break;
			
			case 'US $':
				echo '<a class="text7 change_currency_switch pull-none" id="curr-switch" data-currency="HKD">SWITCH TO HK $</a>';
				break;
		}
	?>
	<input type="hidden" id="currency_code" value="<?php echo $currency_code; ?>">	
</p>
<div style="display: none;"><?php echo do_shortcode("[woocs style=1 show_flags=1 width='300px' flag_position='right' txt_type='desc']"); ?></div>