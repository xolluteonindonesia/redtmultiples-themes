<?php
/**
 * Product Loop End
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/loop-end.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */
?>

	</ul>
</div>
<style>
.isotope_wrapper .products.list .column1{
	width:31.33%;
}
.gutter-sizer{
	width:1%;
}
@media only screen and (max-width: 600px) {
 .isotope_wrapper .products.list .column1{
		width:100%;
  }
}
.isotope_wrapper .products.list img{
	transition: transform .2s;
}
.isotope_wrapper .products.list img:hover{
	transform: scale(1.5);
}
.shop-filters{
	background:none;
}
/* .isotope_wrapper{
	padding-left:15px;
} */
</style>
<?php wp_enqueue_script('swz_wc_isotope_url','https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js'); ?>
<script>
jQuery(document).ready(function($){
var $isotopeGrid = $('.isotope_wrapper .products.list').isotope({
  itemSelector: '.column1', 
  masonry: {
    columnWidth: '.isotope_wrapper .products.list .column1',	
	gutter: 10
  }
});
$isotopeGrid.imagesLoaded().progress( function() {
  $isotopeGrid.isotope('layout');
});
});
</script>