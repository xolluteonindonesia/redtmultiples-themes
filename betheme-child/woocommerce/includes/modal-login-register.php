<div class="modal fade" id="loginFormModal" tabindex="-1" role="dialog" aria-labelledby="loginFormModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
				<?php if ( 'yes' === get_option( 'woocommerce_enable_myaccount_registration' ) ) : ?>

				<div class="u-columns col2-set" id="customer_login">

					<div class="u-column1 col-1">

				<?php endif; ?>

						<h2><?php esc_html_e( 'Login', 'woocommerce' ); ?></h2>

						<form class="woocommerce-form woocommerce-form-login login in-modal" method="post">

							<?php do_action( 'woocommerce_login_form_start' ); ?>

							<input type="hidden" name="redirect-to" id="redirect-to" value="">

							<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
								<label for="username"><?php esc_html_e( 'Username or email address', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
								<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
							</p>
							<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
								<label for="password"><?php esc_html_e( 'Password', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
								<input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" autocomplete="current-password" />
							</p>

							<?php do_action( 'woocommerce_login_form' ); ?>

							<p class="form-row">
								<label class="woocommerce-form__label woocommerce-form__label-for-checkbox woocommerce-form-login__rememberme">
									<input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> <span><?php esc_html_e( 'Remember me', 'woocommerce' ); ?></span>
								</label>
								<?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
								<button type="submit" class="woocommerce-button button woocommerce-form-login__submit" name="login" value="<?php esc_attr_e( 'Log in', 'woocommerce' ); ?>"><?php esc_html_e( 'LOGIN', 'woocommerce' ); ?></button>
							</p>
							<p class="woocommerce-LostPassword lost_password">
								<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php esc_html_e( 'Lost your password?', 'woocommerce' ); ?></a>
							</p>

							<?php do_action( 'woocommerce_login_form_end' ); ?>

						</form>
						<div id="login-result">
							<div id="msg" class="message">
								<p>verifying data ...</p>
							</div>
						</div>

				<?php if ( 'yes' === get_option( 'woocommerce_enable_myaccount_registration' ) ) : ?>

					</div>

					<div class="u-column2 col-2">

						<h2><?php esc_html_e( 'Register', 'woocommerce' ); ?></h2>

						<form method="post" class="woocommerce-form woocommerce-form-register register in-modal" <?php do_action( 'woocommerce_register_form_tag' ); ?> >

							<?php do_action( 'woocommerce_register_form_start' ); ?>

							<?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

								<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
									<label for="reg_username"><?php esc_html_e( 'Username', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
									<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="reg_username" autofill="off" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
								</p>

							<?php endif; ?>

							<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
								<label for="reg_email"><?php esc_html_e( 'Email address', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
								<input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="reg_email" id="reg_email" autofill="off" value="" />
							</p>

							<?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

								<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
									<label for="reg_password"><?php esc_html_e( 'Password', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
									<input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="reg_password" id="reg_password" autofill="off" autocomplete="new-password" />
								</p>

							<?php else : ?>

								<p><?php esc_html_e( 'A password will be sent to your email address.', 'woocommerce' ); ?></p>

							<?php endif; ?>

							<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
								A verification link will be sent to your email address.
							</p>
							<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
								Your personal data will be used to support your experience throughout this website, to manage access to your account, and for other purposes described in our <a href='/terms-and-conditions' target='_blank'>terms and conditions</a>.
							</p>
							<p class="form-row">
								<label class="woocommerce-form__label woocommerce-form__label-for-checkbox woocommerce-form-login__rememberme">
									<input type="checkbox" id="agree-toc" class="woocommerce-form__input woocommerce-form__input-checkbox" value="1">Okay, I agree.
								</label>
							</p>

							<?php do_action( 'woocommerce_register_form' ); ?>

							<p class="woocommerce-FormRow form-row">
								<?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
								<button type="submit" class="woocommerce-Button button" name="register" 
									value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>"
									>
									<?php esc_html_e( 'REGISTER', 'woocommerce' ); ?>
								</button>
							</p>

							<?php do_action( 'woocommerce_register_form_end' ); ?>

						</form>

						<div id="register-result">
							<div id="msg" class="message">
								<p>verifying data ...</p>
							</div>
						</div>

					</div>

				</div>
				<?php endif; ?>
			</div>
			<div class="modal-footer">
				<?php if(is_checkout()): ?>
					<a href="/gallery" class="btn btn-warning">Close & Continue Shopping</a>
				<?php else: ?>
					<a class="btn btn-warning" data-dismiss="modal" aria-label="Close">Close & Continue Shopping</a>
				<?php endif; ?>
				
				<!-- <button type="button" class="btn btn-primary">Save changes</button> -->
			</div>
		</div>
	</div>
</div>