<?php
/**
 * Add to wishlist button template
 *
 * @author Your Inspiration Themes
 * @package YITH WooCommerce Wishlist
 * @version 2.0.8
 */

if ( ! defined( 'YITH_WCWL' ) ) {
	exit;
} // Exit if accessed directly

global $product;
?>
<?php if(is_user_logged_in()): ?>
	<a href="<?php echo esc_url( add_query_arg( 'add_to_wishlist', $product_id ) )?>" rel="nofollow" data-product-id="<?php echo $product_id ?>" data-product-type="<?php echo $product_type?>" class="<?php echo $link_classes ?> addToWishlist" >
		<?php echo $icon ?>
		<span><i class="far fa-heart"></i></span>
	</a>
<?php else: ?>
	<a href="javascript:;" rel="nofollow" data-product-id="<?php echo $product_id ?>" data-product-type="<?php echo $product_type?>" data-toggle="modal" data-target="#loginFormModal" class="<?php echo $link_classes ?> addToWishlist" >
	    <?php echo $icon ?>
	    <span><i class="far fa-heart"></i></span>
	</a>
<?php endif; ?>
<img src="<?php echo esc_url( YITH_WCWL_URL . 'assets/images/wpspin_light.gif' ) ?>" class="ajax-loading" alt="loading" width="16" height="16" style="visibility:hidden" />