<?php 
defined( 'ABSPATH' ) || exit;

$scriptPath = dirname(__FILE__);
$path = realpath($scriptPath . '/./');
$filepath = explode("wp-content",$path);
// print_r($filepath);
define('WP_USE_THEMES', false);
require( ''.$filepath[0]. '/wp-blog-header.php' );

$upload_dir = wp_upload_dir();
$debug = DEV_MODE;

if(isset($_REQUEST['action'])){
	
	$user = wp_get_current_user();
	// var_dump($user->ID); exit;

	$request = $_REQUEST;
	// var_dump($request);
	$data = $request['cropped_img'];

	if (preg_match('/^data:image\/(\w+);base64,/', $data, $type)) {
	    $data = substr($data, strpos($data, ',') + 1);
	    $type = strtolower($type[1]); // jpg, png, gif

	    if (!in_array($type, [ 'jpg', 'jpeg' ])) {
	        throw new \Exception('invalid image type');
	        exit;
	    }

	    $data = base64_decode($data);

	    if (!$data) {
	        throw new \Exception('base64_decode failed');
	        exit;
	    }
	} else {
	    throw new \Exception('did not match data URI with image data');
	    exit;
	}
	// var_dump($upload_dir);
	// var_dump($data);

	$file_path = $upload_dir['basedir'].'/stak_customizer';
	if(!file_exists($file_path))
		mkdir($file_path, 0755);

	$crop_name = $request['id'].'-'.$user->ID.'-custom-'.date("Ymd_His").'.jpg';

	// now we save the image first before add to cart.
	$crop_save = $file_path.'/'.$crop_name;
	file_put_contents($crop_save, $data);
	$_REQUEST['cropped_img'] = $upload_dir['baseurl'].'/stak_customizer/'.$crop_name;

	try {
		WC()->cart->add_to_cart($request['id'], 1);
	} catch (Exception $e) {
		var_dump($e->getMessage());
		exit;
	}
	include('header.php');
	?>
	<div class="container-fluid">
		<div class="row">
			<div class="col pt-3">
				<div class="alert alert-success stak-alert mt-5">
					<h3 class="title">
						Success!
					</h3>
					<p>
						Your customized product has been recorded and added to your cart!
					</p>
					<p>
						<a href="javascript:;" class="button2 stak" onclick="parent.location.assign('/gallery'); parent.jQuery.fancybox.close();">CONTINUE CREATING</a>
						&nbsp;Or&nbsp;
						<a href="javascript:;" class="button2 stak" onclick="parent.location.assign('/cart'); parent.jQuery.fancybox.close();">GO TO YOUR CART</a>
					</p>
				</div>
			</div>
		</div>
	</div>

	<?php
	include('footer.php');
	exit;
}

include('header.php');

$product_id = intval( get_query_var( 'product_id' ) );
// var_dump($product_id);
$product = wc_get_product( $product_id );
// var_dump($product); exit;

// $img_path = IMAGES_FOLDER.$product_id.'.jpg';
// $img_to_crop = get_stylesheet_directory_uri().'/customizer/'.$img_path;
$img_to_crop = wp_get_attachment_url( $product->get_image_id(), 'full' );
// var_dump($img_to_crop); exit;

// $img_blob = get_stylesheet_directory().'/customizer/'.$img_path;
$img_blob = str_replace($upload_dir['baseurl'], $upload_dir['basedir'], $img_to_crop);
// var_dump($img_blob); exit;
$scale_base = [];
try {
	$img = new Imagick($img_blob);
	$scale_base['width'] = $img->getImageWidth();
	$scale_base['height'] = $img->getImageHeight();
} catch (Exception $e) {
	var_dump($e->getMessage()); exit;
}

// var_dump($scale_base); exit;
// var_dump(get_option('woocs'));
?>
<style type="text/css">
	.modal-dialog {
		max-width: 800px;
		margin: 30px auto;
	}
	.modal-body {
	  position:relative;
	  padding:0px;
	}
	.close {
	  position:absolute;
	  right:-30px;
	  top:0;
	  z-index:999;
	  font-size:2rem;
	  font-weight: normal;
	  color:#fff;
	  opacity:1;
	}
</style>
<div class="container-fluid">
	<div id="object" class="row">
		<div class="col-12 px-0">
			<div class="img-container">
				<img id="image" src="<?php echo $img_to_crop; ?>" alt="Artwork Gallery #<?php echo $product_id; ?>">
			</div>
			<div class="on-progress text-center">
				<p style="color: #fff;"><i class="fas fa-cog fa-spin"></i> PROCESSING...</p>
			</div>
		</div>
	</div>
	<div id="preview" class="row justify-content-center" style="display: none;">
		<div class="col-12 px-0">
			<div id="preview_img" class="img-container"></div>
			<div class="on-progress text-center">
				<p style="color: #fff;"><i class="fas fa-cog fa-spin"></i> PROCESSING...</p>
			</div>
		</div>
	</div>
	<div id="tools" class="row justify-content-center">
		<div class="col-12 px-1 stak docs-buttons">
			<div class="btn-group">
				<button type="button" class="btn btn-primary" data-method="zoom" data-option="0.1" title="Zoom In">
					<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="zoom in">
						<span class="fa fa-search-plus"></span>
					</span>
				</button>
				<button type="button" class="btn btn-primary" data-method="zoom" data-option="-0.1" title="Zoom Out">
					<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="zoom out">
						<span class="fa fa-search-minus"></span>
					</span>
				</button>
				<button type="button" class="btn btn-primary d-none" data-method="rotate" data-option="-45" title="Rotate Left">
					<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="rotate left">
						<span class="fa fa-undo-alt"></span>
					</span>
				</button>
				<button type="button" class="btn btn-primary d-none" data-method="rotate" data-option="45" title="Rotate Right">
					<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="rotate right">
						<span class="fa fa-redo-alt"></span>
					</span>
				</button>
			</div>
		</div>
	</div>
	<div class="row controller">
		<div class="col-12 main-controller">
			<form class="form" action="" id="save" method="post" enctype='multipart/form-data' data-product_id="<?php echo absint( $product->get_id() ); ?>">
				<div class="form-row  <?php echo $debug ? '' : 'd-none' ?>">
					<div class="col">
						<input type="hidden" name="action" id="action" value="stak_custom_art_add_to_cart">
						<input type="text" name="id" id="id" value="<?php echo absint( $product->get_id() ); ?>">
						<input type="text" id="pixel" value="<?php echo floatval(PIXEL_TO_CM); ?>">
						<input type="text" id="centi" value="<?php echo floatval(CM_TO_PIXEL); ?>">
						<input type="hidden" id="ori_width" value="<?php echo intval($scale_base['width']) ?>">
						<input type="hidden" id="ori_height" value="<?php echo intval($scale_base['height']) ?>">
						<input type="text" name="img_width" id="img_width" value="">
						<input type="text" name="img_height" id="img_height" value="">
						<input type="text" name="crop_height" id="crop_height" value="">
						<input type="text" name="crop_width" id="crop_width" value="">
						<input type="text" name="crop_top" id="crop_top" value="">
						<input type="text" name="crop_left" id="crop_left" value="">
						<input type="text" name="cropped_img" id="cropped_img" value="">
						<input type="text" id="custom_height" name="custom_height" value="">
						<input type="text" id="custom_width" name="custom_width" value="">
						<input type="text" id="image_data" name="image_data" value="">
						<input type="text" id="crop_box_data" name="crop_box_data" value="">
						<input type="text" id="canvas_data" name="canvas_data" value="">
					</div>
				</div>
				<div id="first" class="row">
					<div id="crop_set_holder" class="col-12 col-sm-4 col-md-3 col-lg-2 field-holder">
						<div class="row">
							<div class="form-group col form-group col pr-sm-0">
								<label>Drag and</label>
							</div>
							<div class="form-group col">
								<button id="set_crop" class="btn btn-primary btn-sm btn-block stak">
									<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="Drag and set the crop box">
										SET
									</span>
								</button>
							</div>
						</div>
					</div>
					
					<div id="size_set_holder" class="col-12 col-sm-8 col-md-9 col-lg-5 field-holder">
						<div class="row">
							<div class="form-group col-2 col-sm-2 col-lg-2 pl-lg-3 pr-lg-0">
								<label for="custom_width">Size</label>
							</div>
							<div class="form-group col col-sm-3 col-lg-3 px-sm-0">
								<div class="input-group input-group-sm">
									<span class="input-group-prepend">
										<span class="input-group-text">H</span>
									</span>
									<input type="text" name="set_height" id="set_height" class="form-control stak">
									<span class="input-group-append">
										<span class="input-group-text">cm</span>
									</span>
								</div>
							</div>
							<div class="form-group col col-sm-3 col-lg-3 px-sm-0">
								<div class="input-group input-group-sm">
									<span class="input-group-prepend">
										<span class="input-group-text">W</span>
									</span>
									<input type="text" name="set_width" id="set_width" class="form-control stak">
									<span class="input-group-append">
										<span class="input-group-text">cm</span>
									</span>
								</div>
							</div>
							<div class="form-group col-12 col-sm-3 col-lg-4 text-center">
								<button id="set_custom" class="btn btn-primary btn-sm btn-block stak">
									<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="Please enter the height and width and press ENTER.">
										ENTER
									</span>
								</button>
							</div>
						</div>
					</div>
					
					<div id="preset_set_holder" class="col-12 col-sm-4 col-md-3 col-lg-2 field-holder">
						<div class="row">
							<div id="preset_select" class="form-group col text-center">
								<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="Select a preset">
									<select id="preset" name="preset" class="form-control form-control-sm stak">
										<option value="0">Choose a preset</option>			
										<option data-w="42" data-h="59.4" value="rect1">42 x 59.4 cm (A2)</option>
										<option data-w="59.4" data-h="84" value="rect2">59.4 x 84 cm (A1)</option>
										<option data-w="84" data-h="118.8" value="rect3">84 x 118.8 cm (A0)</option>
										<option data-w="106" data-h="150" value="rect4">106 x 150 cm</option>
										<option data-w="40" data-h="40" value="sq1">40 x 40 cm</option>
										<option data-w="60" data-h="60" value="sq2">60 x 60 cm</option>
										<option data-w="80" data-h="80" value="sq3">80 x 80 cm</option>
										<option data-w="100" data-h="100" value="sq4">100 x 100 cm</option>
										<option data-w="150" data-h="150" value="sq5">150 x 150 cm </option>
									</select>
								</span>
							</div>
						</div>
					</div>
					
					<div class="col-12 col-sm-4 col-md-5 col-lg-2 field-holder">
						<div class="row">
							<div class="col-3 col-sm-2 col-lg-1 px-0 mx-md-1">
								<a id="help_btn" href="javascript:;" class="form-custom stak" 
									data-toggle="modal" 
									data-src="<?php echo YOUTUBE_URL; ?>" 
									data-target="#helpModal"
									>
									<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="" data-original-title="Click to see the video on how to use this customizer.">
										<i class="fas fa-info-circle"></i>
									</span>
								</a>
							</div>
							<div class="col-3 col-sm-3 col-md-3 col-lg-3 px-0 mx-half">
								<a id="cancel_btn" href="javascript:;" class="form-custom stak" onclick="parent.jQuery.fancybox.close();">BACK</a>
							</div>
							<div class="col-3 col-sm-3 col-md-3 col-lg-3 px-0 mx-half">
								<a id="reset_cropper" href="javascript:;" class="form-custom stak">RESET</a>
							</div>
							<div class="col-3 col-sm-3 col-md-3 col-lg-3 px-0 mx-half">
								<a id="crop_now" href="javascript:;" class="form-custom stak">CROP</a>
							</div>
						</div>
					</div>
					
				</div>
				<div id="second" class="form-row" style="display: none;">
					<div class="form-group col-md-1 d-none d-sm-none d-md-block">&nbsp;</div>
					<div class="form-group col-6 col-md-3 text-center">
						<p id="price_output"></p>
					</div>
					<div class="form-group col-6 col-md-2 ml-md-2 pl-md-3">
						<select id="medium" name="medium" class="form-control form-control-sm stak">
							<option value="0">Choose a medium</option>			
							<option value="paper">Paper</option>
						</select>
					</div>
					<div class="form-group col-12 col-md-5 text-center">
						<a id="back_button" href="javascript:;" class="form-custom stak ml-1">BACK</a>
						<a id="create_more" href="javascript:;"  onclick="parent.jQuery.fancybox.close();" class="form-custom stak ml-1" style="display: none;">CREATE MORE</a>
						<button id="submit_custom" href="javascript:;" class="btn btn-primary btn-sm stak">ADD TO CART</button>
						<button id="save_custom" href="javascript:;" class="btn btn-primary btn-sm stak">SAVE</button>
					</div>
					<div class="form-group col-md-2">&nbsp;</div>
				</div>
			</form>
		</div>
	</div>
	<div class="row preview-holder d-none">
		<div class="col-1">
			<p>&nbsp;</p>
		</div>
		<div class="col-8">
			<div class="col">
				<!-- <h3>Preview:</h3> -->
				<label>Preview</label>
				<div class="docs-preview clearfix">
					<div class="img-preview preview-lg"></div>
				</div>
			</div>
		</div>
		<div class="col-1">
			<p>&nbsp;</p>
		</div>
	</div>
	<div class="row temp-controller <?php echo $debug ? '' : 'd-none' ?>">
		<?php include('includes/old-parts.php'); ?>
	</div>
</div>
<!-- Modal is put here. -->
<div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="helpModal" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				<!-- 16:9 aspect ratio -->
				<div class="embed-responsive embed-responsive-16by9">
					<iframe class="embed-responsive-item" src="" id="video"  allowscriptaccess="always" allow="autoplay"></iframe>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
include('footer.php');
?>