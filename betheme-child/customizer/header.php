<!DOCTYPE html>
<html class="customizer-panel">

<head>
	<meta charset="<?php bloginfo('charset'); ?>" />

	<!-- styles -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css" crossorigin="anonymous">
	<link rel='stylesheet' id='mfn-fonts-css'  href='//fonts.googleapis.com/css?family=Lato%3A1%2C300%2C400%2C400italic%2C500%2C700%2C700italic&#038;ver=5.4' type='text/css' media='all' />
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/customizer/libs/css/bootstrap/bootstrap.min.css?ver=<?php echo THEME_VERSION; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/customizer/libs/js/cropperjs/cropper.min.css?ver=<?php echo THEME_VERSION; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/style.css?ver=<?php echo THEME_VERSION; ?>">
	<style type="text/css">
		
	</style>

	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/customizer/libs/js/jquery/jquery.min.js?ver=<?php echo THEME_VERSION; ?>"></script>
</head>
<body <?php body_class('template-customizer'); ?>>