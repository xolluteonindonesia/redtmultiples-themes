	<!-- this is where the scripts will put to minimize the load time. -->
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/customizer/libs/js/bootstrap/bootstrap.bundle.min.js?ver=<?php echo THEME_VERSION; ?>"></script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/customizer/libs/js/cropperjs/cropper.min.js?ver=<?php echo THEME_VERSION; ?>"></script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/customizer/libs/js/jquery-cropper/jquery-cropper.min.js?ver=<?php echo THEME_VERSION; ?>"></script>
	<script type="text/javascript">
		(function( $ ){
			'use strict';

			function disable_crop()
			{
				$('#crop_set_holder').addClass('disabled');
				$('#set_crop').attr('disabled', 'disabled');
				$('#set_crop').addClass('disabled');
			}

			function enable_crop()
			{
				$('#crop_set_holder').removeClass('disabled');
				$('#set_crop').removeAttr('disabled');
				$('#set_crop').removeClass('disabled');
			}

			function disable_size()
			{
				$('#size_set_holder').addClass('disabled');
				$('#set_height').attr('disabled', 'disabled');
				$('#set_height').addClass('disabled');
				$('#set_width').attr('disabled', 'disabled');
				$('#set_width').addClass('disabled');
			}

			function enable_size()
			{
				$('#size_set_holder').removeClass('disabled');
				$('#set_height').removeAttr('disabled');
				$('#set_height').removeClass('disabled');
				$('#set_width').removeAttr('disabled');
				$('#set_width').removeClass('disabled');
			}

			function disable_preset()
			{
				$('#preset_set_holder').addClass('disabled');
				$('select#preset').attr('disabled', 'disabled');
				$('select#preset').addClass('disabled');
			}

			function enable_preset()
			{
				$('#preset_set_holder').removeClass('disabled');
				$('select#preset').removeAttr('disabled');
				$('select#preset').removeClass('disabled');
			}

			function isTouchDevice(){
			    return true == ("ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch);
			}

			// dom ready
			$(function(){
				$('.on-progress').show(10);
				$('select#medium').val('paper');

				var URL = window.URL || window.webkitURL;
				var preset = false;
				var custom = false;
				var crop_lock = false;
				var aspect_ratio = null;
				var $image = $('#image');
				var $download = $('#download');
				var $dataX = $('#dataX');
				var $dataY = $('#dataY');
				var $dataHeight = $('#dataHeight');
				var $dataWidth = $('#dataWidth');
				var $dataRotate = $('#dataRotate');
				var $dataScaleX = $('#dataScaleX');
				var $dataScaleY = $('#dataScaleY');
				var options = {
					viewMode: 1,
					aspectRatio: null,
					preview: '.img-preview',
					crop: function (e) {
						$dataX.val(Math.round(e.detail.x));
						$dataY.val(Math.round(e.detail.y));
						$dataHeight.val(Math.round(e.detail.height));
						$dataWidth.val(Math.round(e.detail.width));
						$dataRotate.val(e.detail.rotate);
						$dataScaleX.val(e.detail.scaleX);
						$dataScaleY.val(e.detail.scaleY);
					}
				};
				var originalImageURL = $image.attr('src');
				var uploadedImageName = 'cropped.jpg';
				var uploadedImageType = 'image/jpeg';
				var uploadedImageURL;
				var stak_ratio = 0.08;
				var video_src

				// Tooltip
				if(isTouchDevice()===false) {
					$('[data-toggle="tooltip"]').tooltip();
				}

				$('body').on('click', '#help_btn', function(event) {
					event.preventDefault();
					var data = $(this).data();
					video_src = data.src;
					console.log(video_src);
				});

				var watched = localStorage.getItem('watched');
				console.log(watched);
				if(null === watched){
					localStorage.setItem('watched', true);
					$("#video").attr('src', '<?php echo YOUTUBE_URL; ?>');
					setTimeout(function(){
						$('#helpModal').modal('show');
					}, 5000);
					
				}else{
					console.log(watched);
					$("#video").attr('src', null);
					$('#helpModal').modal('hide');
				}
				// $("#video").attr('src', '<?php echo YOUTUBE_URL; ?>');
				// setTimeout(function(){
				// 	$('#helpModal').modal('show');
				// }, 5000);
				
				$('#helpModal').on('shown.bs.modal', function (e){
					$("#video").attr('src', video_src);
				});
				$('#helpModal').on('hide.bs.modal', function (e) {
					$("#video").attr('src', null);
				});
				

				// set the max-height of the container based on the screen size.
				var win_height = $(window).height(), obj_height = 0;
				console.log(win_height);

				if(isTouchDevice() === false) {
					if(win_height >= 918)
						obj_height = 0.85*win_height;

					if(win_height >= 700 && win_height < 918)
						obj_height = 0.775*win_height;
					
					if(win_height >= 653)
						obj_height = 0.8*win_height;

					if(win_height <= 652)
						obj_height = 0.75*win_height;

					console.log(obj_height);

					$('#object .img-container').css({
						'height': obj_height-15,
						'max-height': obj_height,
					});
					$('#preview .img-container').css({
						'height': obj_height,
						'max-height': obj_height,
					});
				}else{
					if(win_height >= 652)
						obj_height = 0.7*win_height;

					console.log(obj_height);

					$('#object .img-container').css({
						'height': obj_height-15,
						'max-height': obj_height,
					});
					$('#preview .img-container').css({
						'height': obj_height,
						'max-height': obj_height,
					});
				}

				// Cropper
				$image.on({
					ready: function (e) {
						console.log(e.type);
						// $image.cropper('moveTo', 0);
					},
					cropstart: function (e) {
						console.log(e.type, e.detail.action);
					},
					cropmove: function (e) {
						console.log(e.type, e.detail.action);
					},
					cropend: function (e) {
						console.log(e.type, e.detail.action);
					},
					crop: function (e) {
						console.log(e.type);
						var canvas = $image.cropper('getCanvasData');
						// console.log(canvas);
						$('#img_width').val(canvas.naturalWidth);
						$('#img_height').val(canvas.naturalHeight);
						var crop = $image.cropper('getCropBoxData');
						var crop_width = parseFloat(( crop.width*canvas.naturalWidth )/canvas.width );
						var crop_height = parseFloat(( crop.height*canvas.naturalHeight )/canvas.height );
						$('#crop_width').val(crop_width);
						$('#crop_height').val(crop_height);
						$('#crop_top').val( parseFloat(( crop.top*canvas.naturalWidth )/canvas.width ));
						$('#crop_left').val( parseFloat(( crop.left*canvas.naturalHeight )/canvas.height ));
						var _width = crop_width*parseFloat($('#pixel').val());
						var _height = crop_height*parseFloat($('#pixel').val());
						if(!custom && !preset){
							$('#custom_width').val(_width.toFixed(1));
							$('#custom_height').val(_height.toFixed(1));
						}
					},
					zoom: function (e) {
						console.log(e.type, e.detail.ratio);
					}
				}).cropper(options);

				// Buttons
				if (!$.isFunction(document.createElement('canvas').getContext)) {
					$('button[data-method="getCroppedCanvas"]').prop('disabled', true);
				}

				if (typeof document.createElement('cropper').style.transition === 'undefined') {
					$('button[data-method="rotate"]').prop('disabled', true);
					$('button[data-method="scale"]').prop('disabled', true);
				}

				// Download
				if (typeof $download[0].download === 'undefined') {
					$download.addClass('disabled');
				}
				$('.on-progress').delay(2000).fadeOut(500);

				$('body').on('click', '#set_crop', function(e){
					e.preventDefault();
					$('.on-progress').show(10);
					custom = false;
					preset = false;
					crop_lock = true;
					var crop_data = $image.cropper('getCropBoxData');
					aspect_ratio = crop_data.width/crop_data.height;
					
					$('.on-progress').delay(2000).fadeOut(500);
					disable_crop();
					disable_preset();
				});
				
				$('body').on('input', 'input#set_width', function(){
					// console.log(aspect_ratio);
					if(aspect_ratio > 0){
						var w = parseFloat($(this).val());
						var h = 0;
						if(w >= parseFloat('<?php echo MAX_SIZE; ?>')){
							alert('<?php echo OVER_SIZE_ALERT; ?>');
							$('#set_width').val('');
							$('#set_width').focus();
							return false;
						}

						if(w > 0 && w !== ''){
							h = w/aspect_ratio;
						}
						
						$('input#set_height').val(h.toFixed(1));
					}
				});

				$('body').on('input', 'input#set_height', function(){
					// console.log(aspect_ratio);
					if(aspect_ratio > 0){
						var h = parseFloat($(this).val());
						var w = 0;
						if(h >= parseFloat('<?php echo MAX_SIZE; ?>')){
							alert('<?php echo OVER_SIZE_ALERT; ?>');
							$('#set_height').val('');
							$('#set_height').focus();
							return false;
						}
						if(h > 0 && h !== ''){
							w = h*aspect_ratio;
						}
						
						$('input#set_width').val(w.toFixed(1));
					}
				});

				$('body').on('click', 'button#set_custom', function(e){
					e.preventDefault();
					
					var w = parseFloat($('#set_width').val()), h = parseFloat($('#set_height').val());

					console.log(w+' x '+h);

					if(isNaN(w)){
						alert('Please insert a width.');
						$('#set_width').focus();
						return false;
					}
					if(isNaN(h)){
						alert('Please insert a height.');
						$('#set_height').focus();
						return false;
					}

					var crop_data = $image.cropper('getCropBoxData');

					$('select#preset').attr('disabled', 'disabled');
					$('select#preset').addClass('disabled');

					if(w >= parseFloat('<?php echo MAX_SIZE; ?>')){
						alert('<?php echo OVER_SIZE_ALERT; ?>');
						$('#set_width').val('');
						$('#set_width').focus();
						return false;
					}
					if(h >= parseFloat('<?php echo MAX_SIZE; ?>')){
						alert('<?php echo OVER_SIZE_ALERT; ?>');
						$('#set_height').val('');
						$('#set_height').focus();
						return false;
					}
					
					$('.on-progress').show(10);

					var crop_w = w * parseFloat($('#centi').val());
					var crop_h = h * parseFloat($('#centi').val());
					$('#custom_width').val(w);
					$('#custom_height').val(h);
					
					console.log(crop_w+' | '+crop_h);

					aspect_ratio = crop_w / crop_h;
					options['aspectRatio'] = aspect_ratio;
					console.log(options);
					$image.cropper('destroy').cropper(options);
					preset = false;
					custom = true;
					crop_lock = true;

					$image.on('ready', function(e){
						if(custom){
							// var curr_canvas = $image.cropper('getCanvasData');
							// crop_w = 0.1*(parseFloat(curr_canvas.width)*crop_w)/parseFloat(curr_canvas.naturalWidth);
							// crop_h = 0.1*(parseFloat(curr_canvas.height)*crop_h)/parseFloat(curr_canvas.naturalHeight);
							// console.log(crop_w+' | '+crop_h);

							// now set the crop box to the current position.
							console.log(crop_data);
							// return false;
							$image.cropper('setCropBoxData', crop_data);
							disable_crop();
						}
						$('.on-progress').delay(2000).fadeOut(500);
					});
					disable_crop();
					disable_preset();
				});

				// stak customizer.
				$('body').on('change', 'select#preset', function(){
					$('.on-progress').show(10);
					if(crop_lock){
						// alert('Preset is disable, please click reset to enable preset!');
						return false;
					}
					var $this = $('option:selected', this);
					if($this.val() == 0){
						options['aspectRatio'] = 16/9;
						preset = false; custom = false;
					}else{
						var data = $this.data();
						preset = true; custom = false;
						// set the actual size first.
						var w = data.w, h = data.h, cp = parseFloat($('#centi').val());
						$('#custom_width').val(parseFloat(w));
						$('#custom_height').val(parseFloat(h));
						var crop_w = w*cp;
						var crop_h = h*cp;
						console.log(crop_w+' | '+crop_h);
						
						// set the ratio here.
						var ratio = crop_w/crop_h;
						console.log(ratio);
						console.log(options);
						options['aspectRatio'] = ratio;
						aspect_ratio = ratio;
					}
					$image.cropper('destroy').cropper(options).delay(200);

					$image.on('ready', function(e){
						if(preset){
							// now we get the canvas size and compare it with the size set.
							var curr_canvas = $image.cropper('getCanvasData');
							crop_w = (parseFloat(curr_canvas.width)*crop_w)/parseFloat($('#ori_width').val());
							crop_h = (parseFloat(curr_canvas.height)*crop_h)/parseFloat($('#ori_height').val());
							crop_w = crop_w*stak_ratio;
							crop_h = crop_h*stak_ratio;
							console.log(crop_w+' | '+crop_h);
							// now set the crop box to the current position.
							var crop_data = $image.cropper('getCropBoxData');
							// console.log(crop_data);
							crop_data.width = crop_w;
							crop_data.height = crop_h;
							$image.cropper('setCropBoxData', crop_data);
							disable_crop();
						}
						$('.on-progress').delay(2000).fadeOut(500);
					});
					disable_crop();
					disable_size();
					
				});

				$('body').on('click', '#reset_cropper', function(e){
					$('.on-progress').show(10);
					e.preventDefault();
					aspect_ratio = null;
					options['aspectRatio'] = aspect_ratio;
					preset = false; custom = false; crop_lock = false;
					$('#object').fadeIn(300);
					$image.cropper('destroy').cropper(options);
					$('input#set_height').val('');
					$('input#set_width').val('');
					$('select#preset').val(0);
					$('select#preset').removeAttr('disabled');
					$('select#preset').removeClass('disabled');
					
					$('#preview').delay(300).fadeOut(300);
					
					$('#tools').slideDown(500);
					$('#cropped_img').val('');
					$('.on-progress').delay(2000).fadeOut(500);
					enable_crop();
					enable_size();
					enable_preset();
				});

				$('body').on('click', '#crop_now', function(e){
					e.preventDefault();
					$('[data-toggle="tooltip"]').tooltip('hide');
					if(crop_lock){
						if($('#set_width').val() === '' && $('#set_height').val() === ''){
							alert('<?php echo NO_SIZE_ALERT; ?>');
							$('#set_width').focus();
							return false;
						}
					}

					if(!crop_lock){
						if(!custom && !preset){
							if($('#set_width').val() === ''){
								alert('Please insert a width.');
								$('#set_width').focus();
								return false;
							}
							if($('#set_height').val() === ''){
								alert('Please insert a height.');
								$('#set_height').focus();
								return false;
							}

							alert('<?php echo NO_SIZE_ALERT; ?>');
							return false;
						}

					}

					if(crop_lock){
						if(!custom){
							if($('#set_width').val() === ''){
								alert('Please insert a width.');
								$('#set_width').focus();
								return false;
							}
							if($('#set_height').val() === ''){
								alert('Please insert a height.');
								$('#set_height').focus();
								return false;
							}

							alert('<?php echo NO_SIZE_ALERT; ?>');
							return false;
						}

						if($('#set_width').val() >= parseFloat('<?php echo MAX_SIZE; ?>')){
							alert('<?php echo OVER_SIZE_ALERT; ?>');
							$('#set_width').val('');
							$('#set_width').focus();
							return false;
						}
						if($('#set_height').val() >= parseFloat('<?php echo MAX_SIZE; ?>')){
							alert('<?php echo OVER_SIZE_ALERT; ?>');
							$('#set_height').val('');
							$('#set_height').focus();
							return false;
						}
					}

					$('.on-progress').show(10);
					// we'll get the cropped image first.
					$('p#price_output').html('<i class="fas fa-cog fa-spin"></i> CROPPING ...');
					$('#first').slideUp(500);
					$('#second').slideDown(500);
					var _canvas = $image.cropper('getCanvasData');
					var export_options = {
						maxWidth : _canvas.naturalWidth,
						maxHeight : _canvas.naturalHeight,
						fillColor : 'transparent',
						imageSmoothingEnabled: false,
						imageSmoothingQuality: 'high'
					};
					var result = $image.cropper('getCroppedCanvas', export_options);
					$('#cropped_img').val(result.toDataURL(uploadedImageType));
					$('#preview_img').html(result);
					$('#object').fadeOut(50);
					$('#tools').slideUp(500);
					$('#preview').fadeIn(500);
					$('.on-progress').delay(2000).fadeOut(500);

					$('p#price_output').html('<i class="fas fa-cog fa-spin"></i> CALCULATING THE PRICE...');
					$.ajax({
						url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
						type: 'POST',
						data: {
							action: 'get_cropped_price',
							width: $('#custom_width').val(),
							height: $('#custom_height').val()
						},
					})
					.done(function(html) {
						$('p#price_output').html(html);
						// console.log(html); return false;
					})
					.fail(function() {
						console.log("error");
					})
					.always(function() {
						console.log("complete");
					});
				});

				$('body').on('click', '#back_button', function(e){
					$('p#price_output').html('');
					$('#second').slideUp(500);
					$('#first').delay(500).slideDown(500);
					$('#tools').delay(500).slideDown(500);
				});

				$('body').on('click', '#next_step', function(e){
					if($('#cropped_img').val() === ''){
						alert('<?php echo NO_CROP_ALERT; ?>');
						return false;
					}
					$('#second').slideUp(500);
					$('#third').delay(500).slideDown(500);
				});

				$('body').on('click', '#prev_step', function(e){
					$('#third').slideUp(500);
					$('#second').delay(500).slideDown();
				});

				$('body').on('click', '#save_custom', function(e){
					e.preventDefault();
					if($('#medium').val() == '0'){
						alert('Please chose a medium.');
						return false;
					}
					$(this).attr('disabled', 'disabled');
					$(this).addClass('disabled');
					$(this).html('<i class="fas fa-cog fa-spin"></i> SAVING THE DATA...');
					var image_data = $image.cropper('getImageData');
					var canvas_data = $image.cropper('getCanvasData');
					var crop_box_data = $image.cropper('getCropBoxData');

					$('#image_data').val(JSON.stringify(image_data));
					$('#crop_box_data').val(JSON.stringify(crop_box_data));
					$('#canvas_data').val(JSON.stringify(canvas_data));

					$('input#action').val('ajax_stak_save_custom_art');

					var forminput = $('form#save').serialize();
					$.ajax({
						url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
						type: 'POST',
						data: forminput,
					})
					.done(function(json) {
						var obj = $.parseJSON(json);
						console.log(obj);
						if(obj.status == 'success'){
							alert(obj.message);
							$('#save_custom').fadeOut('slow');
							$('#back_button').fadeOut(500, function(){
								$('#create_more').fadeIn(800);
							});
						}
						
						if(obj.status == 'not_logged_in'){
							alert(obj.message);
							parent.location.assign('/my-account');
							parent.jQuery.fancybox.close();
						}else{
							if(obj.status != 'success'){
								alert(obj.message);
								$('#save_custom').removeAttr('disabled');
								$('#save_custom').removeClass('disabled');
								return false;
							}
						}
					})
					.fail(function(error) {
						console.log(error);
					});
				});
				
				$('body').on('click', '#submit_custom', function(e){
					e.preventDefault();
					if($('#medium').val() == '0'){
						alert('Please chose a medium.');
						return false;
					}
					
					$('input#action').val('stak_custom_art_add_to_cart');
					$(this).attr('disabled', 'disabled');
					$(this).addClass('disabled');
					$(this).html('<i class="fas fa-cog fa-spin"></i> PROCESSING YOUR ORDER...');
					
					$('form#save').submit();
				});

				// Options
				$('.docs-toggles').on('change', 'input', function () {
					var $this = $(this);
					var name = $this.attr('name');
					var type = $this.prop('type');
					var cropBoxData;
					var canvasData;

					if (!$image.data('cropper')) {
						return;
					}

					if (type === 'checkbox') {
						options[name] = $this.prop('checked');
						cropBoxData = $image.cropper('getCropBoxData');
						canvasData = $image.cropper('getCanvasData');

						options.ready = function () {
							$image.cropper('setCropBoxData', cropBoxData);
							$image.cropper('setCanvasData', canvasData);
						};
					} else if (type === 'radio') {
						options[name] = $this.val();
					}

					$image.cropper('destroy').cropper(options);
				});

				// Methods
				$('.docs-buttons').on('click', '[data-method]', function () {
					var $this = $(this);
					var data = $this.data();
					var cropper = $image.data('cropper');
					var cropped;
					var $target;
					var result;

					if ($this.prop('disabled') || $this.hasClass('disabled')) {
						return;
					}

					if (cropper && data.method) {
						data = $.extend({}, data); // Clone a new one

						if (typeof data.target !== 'undefined') {
							$target = $(data.target);

							if (typeof data.option === 'undefined') {
								try {
									data.option = JSON.parse($target.val());
								} catch (e) {
									console.log(e.message);
								}
							}
						}

						cropped = cropper.cropped;

						switch (data.method) {
							case 'rotate':
								if (cropped && options.viewMode > 0) {
									$image.cropper('clear');
								}

								break;

							case 'getCroppedCanvas':
								if (uploadedImageType === 'image/jpeg') {
									if (!data.option) {
										data.option = {};
									}

									data.option.fillColor = '#fff';
								}

								break;
						}

						result = $image.cropper(data.method, data.option, data.secondOption);

						switch (data.method) {
							case 'rotate':
								if (cropped && options.viewMode > 0) {
									$image.cropper('crop');
								}

								break;

							case 'scaleX':
							case 'scaleY':
								$(this).data('option', -data.option);
								break;

							case 'getCroppedCanvas':
								if (result) {
									// Bootstrap's Modal
									$('#getCroppedCanvasModal').modal().find('.modal-body').html(result);

									if (!$download.hasClass('disabled')) {
										download.download = uploadedImageName;
										$download.attr('href', result.toDataURL(uploadedImageType));
									}
								}

								break;

							case 'destroy':
								if (uploadedImageURL) {
									URL.revokeObjectURL(uploadedImageURL);
									uploadedImageURL = '';
									$image.attr('src', originalImageURL);
								}

								break;
						}

						if ($.isPlainObject(result) && $target) {
							try {
								$target.val(JSON.stringify(result));
							} catch (e) {
								console.log(e.message);
							}
						}
					}
				});

				// Keyboard
				$(document.body).on('keydown', function (e) {
					if (e.target !== this || !$image.data('cropper') || this.scrollTop > 300) {
						return;
					}

					switch (e.which) {
						case 37:
							e.preventDefault();
							$image.cropper('move', -1, 0);
							break;

						case 38:
							e.preventDefault();
							$image.cropper('move', 0, -1);
							break;

						case 39:
							e.preventDefault();
							$image.cropper('move', 1, 0);
							break;

						case 40:
							e.preventDefault();
							$image.cropper('move', 0, 1);
							break;
					}
				});

				// Import image
				var $inputImage = $('#inputImage');

				if (URL) {
					$inputImage.change(function () {
						var files = this.files;
						var file;

						if (!$image.data('cropper')) {
							return;
						}

						if (files && files.length) {
							file = files[0];

							if (/^image\/\w+$/.test(file.type)) {
								uploadedImageName = file.name;
								uploadedImageType = file.type;

								if (uploadedImageURL) {
									URL.revokeObjectURL(uploadedImageURL);
								}

								uploadedImageURL = URL.createObjectURL(file);
								$image.cropper('destroy').attr('src', uploadedImageURL).cropper(options);
								$inputImage.val('');
							} else {
								window.alert('Please choose an image file.');
							}
						}
					});
				} else {
					$inputImage.prop('disabled', true).parent().addClass('disabled');
				}
			});
		})( jQuery );
	</script>
</body>
</html>